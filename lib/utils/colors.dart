import 'package:flutter/material.dart';

var textColorPrimary = Color(0xFF212121);
var textColorSecondary = Color(0xFF757575);
const white = Color(0xFFffffff);
const semi_white = Color(0xFFF8F7F7);
const black = Color(0xFF000000);
const view_color = Color(0xFFeaeaea);
const transparent = Color(0xFF00000000);

var colorPrimary = Color(0xFFfcb800);
var colorPrimaryDark = Colors.yellow[800];
const colorAccent = Color(0xFFf17015);

const background_color = Color(0xFFFCFDFD);
const editText_background = Color(0xFFF1F1F1);
const tomato = Color(0xFFFF6347);
const dots_color = Color(0xFFA5A5A5);
const editText_background_active = Color(0xFFFDE9DA);
const cat_1 = Color(0xFFFA4352);
const cat_2 = Color(0xFF34B5C8);
const cat_3 = Color(0xFFFED76D);
const cat_4 = Color(0xFF0C5A93);
const cat_5 = Color(0xFF3CA69B);
const sidebar_background = Color(0xFFEFEEEF);
const favourite_background = Color(0xFFFFEBEC);
const favourite_unselected_background = Color(0xFFECECEC);
const yellow = Color(0xFFFEBA39);
const light_gray = Color(0xFFECECEC);
const green = Color(0xFF66953A);
const red = Color(0xFFF61929);
const light_green = Color(0xFF81C049);
const more_infoHeading_background = Color(0xFFDDDDDD);
const more_infoValue_background = Color(0xFFF3F3F3);
const item_background = Color(0xFFEFF3F6);
const bg_4star = Color(0xFF78c639);
const light_grey = Color(0xFFFAFAFA);
const checkbox_color = Color(0xFFDFDFDF);
const itemText_background = Color(0xFFF8F8F8);
const track_green = Color(0xFF64B931);
const track_yellow = Color(0xFFECC134);
const track_red = Color(0xFFF61929);
const track_grey = Color(0xFFD3D3D3);
const radiobuttonTint = Color(0xFF4353FA);
const scratch_start_gradient = Color(0xFFe5e5e5);
const scratch_end_gradient = Color(0xFFcccccc);
const shadow_color = Color(0X95E9EBF0);
