import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs{
  static BuildContext? context;
  static SharedPreferences? sharedPreferences;
  static const String _LOGGED_IN = 'logged_in';
  
  static void init(BuildContext context)async{
    context = context;
    sharedPreferences = await SharedPreferences.getInstance();
  }

  static Future<bool> isWalkThroughShown() async{
    return sharedPreferences!.getBool(_LOGGED_IN) ?? false;
  }

  static void setWalkThroughShown(bool shown) async{
    sharedPreferences!.setBool(_LOGGED_IN, shown);
  }

}