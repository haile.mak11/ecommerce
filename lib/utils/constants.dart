/*fonts*/

const fontRegular = 'Regular';
const fontMedium = 'Medium';
const fontSemibold = 'Semibold';
const fontBold = 'Bold';
/* font sizes*/
const textSizeSmall = 12.0;
const textSizeSMedium = 14.0;
const textSizeMedium = 16.0;
const textSizeLargeMedium = 18.0;
const textSizeNormal = 20.0;
const textSizeXNormal = 22.0;
const textSizeLarge = 24.0;
const textSizeXLarge = 30.0;

const spacing_control_half = 2.0;
const spacing_control = 4.0;
const spacing_standard = 8.0;
const spacing_middle = 10.0;
const spacing_standard_new = 16.0;
const spacing_large = 24.0;
const spacing_xlarge = 32.0;
const spacing_xxLarge = 40.0;

enum ConfirmAction { CANCEL, ACCEPT }

const bg_bottom_bar = "assets/images/bg_bottom_bar.png";
const card = "assets/images/card.png";
const chip = "assets/images/chip.png";
const ic_app_background = "assets/images/ic_app_background.png";
const ic_app_icon = "assets/images/ic_app_icon.png";
const ic_reward_back = "assets/images/ic_reward_back.jpg";
const ic_scratch_pattern = "assets/images/ic_scratch_pattern.png";
const ic_walk = "assets/images/ic_walk.jpeg";
const splash_bg = "assets/images/splash_bg.png";
const splash_img = "assets/images/splash_img.png";
const whitegradient = "assets/images/whitegradient.png";

const ic_user = "assets/images/ic_user.png";
const user_placeholder = "assets/images/user.png";
const ic_logo = "assets/images/logo.png";
const ic_logo_1 = "assets/images/logo-1.png";
const settings = "assets/images/settings.png";
const ic_heart = "assets/images/ic_heart.svg";
const ic_home = "assets/images/ic_home.svg";
const ic_no_connection = "assets/images/no-connection.png";
const ic_no_result = "assets/images/no-result-search.png";
const ic_no_product = "assets/images/no-product.png";

const menu = "assets/images/menu.svg";
const ic_cart = "assets/images/ic_cart.svg";
const radar = "assets/images/radar.gif";

const bg_banner_1 = "assets/images/banner-1.jpg";
const bg_banner_2 = "assets/images/banner-2.jpg";
const bg_banner_3 = "assets/images/banner-3.jpg";

const walkthrough_1 = "assets/images/walk_1.svg";
const walkthrough_2 = "assets/images/walk_2.svg";
const walkthrough_3 = "assets/images/walk_3.svg";

const agriculture = "assets/images/categories/agriculture-food.jpg";
const apparel = "assets/images/categories/apparel-textiles-accessories.jpg";
const books = "assets/images/categories/books.jpg";
const electronics = "assets/images/categories/electronics.jpg";
const health = "assets/images/categories/health-beauty.jpg";
const home_lights = "assets/images/categories/home-lights-construction.jpg";
const machinery =
    "assets/images/categories/machinery-industrial-parts-tools.jpg";
const packaging = "assets/images/categories/packaging-advertising-office.jpg";
const auto = "assets/images/categories/auto-transportation.jpg";
const bags_shoes = "assets/images/categories/bags-shoes-accessories.jpg";
const metallurgy =
    "assets/images/categories/metallurgy-chemicals-tire-rubber-plastics.jpg";
const electrical_equipment =
    "assets/images/categories/electrical-equipment-components-telecoms.jpg";
const gifts_sports = "assets/images/categories/gifts-sports-toys.jpg";
