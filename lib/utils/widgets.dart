import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:ecommerce_client/models/category.dart';
import 'package:ecommerce_client/providers/authentication_state.dart';
import 'package:ecommerce_client/providers/product_provider.dart';
import 'package:ecommerce_client/views/auth/login_screen.dart';
import 'package:ecommerce_client/views/auth/signup_screen.dart';
import 'package:ecommerce_client/views/product_detail_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shimmer/shimmer.dart';
import 'app_localizations.dart';
import 'constants.dart';
import 'colors.dart';

extension CurrencyString on String {
  String toCurrencyString() {
    return this.toString() + ' ETB';
  }
}

Widget cartIcon(context, cartCount, {isHome = false}) {
  return Stack(
    alignment: Alignment.center,
    children: <Widget>[
      Container(
          width: 40,
          height: 40,
          margin: EdgeInsets.only(right: spacing_standard_new),
          padding: EdgeInsets.all(spacing_standard),
          decoration: isHome
              ? null
              : BoxDecoration(
                  shape: BoxShape.circle, color: Colors.grey.withOpacity(0.5)),
          child: Icon(
            Icons.shopping_cart,
            size: isHome ? 30 : null,
          )),
      cartCount > 0
          ? Align(
              alignment: Alignment.topRight,
              child: Container(
                padding: EdgeInsets.all(5),
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.red),
                child: Text(
                  cartCount.toString(),
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2!
                      .copyWith(color: Colors.white),
                ),
              ),
            )
          : Container()
    ],
  );
}

Widget text(
  String text, {
  var isCentered = false,
  Color textColor = black,
  var fontSize = 24,
  var fontFamily = "Bold",
  var maxLine = 1,
  var latterSpacing = 0.5,
  bool textAllCaps = false,
  var isLongText = false,
  bool lineThrough = false,
}) {
  return Text(
    textAllCaps ? text.toUpperCase() : text,
    textAlign: isCentered ? TextAlign.center : TextAlign.start,
    maxLines: isLongText ? null : maxLine,
    overflow: TextOverflow.ellipsis,
    style: TextStyle(
      fontFamily: GoogleFonts.workSans().fontFamily,
      fontWeight: FontWeight.bold,
      height: 1.5,
      letterSpacing: latterSpacing,
      decoration:
          lineThrough ? TextDecoration.lineThrough : TextDecoration.none,
    ),
  );
}

BoxDecoration boxDecoration(
    {double radius = 2,
    Color color = Colors.transparent,
    Color bgColor = white,
    var showShadow = false}) {
  return BoxDecoration(
    color: bgColor,
    boxShadow: showShadow
        ? [BoxShadow(color: Colors.grey.withOpacity(0.2))]
        : [BoxShadow(color: Colors.transparent)],
    border: Border.all(color: color),
    borderRadius: BorderRadius.all(Radius.circular(radius)),
  );
}

class QuantityButton extends StatelessWidget {
  final quantity;

  final callback;
  @override
  QuantityButton(this.quantity, this.callback);
//   QuantityButtonState createState() => QuantityButtonState();
// }

// class QuantityButtonState extends State<QuantityButton> {
  final visibility = true;

  final TextEditingController _textController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    _textController.text = quantity.toString();
    var width = MediaQuery.of(context).size.width;
    return Visibility(
      visible: visibility,
      child: Container(
        height: width * 0.08,
        alignment: Alignment.center,
        //width: width * 0.26,
        width: width * 0.4,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: width * 0.08,
              height: width * 0.08,
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(4),
                      topLeft: Radius.circular(4))),
              child: IconButton(
                icon: Icon(Icons.remove, color: white, size: 10),
                onPressed: () {
                  if (quantity == 1 || quantity < 1) {
                    callback(1);
                  } else {
                    callback(quantity - 1);
                  }
                },
              ),
            ),
            Expanded(
              child: Container(
                width: width * 0.08,
                decoration: BoxDecoration(
                    border: Border.all(color: Theme.of(context).primaryColor)),
                child: TextField(
                  onSubmitted: (value) {
                    callback(int.parse(_textController.text));
                  },
                  textInputAction: TextInputAction.done,
                  textAlign: TextAlign.center,
                  textAlignVertical: TextAlignVertical.top,
                  decoration: InputDecoration(
                    focusedBorder: InputBorder.none,
                    counterText: '',
                  ),
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                  style: Theme.of(context).inputDecorationTheme.counterStyle,
                  controller: _textController,
                  maxLength: 6,
                ),
              ),
            ),
            Container(
              width: width * 0.08,
              height: width * 0.08,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(4),
                      topRight: Radius.circular(4))),
              child: IconButton(
                icon: Icon(Icons.add, color: white, size: 10),
                onPressed: () {
                  callback(quantity + 1);
                },
              ),
            ),
          ],
        ),
      ),
      replacement: GestureDetector(
        onTap: () {},
        child: Container(
          width: width * 0.22,
          height: width * 0.08,
          alignment: Alignment.center,
          child: Text(
            AppLocalizations.of(context).translate("lbl_add"),
          ),
        ),
      ),
    );
  }
}

toast(String caption, context) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(caption),
    ),
  );
}

Widget bottomButton(context, buttonText, onclick, {height = 50.0}) {
  return Container(
    child: SizedBox(
      width: MediaQuery.of(context).size.width * 0.6,
      child: FloatingActionButton(
        heroTag: buttonText,
        child: Text(
          buttonText,
          style: Theme.of(context).textTheme.bodyText1,
        ),
        foregroundColor: white,
        backgroundColor: Theme.of(context).primaryColor,
        shape: StadiumBorder(),
        onPressed: () => {onclick()},
      ),
    ),
  );
}

class ProductHorizontalList extends StatelessWidget {
  final list;
  final isHorizontal;

  ProductHorizontalList(this.list, {this.isHorizontal = false});

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        list.isEmpty
            ? Container(
                padding: EdgeInsets.all(spacing_large),
                child: Column(
                  children: [
                    Image.asset(ic_no_product),
                    SizedBox(
                      height: spacing_standard,
                    ),
                    Text(
                      AppLocalizations.of(context)
                          .translate("hint_no_products"),
                    ),
                  ],
                ),
              )
            : Container(
                height: 310,
                margin: EdgeInsets.symmetric(vertical: spacing_standard_new),
                alignment: Alignment.centerLeft,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: list.length,
                    shrinkWrap: true,
                    padding: EdgeInsets.only(right: spacing_standard_new),
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.only(left: spacing_standard_new),
                        width: width * 0.4,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ProductDetail(product: list[index])));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Stack(
                                children: [
                                  CachedNetworkImage(
                                      imageUrl: list[index].thumbnail,
                                      width: double.infinity,
                                      height: 180,
                                      fit: BoxFit.cover),
                                  if (list[index].deal.hasDeal)
                                    Positioned(
                                      right: 0,
                                      child: Container(
                                        decoration: BoxDecoration(color: red),
                                        child: Text(
                                          " ${list[index].unitPriceDiscountPercentage}% ",
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1!
                                              .copyWith(color: Colors.white),
                                        ),
                                      ),
                                    ),
                                ],
                              ),
                              SizedBox(height: spacing_standard),
                              Expanded(
                                child: Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Expanded(
                                            child: Text(
                                          AppLocalizations.of(context)
                                                      .locale
                                                      .languageCode ==
                                                  'en'
                                              ? list[index].name
                                              : list[index].amharic.name,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1,
                                          maxLines: 2,
                                        )),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        RatingBar.builder(
                                          initialRating: double.parse(
                                              list[index].rating.average ?? 0),
                                          minRating: 1,
                                          direction: Axis.horizontal,
                                          allowHalfRating: true,
                                          itemCount: 5,
                                          ignoreGestures: true,
                                          itemSize: textSizeMedium,
                                          unratedColor:
                                              Theme.of(context).backgroundColor,
                                          itemBuilder: (context, _) => Icon(
                                            Icons.star,
                                            color: Colors.amber,
                                          ),
                                          onRatingUpdate: (rating) {},
                                        ),
                                      ],
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          list[index].deal.hasDeal
                                              ? list[index]
                                                  .deal
                                                  .totalPriceRange
                                                  .toString()
                                                  .toCurrencyString()
                                              : list[index]
                                                  .totalPriceRange
                                                  .toString()
                                                  .toCurrencyString(),
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1!
                                              .copyWith(
                                                  fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        SizedBox(width: spacing_control),
                                        if (list[index].deal.hasDeal)
                                          Text(
                                            list[index]
                                                .totalPriceRange
                                                .toString()
                                                .toCurrencyString(),
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText2!
                                                .copyWith(
                                                  color: Colors.grey,
                                                  decoration: TextDecoration
                                                      .lineThrough,
                                                ),
                                          ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }),
              ),
      ],
    );
  }
}

Widget horizontalHeading(context, var title,
    {bool showViewAll = true, var callback}) {
  return Padding(
    padding: const EdgeInsets.only(
      left: spacing_standard_new,
      right: spacing_standard_new,
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          child: Text(title,
              style: TextStyle(
                fontSize: 18,
              )),
        ),
        showViewAll
            ? GestureDetector(
                onTap: callback,
                child: Container(
                  padding: EdgeInsets.only(
                      left: spacing_standard_new,
                      top: spacing_control,
                      bottom: spacing_control),
                  child: Text(
                    AppLocalizations.of(context).translate("lbl_view_all"),
                  ),
                ),
              )
            : Container()
      ],
    ),
  );
}

class CustomCarousel extends StatefulWidget {
  final banners;

  CustomCarousel({this.banners});
  @override
  _CustomCarouselState createState() => _CustomCarouselState();
}

class _CustomCarouselState extends State<CustomCarousel> {
  var _carouselController = new CarouselController();
  var currentIndexPage = 0;
  @override
  Widget build(context) {
    var width = MediaQuery.of(context).size.width;

    return Column(
      children: [
        CarouselSlider(
          carouselController: _carouselController,
          options: CarouselOptions(
            autoPlay: true,
            onPageChanged: (index, reason) {
              setState(() {
                currentIndexPage = index;
              });
            },
            viewportFraction: 1.0,
            height: MediaQuery.of(context).size.height * 0.25,
            enlargeCenterPage: false,
            scrollDirection: Axis.horizontal,
          ),
          items: widget.banners.map<Widget>((slider) {
            return Builder(
              builder: (BuildContext context) {
                return Container(
                  width: width * 1.0,
                  //height: 400,
                  //height: width + width * 0.1,
                  decoration: BoxDecoration(
                    color: white,
                    borderRadius:
                        BorderRadius.all(Radius.circular(spacing_standard)),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.4),
                          spreadRadius: spacing_control_half,
                          blurRadius: 10,
                          offset: Offset(1, 3))
                    ],
                  ),
                  margin: EdgeInsets.all(spacing_standard_new),
                  child: Center(
                      child: Image.asset(slider,
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          fit: BoxFit.fill)),
                );
              },
            );
          }).toList(),
        ),
        DotsIndicator(
          dotsCount: widget.banners.length > 1 ? widget.banners.length : 1,
          position: currentIndexPage.toDouble(),
          decorator: DotsDecorator(
              color: view_color,
              activeColor: Theme.of(context).primaryColor,
              activeSize: const Size.square(10.0),
              spacing: EdgeInsets.all(spacing_control)),
        ),
      ],
    );
  }
}

BoxDecoration boxDecorations(
    {double radius = 8,
    Color color = Colors.transparent,
    Color bgColor = Colors.white,
    var showShadow = false}) {
  return BoxDecoration(
    color: bgColor,
    boxShadow: showShadow
        ? [BoxShadow(color: Color(0x95E9EBF0), blurRadius: 10, spreadRadius: 2)]
        : [BoxShadow(color: Colors.transparent)],
    border: Border.all(color: color),
    borderRadius: BorderRadius.all(Radius.circular(radius)),
  );
}

void showErrorDialog(dynamic message, context) {
  print(message);
  var textJson = json.decode(message.toString());
  print(textJson);
  showDialog(
    context: context,
    builder: (_) => AlertDialog(
      title: Text(textJson.keys.first),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ...textJson.values.map(
            (e) => Text(e),
          ),
        ],
      ),
      actions: <Widget>[
        if (textJson.keys.first == 'Information')
          TextButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (ctx) => LoginScreen()),
              );
            },
            child: Text("Login"),
          ),
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text("Close"),
        ),
      ],
    ),
  );
}

Widget errorScreen(BuildContext context, fetchData, {errorCode = 0}) {
  String errorTitle = '';
  String errorMessage = '';
  String image = '';
  switch (errorCode) {
    case 0:
      errorTitle = AppLocalizations.of(context).translate("error_no_internet");
      errorMessage = AppLocalizations.of(context).translate("lbl_try_again");
      image = ic_no_connection;
      break;
    case 1:
      errorTitle = AppLocalizations.of(context).translate("error_no_result");
      errorMessage = AppLocalizations.of(context).translate("error_no_match");
      image = ic_no_result;
      break;
    case 2:
      errorTitle = AppLocalizations.of(context).translate("err_empty_cart");
      errorMessage = AppLocalizations.of(context).translate("lbl_browse_buy");
      image = ic_no_result;
      break;
    case 3:
      errorTitle = AppLocalizations.of(context).translate("lbl_empty_wishlist");
      errorMessage =
          AppLocalizations.of(context).translate("lbl_browse_wishlist");
      image = ic_no_result;
      break;
    case 4:
      errorTitle = AppLocalizations.of(context).translate("err_empty_orders");
      errorMessage = AppLocalizations.of(context).translate("lbl_browse_buy");
      image = ic_no_result;
      break;
    case 5:
      errorTitle = AppLocalizations.of(context).translate("hint_no_products");
      errorMessage = "";
      image = ic_no_product;
      break;
  }
  return Container(
    height: MediaQuery.of(context).size.height,
    child: Stack(
      alignment: Alignment.center,
      children: [
        SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Image.asset(image),
                height: 200,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(spacing_large),
                          child: Text(
                            errorTitle,
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .headline4!
                                .copyWith(fontWeight: FontWeight.normal),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: spacing_xxLarge),
                          child: Text(
                            errorMessage,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              if (errorCode == 0)
                MaterialButton(
                  color: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  onPressed: () {
                    fetchData();
                  },
                  child: Text("Refresh"),
                ),
              if ((errorCode == 2 || errorCode == 3) &&
                  !context.read(authenticationNotifierProvider).isAuth)
                Padding(
                  padding: const EdgeInsets.all(spacing_xxLarge),
                  child: Column(
                    children: [
                      Container(
                        child: Text(
                          AppLocalizations.of(context)
                              .translate("lbl_account_needed"),
                          style: Theme.of(context)
                              .textTheme
                              .headline6!
                              .copyWith(fontWeight: FontWeight.normal),
                        ),
                      ),
                      SizedBox(
                        height: spacing_standard_new,
                      ),
                      SizedBox(
                        width: double.infinity,
                        height: 50,
                        // height: double.infinity,
                        child: MaterialButton(
                          padding: EdgeInsets.all(spacing_standard),
                          child: Text(
                            AppLocalizations.of(context)
                                .translate("lbl_sign_in"),
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          color: Theme.of(context).primaryColor,
                          onPressed: () => Navigator.of(context)
                              .pushReplacement(MaterialPageRoute(
                                  builder: (context) => LoginScreen())),
                        ),
                      ),
                      SizedBox(height: spacing_standard_new),
                      SizedBox(
                        width: double.infinity,
                        height: 50,
                        // height: double.infinity,
                        child: MaterialButton(
                          padding: EdgeInsets.all(spacing_standard),
                          child: Text(
                            " ${AppLocalizations.of(context).translate("lbl_create_account")}",
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(color: Colors.black87),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(8.0),
                          ),
                          onPressed: () => Navigator.of(context)
                              .pushReplacement(MaterialPageRoute(
                                  builder: (context) => SignupScreen())),
                          color: white,
                        ),
                      )
                    ],
                  ),
                ),
            ],
          ),
        ),
      ],
    ),
  );
}

Widget getListView(context, productModels) {
  var width = MediaQuery.of(context).size.width;

  return Container(
    child: ListView.builder(
      itemCount: productModels.length,
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    ProductDetail(product: productModels[index]),
              ),
            );
          },
          child: Container(
            padding: EdgeInsets.all(10.0),
            child: IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Stack(
                    children: [
                      Container(
                        // padding: EdgeInsets.all(1),
                        decoration: BoxDecoration(
                            border: Border.all(color: view_color, width: 1)),
                        child: Image.network(productModels[index].thumbnail,
                            fit: BoxFit.cover,
                            height: width * 0.35,
                            width: width * 0.29),
                      ),
                      if (productModels[index].deal.hasDeal)
                        Positioned(
                          right: 0,
                          child: Container(
                            decoration: BoxDecoration(color: red),
                            child: Text(
                              " ${productModels[index].unitPriceDiscountPercentage}% ",
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(color: Colors.white),
                            ),
                          ),
                        ),
                    ],
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            AppLocalizations.of(context).locale.languageCode ==
                                    'en'
                                ? productModels[index].name
                                : productModels[index].amharic.name,
                            style: Theme.of(context).textTheme.bodyText1,
                            maxLines: 2,
                          ),
                        ),
                        SizedBox(height: 4),
                        Row(
                          children: <Widget>[
                            Text(
                              productModels[index].deal.hasDeal
                                  ? productModels[index]
                                      .deal
                                      .totalPriceRange
                                      .toString()
                                      .toCurrencyString()
                                  : productModels[index]
                                      .totalPriceRange
                                      .toString()
                                      .toCurrencyString(),
                              // productModels[index].deal.hasDeal
                              //     ? productModels[index].deal.totalUnitPrice
                              //     : productModels[index].totalUnitPrice,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: spacing_control,
                            ),
                            if (productModels[index].deal.hasDeal)
                              Text(
                                productModels[index]
                                    .totalPriceRange
                                    .toString()
                                    .toCurrencyString(),
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                        color: Colors.grey,
                                        decoration: TextDecoration.lineThrough),
                              ),
                          ],
                        ),
                        SizedBox(
                          height: spacing_standard,
                        ),
                        Expanded(
                          child: Text(
                            AppLocalizations.of(context).locale.languageCode ==
                                    'en'
                                ? productModels[index].description
                                : productModels[index].amharic.description,
                            maxLines: 2,
                          ),
                        ),
                        SizedBox(height: 4),
                        Expanded(
                          child: Align(
                            alignment: Alignment.bottomLeft,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                RatingBar.builder(
                                  initialRating: double.parse(
                                      productModels[index].rating.average),
                                  direction: Axis.horizontal,
                                  allowHalfRating: true,
                                  ignoreGestures: true,
                                  itemCount: 5,
                                  itemSize: 16,
                                  unratedColor:
                                      Theme.of(context).backgroundColor,
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.amber,
                                  ),
                                  onRatingUpdate: (rating) {},
                                ),
                                Container(
                                  padding: EdgeInsets.all(spacing_control),
                                  margin:
                                      EdgeInsets.only(right: spacing_standard),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle, color: white),
                                  child: Icon(
                                    Icons.favorite_border,
                                    color: textColorPrimary,
                                    size: 16,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 4),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    ),
  );
}

Widget getGridView(BuildContext context, productModels) {
  return Container(
    child: GridView.builder(
        itemCount: productModels.length,
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        padding: EdgeInsets.all(spacing_middle),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 9 / 16,
            crossAxisSpacing: spacing_middle,
            mainAxisSpacing: spacing_large),
        itemBuilder: (_, index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          ProductDetail(product: productModels[index])));
            },
            child: Container(
              child: Wrap(
                crossAxisAlignment: WrapCrossAlignment.start,
                children: <Widget>[
                  AspectRatio(
                    aspectRatio: 9 / 11,
                    child: Stack(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(1),
                          decoration: BoxDecoration(
                              border:
                                  Border.all(color: view_color, width: 0.5)),
                          child: Image.network(
                            productModels[index].thumbnail,
                            fit: BoxFit.cover,
                            width: double.infinity,
                            height: double.infinity,
                          ),
                        ),
                        Positioned(
                          right: 0,
                          bottom: 0,
                          child: Container(
                            padding: EdgeInsets.all(spacing_control),
                            margin: EdgeInsets.all(spacing_standard),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle, color: white),
                            child: Icon(
                              Icons.favorite_border,
                              color: textColorPrimary,
                              size: 16,
                            ),
                          ),
                        ),
                        if (productModels[index].unitPriceDiscountPercentage !=
                            '0')
                          Positioned(
                            right: 0,
                            child: Container(
                                decoration: BoxDecoration(color: red),
                                margin: EdgeInsets.all(spacing_control),
                                child: Text(
                                  "${productModels[index].unitPriceDiscountPercentage}%",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(color: Colors.white),
                                )),
                          )
                      ],
                    ),
                  ),
                  SizedBox(height: 2),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          AppLocalizations.of(context).locale.languageCode ==
                                  'en'
                              ? productModels[index].name
                              : productModels[index].amharic.name,
                          style: Theme.of(context).textTheme.bodyText1,
                          maxLines: 2,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      RatingBar.builder(
                        initialRating:
                            double.parse(productModels[index].rating.average),
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        ignoreGestures: true,
                        itemSize: textSizeLargeMedium,
                        unratedColor: Theme.of(context).backgroundColor,
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        onRatingUpdate: (rating) {
                          print(rating);
                        },
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        productModels[index].deal.hasDeal
                            ? productModels[index]
                                .deal
                                .totalPriceRange
                                .toString()
                                .toCurrencyString()
                            : productModels[index]
                                .totalPriceRange
                                .toString()
                                .toCurrencyString(),
                        // productModels[index].deal.hasDeal
                        //     ? productModels[index].deal.totalUnitPrice
                        //     : productModels[index].totalUnitPrice,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        width: spacing_control,
                      ),
                    ],
                  ),
                  if (productModels[index].deal.hasDeal)
                    Row(
                      children: [
                        Text(
                          productModels[index]
                              .totalPriceRange
                              .toString()
                              .toCurrencyString(),
                          style:
                              Theme.of(context).textTheme.bodyText2!.copyWith(
                                    color: Colors.grey,
                                    decoration: TextDecoration.lineThrough,
                                  ),
                        ),
                      ],
                    )
                ],
              ),
            ),
          );
        }),
  );
}

class FilterBottomSheetLayout extends StatefulWidget {
  final categoryList;
  final onSave;
  final CategoryModel? mainCategory;
  FilterBottomSheetLayout({
    Key? key,
    required this.categoryList,
    this.onSave,
    this.mainCategory,
  }) : super(key: key);

  @override
  FilterBottomSheetLayoutState createState() {
    return FilterBottomSheetLayoutState();
  }
}

class FilterBottomSheetLayoutState extends State<FilterBottomSheetLayout> {
  int currentIndex = -1;

  @override
  Widget build(BuildContext context) {
    List<CategoryModel> categoryList = widget.categoryList;
    final productCategoryListView = categoryList.isEmpty
        ? Text(
            AppLocalizations.of(context).translate("lbl_no_category"),
          )
        : ListView.builder(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemCount: categoryList.length,
            itemBuilder: (_, index) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: ChoiceChip(
                  label: Text(
                    AppLocalizations.of(context).locale.languageCode == 'en'
                        ? categoryList[index].name
                        : categoryList[index].nameAm,
                    style: TextStyle(
                        color: currentIndex == index ? Colors.white : black),
                  ),
                  selected: currentIndex == index,
                  onSelected: (selected) {
                    setState(() {
                      currentIndex = index;
                    });
                  },
                  elevation: 2,
                  backgroundColor: Colors.white10,
                  selectedColor:
                      Theme.of(context).primaryColor.withOpacity(0.9),
                ),
              );
            });

    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate("lbl_filter"),
        ),
        iconTheme: IconThemeData(color: white),
        actions: <Widget>[
          InkWell(
              child: Container(
                  alignment: Alignment.centerRight,
                  padding: EdgeInsets.only(right: spacing_middle),
                  child: Text(
                    AppLocalizations.of(context).translate("lbl_apply"),
                    style: TextStyle(fontSize: 18),
                  )),
              onTap: () {
                widget.onSave(
                  currentIndex,
                );
              })
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                  left: spacing_standard_new, top: spacing_standard_new),
              child: Text(
                AppLocalizations.of(context).translate("lbl_categories"),
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
            SizedBox(height: 10),
            Container(child: productCategoryListView, height: 50),
            // Padding(
            //   padding: EdgeInsets.only(
            //       left: spacing_standard_new, top: spacing_standard_new),
            //   child: Text(
            //     AppLocalizations.of(context).translate("lbl_price"),
            //     style: Theme.of(context).textTheme.bodyText1,
            //   ),
            // ),
            // SizedBox(height: 10),
            // RangeSlider(
            //   divisions: 20,
            //   values: _currentRangeValues,
            //   min: 0,
            //   max: 1000,
            //   labels: RangeLabels(
            //     _currentRangeValues.start.round().toString(),
            //     _currentRangeValues.end.round().toString(),
            //   ),
            //   onChanged: (RangeValues values) {
            //     setState(() {
            //       _currentRangeValues = values;
            //     });
            //   },
            // ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: [
            //     Text(
            //         ' ${AppLocalizations.of(context).translate("lbl_from")} ${_currentRangeValues.start} ${AppLocalizations.of(context).translate("lbl_upto")} ${_currentRangeValues.end}')
            //   ],
            // )
          ],
        ),
      ),
    );
  }
}

class LoadingListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Container(
      width: double.infinity,
      child: Column(
        children: <Widget>[
          Expanded(
            child: Shimmer.fromColors(
              baseColor: Theme.of(context).backgroundColor,
              highlightColor: Colors.grey.shade100,
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: 6,
                itemBuilder: (_, index) => Container(
                  padding: EdgeInsets.all(spacing_standard),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.all(spacing_standard_new),
                        width: width * 0.25,
                        height: width * 0.3,
                        color: Theme.of(context).backgroundColor,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: spacing_standard,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: spacing_standard_new,
                              width: width * 0.6,
                              color: Theme.of(context).backgroundColor,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                vertical: spacing_standard,
                              ),
                            ),
                            Container(
                              height: spacing_standard_new,
                              width: width * 0.4,
                              color: Theme.of(context).backgroundColor,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                vertical: spacing_standard,
                              ),
                            ),
                            Container(
                              height: spacing_standard_new,
                              width: width * 0.2,
                              color: Theme.of(context).backgroundColor,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class LoadingGridView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Container(
      width: double.infinity,
      child: Column(
        children: <Widget>[
          Expanded(
            child: Shimmer.fromColors(
              baseColor: Theme.of(context).backgroundColor,
              highlightColor: Colors.grey.shade100,
              child: GridView.builder(
                shrinkWrap: true,
                padding: EdgeInsets.all(spacing_middle),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 9 / 14,
                    crossAxisSpacing: spacing_middle,
                    mainAxisSpacing: spacing_standard_new),
                itemCount: 6,
                itemBuilder: (_, index) => Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AspectRatio(
                          aspectRatio: 9 / 11,
                          child: Container(
                            color: Theme.of(context).backgroundColor,
                          ),
                        ),
                        const Padding(
                          padding:
                              EdgeInsets.symmetric(vertical: spacing_standard),
                        ),
                        Container(
                          height: spacing_standard_new,
                          width: width * 0.3,
                          color: Theme.of(context).backgroundColor,
                        ),
                        const Padding(
                          padding: EdgeInsets.symmetric(vertical: 2.0),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
