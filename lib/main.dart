import 'package:ecommerce_client/providers/app_theme_state.dart';
import 'package:ecommerce_client/providers/app_lang_state.dart';
import 'package:ecommerce_client/providers/shared_utility.dart';
import 'package:ecommerce_client/views/home/home_screen.dart';
import 'package:ecommerce_client/views/order/order_detail_screen.dart';
import 'package:ecommerce_client/views/order/orders_screen.dart';
import 'package:ecommerce_client/views/splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'utils/app_localizations.dart';
import 'package:loader_overlay/loader_overlay.dart';

late AndroidNotificationChannel channel;
late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

late FirebaseMessaging messaging;
late Firebase firebase;
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('Handling a background message ${message.messageId}');
  if (!kIsWeb) {
    channel = const AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      description:
          'This channel is used for important notifications.', // description
      importance: Importance.high,
    );

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
    messaging = FirebaseMessaging.instance;
  }

  RemoteNotification? notification = message.notification;
  AndroidNotification? android = message.notification?.android;
  if (notification != null && android != null && !kIsWeb) {
    flutterLocalNotificationsPlugin.show(
        notification.hashCode,
        notification.title,
        notification.body,
        NotificationDetails(
          android: AndroidNotificationDetails(
            channel.id,
            channel.name,
            channelDescription: channel.description,
            icon: 'launch_background',
          ),
        ),
        payload: notification.body!.split(' ').first.split('-').last);
  }
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final sharedPreferences = await SharedPreferences.getInstance();
  await Firebase.initializeApp();
  FirebaseMessaging.instance.requestPermission();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  if (!kIsWeb) {
    channel = const AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      description:
          'This channel is used for important notifications.', // description
      importance: Importance.high,
    );

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
    messaging = FirebaseMessaging.instance;
  }
  runApp(
    ProviderScope(
      overrides: [
        sharedPreferencesProvider.overrideWithValue(sharedPreferences),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Route<dynamic> generateRoute(RouteSettings settings) {
      switch (settings.name) {
        case '/home':
          return MaterialPageRoute(builder: (_) => HomeScreen());
        case '/home/orders':
          return MaterialPageRoute(builder: (_) => OrdersScreen());
        case '/home/orders/order':
          var data = settings.arguments as String;
          return MaterialPageRoute(builder: (_) => OrderDetail(id: data));
        default:
          return MaterialPageRoute(
              builder: (_) => Scaffold(
                    body: Center(
                        child: Text('No route defined for ${settings.name}')),
                  ));
      }
    }

    final _appThemeState = watch(appThemeStateProvider);
    final _appLanguageState = watch(languageStateProvider);
    return GlobalLoaderOverlay(
      overlayColor: Theme.of(context).backgroundColor,
      overlayOpacity: 0.4,
      child: MaterialApp(
        initialRoute: '/',
        onGenerateRoute: generateRoute,
        supportedLocales: [
          Locale('am', 'ET'),
          Locale('en', 'US'),
        ],
        locale: LanguageHelper.convertLangNameToLocale(_appLanguageState),
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        localeResolutionCallback: (locale, supportedLocales) {
          for (var supportedLocale in supportedLocales) {
            if (supportedLocale.languageCode == locale!.languageCode &&
                supportedLocale.countryCode == locale.countryCode) {
              return supportedLocale;
            }
          }
          return LanguageHelper.convertLangNameToLocale(_appLanguageState);
        },
        title: 'Wuqiyanos',
        theme: context
            .read(appThemeProvider)
            .getAppThemedata(context, _appThemeState),
        home: SplashScreen(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
