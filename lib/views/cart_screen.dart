import 'package:ecommerce_client/providers/cart_provider.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'items_fragment.dart';
import 'order/make_order_screen.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CartScreen extends StatefulWidget {
  static String tag = '/CartScreen';
  final isHome;
  CartScreen({this.isHome});
  @override
  CartScreenState createState() => CartScreenState();
}

class CartScreenState extends State<CartScreen> {
  @override
  void initState() {
    super.initState();
    fetchData();
  }

  bool isLoading = true;
  bool isError = false;
  fetchData() async {
    try {
      await context.read(cartProvider).fetchCart();
      setState(() {
        isLoading = false;
        isError = false;
      });
    } catch (error) {
      setState(() {
        isLoading = false;
        isError = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var bottomButtons = Container(
      height: 65,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              color: Theme.of(context).backgroundColor,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    AppLocalizations.of(context)
                        .translate("lbl_subtotal_amount"),
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  Consumer(builder: (context, watch, child) {
                    var items = watch(cartProvider).cart.items;
                    var total = 0;
                    items.forEach((item) {
                      total += item.price * item.quantity;
                    });
                    return Text("${total.toString().toCurrencyString()}",
                        style: Theme.of(context).textTheme.bodyText1);
                  }),
                ],
              ),
            ),
          ),
          Expanded(
            child: InkWell(
              child: Container(
                child: Text(
                  AppLocalizations.of(context).translate("lbl_continue"),
                  style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: Colors.white, fontWeight: FontWeight.normal),
                ),
                color: Theme.of(context).primaryColor,
                alignment: Alignment.center,
                height: double.infinity,
              ),
              onTap: () {
                onClick();
              },
            ),
          )
        ],
      ),
    );
    return Scaffold(
      appBar: widget.isHome
          ? null
          : AppBar(
              title: Text(
                AppLocalizations.of(context).translate("lbl_cart_details"),
              ),
            ),
      body: isLoading
          ? LoadingListView()
          : isError
              ? errorScreen(context, fetchData, errorCode: 2)
              : Consumer(builder: (context, watch, child) {
                  return watch(cartProvider).cart.items.isEmpty
                      ? errorScreen(context, fetchData, errorCode: 2)
                      : Container(
                          child: Stack(children: <Widget>[
                          SingleChildScrollView(
                            child: Column(
                              children: [
                                Consumer(builder: (context, watch, child) {
                                  return ItemsFragment(
                                    items: watch(cartProvider).cart.items,
                                  );
                                }),
                                SizedBox(
                                  height: 56,
                                )
                              ],
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Padding(
                              padding: const EdgeInsets.all(0),
                              child: bottomButtons,
                            ),
                          ),
                        ]));
                }),
    );
  }

  void onClick() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => MakeOrderRequest(),
      ),
    );
  }
}
