import 'package:ecommerce_client/models/product.dart';
import 'package:ecommerce_client/providers/product_provider.dart';
import 'package:ecommerce_client/providers/wishlist_provider.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';

import 'package:ecommerce_client/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:ecommerce_client/utils/colors.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../product_detail_screen.dart';

class Wishlist extends StatefulWidget {
  static String tag = '/Wishlist';
  final isHome;
  Wishlist({this.isHome});
  @override
  WishlistState createState() => WishlistState();
}

class WishlistState extends State<Wishlist> {
  @override
  void initState() {
    super.initState();
    fetchData();
  }

  fetchData() async {
    try {
      await context.read(wishlistProvider).fetchWishlist();
      setState(() {
        isLoading = false;
        isError = false;
      });
    } catch (error) {
      setState(() {
        isLoading = false;
        isError = true;
      });

      print(error);
    }
  }

  bool isLoading = true;
  bool isError = false;

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    // if (isLoading)
    //   context.loaderOverlay.show();
    // else
    //   context.loaderOverlay.hide();

    return Scaffold(body: Consumer(builder: (context, watch, child) {
      List<ProductModel> list = watch(wishlistProvider).wishlist;

      return isLoading
          ? LoadingListView()
          : isError || context.read(wishlistProvider).wishlist.isEmpty
              ? errorScreen(context, fetchData, errorCode: 3)
              : ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: list.length,
                  shrinkWrap: true,
                  padding: EdgeInsets.only(bottom: 70),
                  itemBuilder: (context, index) {
                    return Container(
                      //color: Theme.of(context).backgroundColor,
                      margin: EdgeInsets.only(
                          left: spacing_standard_new,
                          right: spacing_standard_new,
                          top: spacing_standard_new),
                      child: IntrinsicHeight(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Image.network(
                              list[index].thumbnail,
                              width: width * 0.25,
                              height: width * 0.3,
                              fit: BoxFit.fill,
                            ),
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left: 16.0),
                                    child: Text(
                                      AppLocalizations.of(context)
                                                  .locale
                                                  .languageCode ==
                                              'en'
                                          ? list[index].name
                                          : list[index].amharic.name,
                                      style:
                                          Theme.of(context).textTheme.bodyText1,
                                    ),
                                  ),
                                  SizedBox(
                                    height: spacing_standard,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 16.0),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Text(
                                          list[index].deal.hasDeal
                                              ? list[index]
                                                  .deal
                                                  .totalUnitPrice
                                                  .toString()
                                                  .toCurrencyString()
                                              : list[index]
                                                  .totalUnitPrice
                                                  .toString()
                                                  .toCurrencyString(),
                                        ),
                                        SizedBox(
                                          width: spacing_control,
                                        ),
                                        if (list[index].deal.hasDeal)
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 3.0),
                                            child: Text(
                                              list[index]
                                                  .totalUnitPrice
                                                  .toString(),
                                              style: TextStyle(
                                                  color: textColorSecondary,
                                                  fontFamily: fontRegular,
                                                  fontSize: textSizeSMedium,
                                                  decoration: TextDecoration
                                                      .lineThrough),
                                            ),
                                          ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(left: 16.0),
                                      child: Align(
                                        alignment: Alignment.bottomLeft,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                            RatingBar.builder(
                                              initialRating: double.parse(
                                                  list[index].rating.average),
                                              direction: Axis.horizontal,
                                              ignoreGestures: true,
                                              itemCount: 5,
                                              itemSize: 16,
                                              itemBuilder: (context, _) => Icon(
                                                Icons.star,
                                                color: Colors.amber,
                                              ),
                                              onRatingUpdate: (rating) {},
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Expanded(
                                            child: InkWell(
                                              onTap: () {
                                                Navigator.of(context).push(
                                                  MaterialPageRoute(
                                                    builder: (ctx) =>
                                                        ProductDetail(
                                                      product: list[index],
                                                    ),
                                                  ),
                                                );
                                              },
                                              child: Container(
                                                padding: EdgeInsets.all(
                                                    spacing_standard),
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                      color: Theme.of(context)
                                                          .backgroundColor),
                                                ),
                                                child: Row(
                                                  children: <Widget>[
                                                    Icon(
                                                      Icons.details,
                                                      size: 18,
                                                    ),
                                                    Text(
                                                      AppLocalizations.of(
                                                              context)
                                                          .translate(
                                                              "lbl_product_details"),
                                                    )
                                                  ],
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            child: InkWell(
                                              onTap: () => context
                                                  .read(wishlistProvider)
                                                  .removeItem(
                                                      product: list[index]),
                                              child: Container(
                                                padding: EdgeInsets.all(
                                                    spacing_standard),
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color: Theme.of(context)
                                                            .backgroundColor)),
                                                child: Row(
                                                  children: <Widget>[
                                                    Icon(
                                                      Icons.delete_outline,
                                                      size: 18,
                                                    ),
                                                    Text(
                                                      AppLocalizations.of(
                                                              context)
                                                          .translate(
                                                              "lbl_remove"),
                                                    )
                                                  ],
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                ),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  });
    }));
  }
}
