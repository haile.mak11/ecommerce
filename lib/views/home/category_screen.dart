import 'package:ecommerce_client/models/category.dart';
import 'package:ecommerce_client/models/product.dart';
import 'package:ecommerce_client/providers/categories_provider.dart';
import 'package:ecommerce_client/providers/product_provider.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';

import 'package:ecommerce_client/utils/widgets.dart';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CategoryScreen extends StatefulWidget {
  static String tag = '/Category';
  final CategoryModel category;

  CategoryScreen({required this.category});

  @override
  CategoryScreenState createState() => CategoryScreenState();
}

class CategoryScreenState extends State<CategoryScreen> {
  var currentIndex = -1;
  var products = [];
  bool isListViewSelected = false;
  List<CategoryModel> subCategoriesList = [];
  bool isInitLoading = false;
  bool isLoadingMore = false;
  bool hasNextPage = true;
  int page = 1;
  ScrollController _controller = new ScrollController();

  @override
  void initState() {
    super.initState();
    initLoad();
    _controller..addListener(loadMore);
  }

  @override
  void dispose() {
    _controller.removeListener(loadMore);
    super.dispose();
  }

  void initLoad() async {
    try {
      setState(() {
        isInitLoading = true;
      });
      subCategoriesList = await context
          .read(categoriesProvider)
          .fetchSubCategories(widget.category.name, 2);

      List<ProductModel> categoryProducts = await context
          .read(productProvider)
          .fetchProducts(category: widget.category.name);

      setState(() {
        isInitLoading = false;

        products.addAll(categoryProducts);
      });
    } catch (error) {
      setState(() {
        isInitLoading = false;
      });
      showErrorDialog(error, context);
    }
  }

  void loadMore() async {
    if (hasNextPage == true &&
        isInitLoading == false &&
        isLoadingMore == false &&
        _controller.position.extentAfter < 300)
      try {
        setState(() {
          isLoadingMore = true;
        });
        page += 1;
        List<ProductModel> categoryProducts =
            await context.read(productProvider).fetchProducts(
                  category: widget.category.name,
                  pageNumber: page,
                );

        setState(() {
          isLoadingMore = false;
          if (categoryProducts.length > 0) {
            products.addAll(categoryProducts);
          } else {
            hasNextPage = false;
          }
        });
      } catch (error) {
        setState(() {
          isLoadingMore = false;
        });
        showErrorDialog(error, context);
      }
  }

  void onListClick(which) {
    setState(() {
      if (which == 1) {
        isListViewSelected = true;
      } else if (which == 2) {
        isListViewSelected = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).locale.languageCode == 'en'
              ? widget.category.name
              : widget.category.nameAm,
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.filter_list),
              onPressed: () => showMyBottomSheet(context)),
          IconButton(
              icon: Icon(
                isListViewSelected ? Icons.view_list : Icons.border_all,
                size: 24,
              ),
              onPressed: () {
                setState(() {
                  isListViewSelected = !isListViewSelected;
                });
              })
        ],
      ),
      body: isInitLoading
          ? LoadingGridView()
          : products.isEmpty
              ? errorScreen(context, initLoad, errorCode: 5)
              : SingleChildScrollView(
                  controller: _controller,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: spacing_standard_new),
                      Column(children: <Widget>[
                        isListViewSelected
                            ? getListView(context, products)
                            : getGridView(context, products),
                      ]),
                      SizedBox(
                        height: spacing_standard_new,
                      ),
                      if (isLoadingMore == true)
                        Padding(
                          padding: const EdgeInsets.only(top: 10, bottom: 40),
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        ),
                    ],
                  ),
                ),
    );
  }

  void showMyBottomSheet(BuildContext context) {
    void onSave(
      int currentIndex,
    ) async {
      Navigator.of(context).pop();
      List<ProductModel> categoryProducts =
          await context.read(productProvider).fetchProducts(
                category: currentIndex >= 0
                    ? subCategoriesList[currentIndex].name
                    : widget.category.name,
              );
      setState(() {
        products = categoryProducts;
      });
    }

    Navigator.of(context).push(
      new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return FilterBottomSheetLayout(
            categoryList: subCategoriesList,
            onSave: onSave,
            mainCategory: widget.category,
          );
        },
        fullscreenDialog: true,
      ),
    );
  }

  filterSubCategory(index) async {
    List<ProductModel> categoryProducts =
        await context.read(productProvider).fetchProducts(
              category: currentIndex >= 0
                  ? subCategoriesList[index].name
                  : widget.category.name,
            );
    setState(() {
      products = categoryProducts;
    });
  }
}
