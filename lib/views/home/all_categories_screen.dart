import 'package:ecommerce_client/providers/categories_provider.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';
import 'package:ecommerce_client/views/home/category_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AllCategoriesScreen extends StatefulWidget {
  static String tag = '/CategoriestScreen';

  @override
  AllCategoriesScreenState createState() {
    return AllCategoriesScreenState();
  }
}

class AllCategoriesScreenState extends State<AllCategoriesScreen> {
  var categoriesList = [];
  var isListViewSelected = false;
  var errorMsg = '';
  var scrollController = new ScrollController();
  bool isLoading = true;
  bool isLoadingMoreData = false;
  int page = 1;
  bool isLastPage = false;

  @override
  void dispose() {
    super.dispose();
    scrollController.dispose();
  }

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  fetchData() async {
    categoriesList
        .addAll(await context.read(categoriesProvider).fetchMainCategories());
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final gridView = Container(
      child: GridView.builder(
          itemCount: categoriesList.length,
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          padding: EdgeInsets.all(spacing_middle),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 9 / 11,
              crossAxisSpacing: spacing_middle,
              mainAxisSpacing: spacing_standard_new),
          itemBuilder: (_, index) {
            return InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            CategoryScreen(category: categoriesList[index])));
              },
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(
                        color: Theme.of(context).backgroundColor, width: 1)),
                child: Wrap(
                  crossAxisAlignment: WrapCrossAlignment.start,
                  children: <Widget>[
                    categoriesList[index].image == null
                        ? Container(
                            height: 100,
                          )
                        : Image.asset(
                            categoriesList[index].image,
                            width: 200,
                            height: 180,
                            fit: BoxFit.cover,
                          ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          AppLocalizations.of(context).locale.languageCode ==
                                  'en'
                              ? categoriesList[index].name
                              : categoriesList[index].nameAm,
                          textAlign: TextAlign.center,
                        )),
                      ],
                    ),
                  ],
                ),
              ),
            );
          }),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate("lbl_categories"),
        ),
      ),
      body: SingleChildScrollView(
        controller: scrollController,
        child: Column(
          children: <Widget>[
            !isLoading
                ? Center(
                    child: Column(children: <Widget>[
                    gridView,
                  ]))
                : Center(child: Text(errorMsg)),
          ],
        ),
      ),
    );
  }
}
