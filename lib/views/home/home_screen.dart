import 'dart:io';

import 'package:ecommerce_client/main.dart';
import 'package:ecommerce_client/models/product.dart';
import 'package:ecommerce_client/providers/cart_provider.dart';
import 'package:ecommerce_client/providers/categories_provider.dart';
import 'package:ecommerce_client/providers/product_provider.dart';
import 'package:ecommerce_client/providers/wishlist_provider.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';

import 'package:ecommerce_client/utils/widgets.dart';
import 'package:ecommerce_client/views/cart_screen.dart';
import 'package:ecommerce_client/views/home/home_fragment.dart';
import 'package:ecommerce_client/views/home/search_screen.dart';
import 'package:ecommerce_client/views/settings/settings_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'wishlist_screen.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var list = [];
  var homeFragment = Home();
  var cartFragment = CartScreen(isHome: true);
  var wishlistFragment = Wishlist();

  var settings = Settings();
  var fragments;
  var selectedTab = 0;

  String searchText = "";

  TextEditingController searchController = new TextEditingController();

  bool isEmpty = false;
  late FirebaseMessaging messaging;
  late Firebase firebase;
  @override
  void initState() {
    super.initState();
    fragments = [
      homeFragment,
      wishlistFragment,
      cartFragment,
      settings,
    ];

    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('launch_background');
    final InitializationSettings initializationSettings =
        InitializationSettings(android: initializationSettingsAndroid);

    if (Platform.isAndroid) {
      flutterLocalNotificationsPlugin.initialize(initializationSettings,
          onSelectNotification: (payLoad) async {
        if (payLoad != null) {
          Navigator.pushNamed(context, '/home/orders/order',
              arguments: '$payLoad');
        } else
          Navigator.pushNamed(context, '/home');
      });
    }

    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      RemoteNotification? notification = event.notification;
      AndroidNotification? android = event.notification?.android;
      if (notification != null && android != null && !kIsWeb) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channelDescription: channel.description,
                icon: 'launch_background',
              ),
            ),
            payload: notification.body!.split(' ').first.split('-').last);
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      var payload;
      try{
        payload=notification!.body!.split(' ').first.split('-').last;
          Navigator.pushNamed(context, '/home/orders/order',
              arguments: '$payload');
      }
      catch(e){
          Navigator.pushNamed(context, '/home');
      }
      
      
    });

    fetchData();
  }

  bool isLoading = true;
  bool isError = false;
  fetchData() async {
    try {
      await context.read(categoriesProvider).fetchMainCategories();
      await context.read(productProvider).fetch(
            "Agriculture & Food",
            "Health & Beauty",
            "Home, Lights & Construction",
          );
      await context.read(cartProvider).fetchCart();
      await context.read(wishlistProvider).fetchWishlist();

      setState(() {
        isLoading = false;
        isError = false;
      });
    } catch (error) {
      setState(() {
        isLoading = false;
        isError = true;
      });
    }
  }

  search() async {
    try {
      List<ProductModel> products =
          await context.read(productProvider).searchProduct(searchText);
      var featured = [];
      products.forEach((product) {
        featured.add(product);
      });
      setState(() {
        isLoading = false;
        isEmpty = products.isEmpty;
        list.clear();
        list.addAll(products);
      });
    } catch (error) {
      setState(() {
        isLoading = false;
        isEmpty = list.isEmpty;
      });
      showErrorDialog(error, context);
    }
  }

  Widget getTitle() {
    switch (selectedTab) {
      case 0:
        return Container(
          margin: const EdgeInsets.only(bottom: 8.0, top: 8),
          padding: const EdgeInsets.all(8.0),
          height: 60,
          child: TextFormField(
            textAlignVertical: TextAlignVertical.bottom,
            onChanged: (value) {
              setState(() {
                searchText = value;
              });
            },
            onFieldSubmitted: (value) {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => SearchScreen(
                    searchText: searchText,
                  ),
                ),
              );
            },
            controller: searchController,
            textInputAction: TextInputAction.search,
            style: Theme.of(context).textTheme.bodyText1,
            decoration: InputDecoration(
              hintText: AppLocalizations.of(context).translate("hint_search"),
              prefixIcon: Icon(
                Icons.search,
                color: Theme.of(context).textTheme.bodyText1!.color,
              ),
              fillColor: Theme.of(context).backgroundColor,
              filled: true,
              hintStyle: TextStyle(
                  color: Theme.of(context).textTheme.bodyText1?.color),
              border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.all(Radius.circular(0)),
              ),
              suffixIcon: IconButton(
                onPressed: () {
                  searchController.clear();
                  setState(() {
                    searchText = '';
                  });
                },
                alignment: Alignment.bottomRight,
                icon: Icon(
                  Icons.clear,
                  color: Theme.of(context).textTheme.bodyText1!.color,
                ),
              ),
            ),
            keyboardType: TextInputType.text,
            textAlign: TextAlign.start,
          ),
        );

      case 1:
        return Text(
          AppLocalizations.of(context).translate("lbl_wishlist"),
        );
      case 2:
        return Text(
          AppLocalizations.of(context).translate("lbl_cart_details"),
        );
      case 3:
        return Text(
          AppLocalizations.of(context).translate("lbl_settings"),
        );

      default:
        return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        elevation: 0,
        title: getTitle(),
        centerTitle: true,
      ),
      body: fragments[selectedTab],
      bottomNavigationBar: getBottomNav(height),
    );
  }

  Container getBottomNav(double height) {
    return Container(
      height: 50,
      decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
          border: Border(
            top: BorderSide(
              color: Theme.of(context).accentColor,
              width: 0.12,
            ),
          ),
          boxShadow: [
            BoxShadow(),
          ]),
      child: Stack(
        alignment: Alignment.centerLeft,
        children: <Widget>[
          Container(
            height: height * 0.15,
            width: double.maxFinite,
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                tabItem(0, [Icons.home, Icons.home_outlined]),
                tabItem(1, [Icons.favorite, Icons.favorite_border_outlined]),
                tabItem(2, [Icons.shopping_cart, Icons.shopping_cart_outlined]),
                tabItem(3, [Icons.menu, Icons.menu_outlined]),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget tabItem(var pos, List<IconData> icons) {
    return Consumer(builder: (context, watch, child) {
      var cartItems = watch(cartProvider).cart.items.length;
      var wishlistItems = watch(wishlistProvider).wishlist.length;

      return GestureDetector(
        onTap: () {
          selectedTab = pos;
          searchText = '';
          setState(() {});
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              width: 40,
              height: 40,
              margin: EdgeInsets.only(
                right:
                    pos == 1 || pos == 2 || pos == 3 ? spacing_standard_new : 0,
                left: pos == 0 ? spacing_standard_new : 0,
              ),
              alignment: Alignment.center,
              decoration: selectedTab == pos
                  ? BoxDecoration(
                      shape: BoxShape.circle,
                      color: Theme.of(context).primaryColor.withOpacity(0.2))
                  : BoxDecoration(),
              child: selectedTab == pos
                  ? Icon(
                      icons[0],
                      size: 30,
                    )
                  : Icon(
                      icons[1],
                      size: 28,
                    ),
            ),
            cartItems > 0 && pos == 2
                ? Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: Colors.red),
                      child: Text(
                        cartItems.toString(),
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2!
                            .copyWith(color: Colors.white),
                      ),
                    ),
                  )
                : Container(),
            wishlistItems > 0 && pos == 1
                ? Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: Colors.red),
                      child: Text(
                        wishlistItems.toString(),
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2!
                            .copyWith(color: Colors.white),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      );
    });
  }
}
