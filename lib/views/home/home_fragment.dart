import 'package:ecommerce_client/models/category.dart';
import 'package:ecommerce_client/providers/app_lang_state.dart';
import 'package:ecommerce_client/providers/categories_provider.dart';
import 'package:ecommerce_client/providers/product_provider.dart';
import 'package:ecommerce_client/providers/shared_utility.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';
import 'package:ecommerce_client/utils/widgets.dart';
import 'package:ecommerce_client/views/home/all_categories_screen.dart';
import 'package:ecommerce_client/views/home/category_screen.dart';
import 'package:ecommerce_client/views/home/all_product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/index.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shimmer/shimmer.dart';

class Home extends StatefulWidget {
  static String tag = '/HomeFragment';

  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  var categoriesList = [];
  var banners = [];
  String country = '';
  var position = 0;

  var endTime = DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day + 1)
      .millisecondsSinceEpoch;
  var countdownController;

  bool isRunning = true;

  bool isLoading = true;

  bool isError = false;

  @override
  void initState() {
    super.initState();

    countdownController = new CountdownTimerController(endTime: endTime);
    fetchData();
  }

  fetchData() async {
    try {
      categoriesList =
          await context.read(categoriesProvider).fetchMainCategories();

      country = await context.read(sharedUtilityProvider).lookupUserCountry();
      var banner = [];

      banner.addAll({bg_banner_1});
      banner.addAll({bg_banner_2});
      banner.addAll({bg_banner_3});
      setState(() {
        isLoading = false;
        isError = false;

        banners.clear();
        banners.addAll(banner);
      });
    } catch (error) {
      setState(() {
        isLoading = false;
        isError = true;
      });
    }
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: isError
          ? errorScreen(context, fetchData)
          : isLoading
              ? LoadingHomePage()
              : SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.only(bottom: 30),
                    child: Consumer(builder: (context, watch, child) {
                      return Column(
                        children: <Widget>[
                          if (country != '')
                            Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 20),
                              width: width,
                              decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.location_pin,
                                  ),
                                  Text(
                                    AppLocalizations.of(context)
                                                .locale
                                                .languageCode ==
                                            'en'
                                        ? "${AppLocalizations.of(context).translate("lbl_deliver_to")}$country"
                                        : "$country ${AppLocalizations.of(context).translate("lbl_deliver_to")}",
                                    style: TextStyle(
                                        fontSize: 20, color: Colors.white),
                                  ),
                                ],
                              ),
                            ),

                          CustomCarousel(banners: banners),
                          //today's deals
                          Padding(
                            padding: const EdgeInsets.only(
                              left: spacing_standard_new,
                              right: spacing_standard_new,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  AppLocalizations.of(context)
                                      .translate("lbl_todays_deals"),
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => AllProductScreen(
                                          prodcuts: watch(productProvider)
                                              .dailyDealProducts,
                                          title: AppLocalizations.of(context)
                                              .translate("lbl_todays_deals"),
                                        ),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        left: spacing_standard_new,
                                        top: spacing_control,
                                        bottom: spacing_control),
                                    child: Text(
                                      AppLocalizations.of(context)
                                          .translate("lbl_view_all"),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),

                          ProductHorizontalList(
                              watch(productProvider).dailyDealProducts),
                          //categories section
                          horizontalHeading(
                              context,
                              AppLocalizations.of(context)
                                  .translate("lbl_categories"), callback: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        AllCategoriesScreen()));
                          }),
                          getCategories(),
                          SizedBox(
                            height: spacing_standard,
                          ),
                          //cat0 section
                          horizontalHeading(
                              context,
                              AppLocalizations.of(context)
                                  .translate("lbl_agriculture"), callback: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CategoryScreen(
                                          category: categoriesList.firstWhere(
                                              (element) =>
                                                  element.name ==
                                                  "Agriculture & Food"),
                                        )));
                          }),
                          ProductHorizontalList(
                              watch(productProvider).category0Products),
                          //cat1 section
                          horizontalHeading(
                              context,
                              AppLocalizations.of(context).translate(
                                "lbl_health",
                              ), callback: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CategoryScreen(
                                          category: categoriesList.firstWhere(
                                              (element) =>
                                                  element.name ==
                                                  "Health & Beauty"),
                                        )));
                          }),
                          ProductHorizontalList(
                              watch(productProvider).category1Products),
                          //banner
                          CustomCarousel(banners: banners),
                          //cat2 section
                          horizontalHeading(
                              context,
                              AppLocalizations.of(context)
                                  .translate("lbl_home_lights"), callback: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CategoryScreen(
                                          category: categoriesList.firstWhere(
                                              (element) =>
                                                  element.name ==
                                                  "Home, Lights & Construction"),
                                        )));
                          }),
                          ProductHorizontalList(
                              watch(productProvider).category2Products),
                        ],
                      );
                    }),
                  ),
                ),
    );
  }

  Container getCategories() {
    var categories = [
      'agriculture-food',
      'appareltextiles-accessories',
      'books',
      'electronics',
      'health-beauty',
      'home-lights-construction',
      'machinery-industrial-parts-tools',
      'packaging-advertising-office',
    ];
    List<dynamic> categoriesList = this
        .categoriesList
        .where((element) => categories.contains(element.slug))
        .toList();
    return Container(
      height: 250,
      margin: EdgeInsets.only(top: spacing_standard_new),
      alignment: Alignment.topLeft,
      child: ListView.builder(
        itemCount: categoriesList.length,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        padding:
            EdgeInsets.only(left: spacing_standard, right: spacing_standard),
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          CategoryScreen(category: categoriesList[index])));
            },
            child: Container(
              margin: EdgeInsets.only(
                  left: spacing_standard, right: spacing_standard),
              decoration: BoxDecoration(
                  border: Border.all(color: Theme.of(context).backgroundColor)),
              width: 210,
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(spacing_middle),
                    child: categoriesList[index].image == null
                        ? Container(
                            width: 200,
                            height: 100,
                          )
                        : Image.asset(
                            categoriesList[index].image,
                            width: 200,
                            height: 180,
                            fit: BoxFit.cover,
                          ),
                  ),
                  SizedBox(height: spacing_control),
                  Expanded(
                    child: Text(
                      AppLocalizations.of(context).locale.languageCode == 'en'
                          ? categoriesList[index].name
                          : categoriesList[index].nameAm,
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class LoadingHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      child: Column(
        children: <Widget>[
          Expanded(
            child: Shimmer.fromColors(
              baseColor: Theme.of(context).backgroundColor,
              highlightColor: Colors.grey.shade100,
              child: ListView.builder(
                itemCount: 5,
                itemBuilder: (_, index) => Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: index == 0
                      ? Container(
                          height: height * 0.25,
                          margin: EdgeInsets.all(spacing_standard_new),
                          color: Theme.of(context).backgroundColor,
                        )
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  padding: EdgeInsets.all(spacing_standard),
                                  height: spacing_standard_new,
                                  width: width * 0.4,
                                  color: Theme.of(context).backgroundColor,
                                ),
                                Container(
                                  height: spacing_standard_new,
                                  width: width * 0.3,
                                  color: Theme.of(context).backgroundColor,
                                ),
                              ],
                            ),
                            const Padding(
                              padding: EdgeInsets.symmetric(vertical: 2.0),
                            ),
                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  ...[0, 1, 2].map(
                                    (e) => Container(
                                      padding: EdgeInsets.all(spacing_standard),
                                      width: width * 0.4,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            height: 180,
                                            color: Theme.of(context)
                                                .backgroundColor,
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: spacing_standard),
                                          ),
                                          Container(
                                            height: spacing_standard_new,
                                            width: width * 0.3,
                                            color: Theme.of(context)
                                                .backgroundColor,
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 2.0),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
