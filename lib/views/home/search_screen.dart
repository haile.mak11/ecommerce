import 'package:ecommerce_client/models/product.dart';
import 'package:ecommerce_client/providers/product_provider.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';

import 'package:ecommerce_client/utils/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:ecommerce_client/utils/colors.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../product_detail_screen.dart';

class SearchScreen extends StatefulWidget {
  static String tag = '/SearchScreen';
  final searchText;

  SearchScreen({this.searchText});
  @override
  SearchScreenState createState() => SearchScreenState();
}

class SearchScreenState extends State<SearchScreen> {
  TextEditingController searchController = TextEditingController();
  var list = [];
  bool isLoadingMoreData = false;
  bool isEmpty = false;
  var searchText = "";

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    searchText = widget.searchText;
    searchController.text = searchText;
    fetchData();
  }

  void updateText(String text) {
    setState(() {
      searchText = text;
    });
    fetchData();
  }

  fetchData() async {
    try {
      setState(() {
        isLoading = true;
      });
      List<ProductModel> products =
          await context.read(productProvider).searchProduct(searchText);
      var featured = [];
      products.forEach((product) {
        featured.add(product);
      });
      setState(() {
        isLoading = false;
        list.clear();
        list.addAll(products);
      });
    } catch (error) {
      setState(() {
        isLoading = false;
      });
      showErrorDialog(error, context);
      print(error);
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var searchList = ListView.builder(
      itemCount: list.length,
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ProductDetail(product: list[index])));
          },
          child: Container(
            padding: EdgeInsets.all(10.0),
            child: IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(1),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Theme.of(context).backgroundColor,
                            width: 1)),
                    child: Image.network(list[index].thumbnail,
                        fit: BoxFit.cover,
                        height: width * 0.35,
                        width: width * 0.29),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          AppLocalizations.of(context).locale.languageCode ==
                                  'en'
                              ? list[index].name
                              : list[index].amharic.name,
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        SizedBox(height: 4),
                        Row(
                          children: <Widget>[
                            Text(
                              list[index].deal.hasDeal
                                  ? list[index]
                                      .deal
                                      .totalUnitPrice
                                      .toString()
                                      .toCurrencyString()
                                  : list[index]
                                      .totalUnitPrice
                                      .toString()
                                      .toCurrencyString(),
                              style: Theme.of(context).textTheme.bodyText2,
                            ),
                            SizedBox(
                              width: spacing_control,
                            ),
                            if (list[index].deal.hasDeal)
                              Text(
                                list[index]
                                    .totalUnitPrice
                                    .toString()
                                    .toCurrencyString(),
                                style: TextStyle(
                                    color: textColorSecondary,
                                    decoration: TextDecoration.lineThrough),
                              ),
                          ],
                        ),
                        SizedBox(
                          height: spacing_standard,
                        ),
                        Expanded(
                          child: Text(
                            AppLocalizations.of(context).locale.languageCode ==
                                    'en'
                                ? list[index].description
                                : list[index].amharic.description,
                            maxLines: 2,
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                        ),
                        SizedBox(height: 4),
                        Expanded(
                          child: Align(
                            alignment: Alignment.bottomLeft,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                RatingBar.builder(
                                  initialRating: list[index].rating.average !=
                                          "null"
                                      ? double.parse(list[index].rating.average)
                                      : 0,
                                  direction: Axis.horizontal,
                                  allowHalfRating: true,
                                  tapOnlyMode: true,
                                  itemCount: 5,
                                  itemSize: 16,
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.amber,
                                  ),
                                  onRatingUpdate: (rating) {},
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 4),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Container(
          margin: const EdgeInsets.only(bottom: 8.0, top: 8),
          padding: const EdgeInsets.all(8.0),
          height: 60,
          child: TextFormField(
            textAlignVertical: TextAlignVertical.bottom,
            onFieldSubmitted: (value) {
              setState(() {
                searchText = value;
                isEmpty = false;
                isLoadingMoreData = true;
              });
              fetchData();
            },
            controller: searchController,
            textInputAction: TextInputAction.search,
            style: TextStyle(fontSize: textSizeMedium, color: textColorPrimary),
            decoration: InputDecoration(
              hintText: "Search",
              fillColor: white,
              filled: true,
              border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.all(Radius.circular(0)),
              ),
              suffixIcon: IconButton(
                onPressed: () {
                  searchController.clear();
                  list.clear();
                  isEmpty = false;
                  isLoadingMoreData = false;
                },
                alignment: Alignment.bottomRight,
                icon: Icon(Icons.clear),
              ),
            ),
            keyboardType: TextInputType.text,
            textAlign: TextAlign.start,
          ),
        ),
      ),
      body: isLoading
          ? LoadingListView()
          : SingleChildScrollView(
              child: !isEmpty
                  ? Column(
                      children: [
                        searchList,
                      ],
                    )
                  : Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            height: 80,
                          ),
                          Text(
                              AppLocalizations.of(context)
                                  .translate("error_no_result"),
                              style: Theme.of(context).textTheme.headline4),
                          Text(
                              AppLocalizations.of(context)
                                  .translate("error_no_match"),
                              style: Theme.of(context).textTheme.headline6)
                        ],
                      ),
                    ),
            ),
    );
  }
}
