import 'dart:async';

import 'package:ecommerce_client/utils/constants.dart';
import 'package:ecommerce_client/utils/shared_prefs.dart';

import 'package:ecommerce_client/views/walkthrough.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'home/home_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    SharedPrefs.init(context);
    Timer(Duration(seconds: 1), goToNextPage);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Theme.of(context).backgroundColor,
        child: Padding(
          padding: const EdgeInsets.only(top: 80),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 200,
                        child: Image.asset(
                          ic_logo,
                          cacheWidth: 200,
                          cacheHeight: 251,
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      SpinKitCircle(
                        color: Theme.of(context).textTheme.subtitle1?.color,
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(spacing_large),
                  child: Text("Powered by Aleph"),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void goToNextPage() async {
    if (await SharedPrefs.isWalkThroughShown()) {
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) {
        return ProviderScope(
          child: HomeScreen(),
        );
      }));
    } else {
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) {
        return ProviderScope(child: WalkThroughScreen());
      }));
    }
  }
}
