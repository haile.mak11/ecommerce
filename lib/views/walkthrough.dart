import 'package:carousel_slider/carousel_slider.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';
import 'package:ecommerce_client/utils/colors.dart';
import 'package:ecommerce_client/utils/shared_prefs.dart';
import 'package:ecommerce_client/views/auth/login_screen.dart';
import 'package:ecommerce_client/views/home/home_screen.dart';

import 'package:flutter/material.dart';

import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter_svg/svg.dart';

class WalkThroughScreen extends StatefulWidget {
  @override
  _WalkThroughScreenState createState() => _WalkThroughScreenState();
}

class _WalkThroughScreenState extends State<WalkThroughScreen> {
  var sliderList = <String>[walkthrough_1, walkthrough_2, walkthrough_3];
  //var mHeadingList = <String>[];

  int currentIndexPage = 0;
  int pageLength = 3;
  final _controller = new CarouselController();

  @override
  void initState() {
    super.initState();
  }

  void setWalkthroughState(value) {
    setState(() {
      _controller.animateToPage(value.toInt());
      currentIndexPage = value;
    });
  }

  void goToSignIn() {
    SharedPrefs.setWalkThroughShown(true);
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (ctx) => LoginScreen()));
  }

  void goToHomeScreen() {
    SharedPrefs.setWalkThroughShown(true);
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (ctx) => HomeScreen()));
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    width = width - 50;
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              height: 100,
              margin: EdgeInsets.fromLTRB(spacing_large, spacing_large,
                  spacing_large, spacing_standard_new),
              child: Column(
                children: <Widget>[
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          currentIndexPage == 0
                              ? AppLocalizations.of(context)
                                  .translate("lbl_walkthrough_1")
                              : currentIndexPage == 1
                                  ? AppLocalizations.of(context)
                                      .translate("lbl_walkthrough_2")
                                  : AppLocalizations.of(context)
                                      .translate("lbl_walkthrough_3"),
                          style: Theme.of(context).textTheme.headline4,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            CarouselSlider(
              carouselController: _controller,
              options: CarouselOptions(
                enableInfiniteScroll: false,
                onPageChanged: (index, reason) {
                  setWalkthroughState(index);
                },
                viewportFraction: 1,
                enlargeCenterPage: false,
                scrollDirection: Axis.horizontal,
              ),
              items: sliderList.map((slider) {
                return Builder(
                  builder: (BuildContext context) {
                    return Container(
                      width: width,
                      margin: EdgeInsets.all(spacing_standard_new),
                      child: Center(
                          child: SvgPicture.asset(slider,
                              width: MediaQuery.of(context).size.width,
                              fit: BoxFit.cover)),
                    );
                  },
                );
              }).toList(),
            ),
            Padding(
              padding: const EdgeInsets.all(spacing_large),
              child: Column(
                children: <Widget>[
                  DotsIndicator(
                    dotsCount: 3,
                    position: currentIndexPage.toDouble(),
                    decorator: DotsDecorator(
                        color: view_color,
                        activeColor: Theme.of(context).primaryColor,
                        activeSize: const Size.square(14.0),
                        spacing: EdgeInsets.all(spacing_control)),
                  ),
                  SizedBox(height: spacing_standard),
                  if (currentIndexPage != pageLength - 1)
                    Column(
                      children: [
                        SizedBox(
                          width: double.infinity,
                          height: 50,
                          child: MaterialButton(
                            padding: EdgeInsets.all(spacing_standard),
                            child: Text(
                                AppLocalizations.of(context)
                                    .translate("lbl_continue"),
                                style: TextStyle(fontSize: 18)),
                            textColor: white,
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(40.0)),
                            color: Theme.of(context).primaryColor,
                            onPressed: () {
                              setWalkthroughState(currentIndexPage + 1);
                            },
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            currentIndexPage = pageLength - 1;
                            _controller.jumpToPage(pageLength - 1);
                          },
                          child: Container(
                            child: Text(
                              AppLocalizations.of(context)
                                  .translate("lbl_skip"),
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                            padding: EdgeInsets.all(16),
                          ),
                        ),
                      ],
                    )
                  else
                    Column(
                      children: [
                        SizedBox(
                          width: double.infinity,
                          height: 50,
                          child: MaterialButton(
                            padding: EdgeInsets.all(spacing_standard),
                            child: Text(
                                AppLocalizations.of(context)
                                    .translate("text_start_shopping"),
                                style: TextStyle(fontSize: 18)),
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(40.0)),
                            color: Theme.of(context).primaryColor,
                            onPressed: () {
                              goToHomeScreen();
                            },
                          ),
                        ),
                        TextButton(
                            onPressed: () => goToSignIn(),
                            child: Container(
                              child: Text(
                                AppLocalizations.of(context).translate(
                                        "lbl_already_have_an_account") +
                                    AppLocalizations.of(context)
                                        .translate("lbl_sign_in"),
                                textAlign: TextAlign.center,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              padding: EdgeInsets.all(16),
                            ))
                      ],
                    ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
