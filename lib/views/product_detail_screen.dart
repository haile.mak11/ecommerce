import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerce_client/models/cart.dart';
import 'package:ecommerce_client/models/product.dart';
import 'package:ecommerce_client/models/review.dart';
import 'package:ecommerce_client/providers/cart_provider.dart';
import 'package:ecommerce_client/providers/product_provider.dart';
import 'package:ecommerce_client/providers/wishlist_provider.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';
import 'package:ecommerce_client/utils/widgets.dart';
import 'package:ecommerce_client/views/order/make_order_screen.dart';
import 'package:flutter/material.dart';

import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'package:ecommerce_client/utils/colors.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'cart_screen.dart';

class ProductDetail extends StatefulWidget {
  static final String tag = '/ProductDetail';
  final ProductModel product;

  ProductDetail({required this.product});

  @override
  ProductDetailState createState() => ProductDetailState();
}

class ProductDetailState extends State<ProductDetail> {
  int quantity = 1;
  var position = 0;
  bool isExpanded = false;
  var selections = {};
  double fiveStar = 0;
  double fourStar = 0;
  double threeStar = 0;
  double twoStar = 0;
  double oneStar = 0;
  List<ReviewModel> reviewList = [];
  late int endTime;

  void addToCart() async {
    try {
      Map variation = widget.product.variations;
      await context.read(cartProvider).addToCart(
            product: widget.product,
            quantity: quantity,
            variations: selections.map((key, value) {
              return MapEntry(key, variation[key][value]);
            }),
          );
    } catch (error) {
      showErrorDialog(error, context);
    }
  }

  void addToWishlist(bool isFavourite) async {
    try {
      !isFavourite
          ? await context.read(wishlistProvider).addToWishlist(
                product: widget.product,
              )
          : await context.read(wishlistProvider).removeItem(
                product: widget.product,
              );
    } catch (error) {
      showErrorDialog(error, context);
    }
  }

  @override
  void initState() {
    super.initState();
    fetchData();
    var updatedAt = widget.product.deal.updatedAt ?? DateTime.now();
    endTime = DateTime(updatedAt.year, updatedAt.month, updatedAt.day + 1,
            updatedAt.hour, updatedAt.minute, updatedAt.second)
        .millisecondsSinceEpoch;
  }

  void fetchData() async {
    try {
      //init selections
      selections = widget.product.variations.map((key, value) {
        return MapEntry("$key", 0);
      });
      //load review
      var reviews = await context
          .read(productProvider)
          .getProductReviews(widget.product.id);
      setState(() {
        reviewList.clear();
        reviewList.addAll(reviews);
      });
      setRating();
    } catch (error) {
      showErrorDialog(error, context);
    }
  }

  setRating() {
    reviewList.forEach((review) {
      switch (review.stars) {
        case 5:
          fiveStar++;
          break;
        case 4:
          fourStar++;
          break;
        case 3:
          threeStar++;
          break;
        case 2:
          twoStar++;
          break;
        case 1:
          oneStar++;
          break;
      }
    });
    if (reviewList.length > 0) {
      fiveStar = (fiveStar * 100) / reviewList.length;
      fourStar = (fourStar * 100) / reviewList.length;
      threeStar = (threeStar * 100) / reviewList.length;
      twoStar = (twoStar * 100) / reviewList.length;
      oneStar = (oneStar * 100) / reviewList.length;
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    var sliderImages = Container(
      height: 380,
      child: PageView.builder(
        itemCount: widget.product.photosCount,
        itemBuilder: (context, index) {
          return CachedNetworkImage(
              imageUrl: context
                      .read(productProvider)
                      .getProductImage(id: widget.product.id) +
                  index.toString(),
              width: width,
              height: width * 1.05,
              fit: BoxFit.cover);
        },
        onPageChanged: (index) {
          position = index;
          setState(() {});
        },
      ),
    );

    var productInfo = Padding(
      padding: EdgeInsets.only(top: 14, left: 14, right: 14),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Text(
                  AppLocalizations.of(context).locale.languageCode == 'en'
                      ? widget.product.name
                      : widget.product.amharic.name,
                  style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: Colors.white, fontWeight: FontWeight.normal),
                  maxLines: 1,
                ),
              ),
              Text(
                widget.product.deal.hasDeal
                    ? widget.product.deal.totalUnitPrice!.toCurrencyString()
                    : widget.product.totalUnitPrice!.toCurrencyString(),
                style: Theme.of(context)
                    .textTheme
                    .headline6!
                    .copyWith(color: Colors.white),
              ),
            ],
          ),
          SizedBox(height: spacing_standard),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                                left: 12, right: 12, top: 0, bottom: 0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(spacing_standard_new)),
                              color: double.parse(
                                          widget.product.rating.average) <
                                      2
                                  ? Colors.red
                                  : double.parse(
                                              widget.product.rating.average) <
                                          4
                                      ? Colors.orange
                                      : Colors.green,
                            ),
                            child: Row(
                              children: <Widget>[
                                Text(widget.product.rating.average,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline5!
                                        .copyWith(color: Colors.white)),
                                SizedBox(width: spacing_control_half),
                                Icon(Icons.star, color: white, size: 18),
                              ],
                            ),
                          ),
                          SizedBox(width: spacing_standard),
                          Text(
                            reviewList.length.toString() +
                                " " +
                                AppLocalizations.of(context)
                                    .translate("lbl_reviews"),
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        SizedBox(width: spacing_control_half),
                        if (widget.product.deal.hasDeal)
                          Text(
                            widget.product.totalUnitPrice!.toCurrencyString(),
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.normal,
                                    decoration: TextDecoration.lineThrough),
                          ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );

    var reviews = ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: reviewList.length,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        var initials =
            reviewList[index].name.toString().split(' ').map((e) => e[0]);
        return Container(
          margin: EdgeInsets.only(bottom: spacing_standard_new),
          padding: EdgeInsets.all(spacing_control),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding:
                        EdgeInsets.only(left: 12, right: 12, top: 1, bottom: 1),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                            Radius.circular(spacing_standard_new)),
                        color: reviewList[index].stars < 2
                            ? Colors.red
                            : reviewList[index].stars < 4
                                ? Colors.orange
                                : Colors.green),
                    child: Row(
                      children: <Widget>[
                        Text(reviewList[index].stars.toString()),
                        SizedBox(width: spacing_control_half),
                        Icon(Icons.star, color: white, size: 12)
                      ],
                    ),
                  ),
                  SizedBox(width: spacing_standard_new),
                  Text(
                    reviewList[index].name,
                  )
                ],
              ),
              SizedBox(height: spacing_standard),
              Row(
                children: [
                  Container(
                    child: Text(
                      '${initials.first}${initials.last}',
                      style: Theme.of(context)
                          .textTheme
                          .headline6!
                          .copyWith(fontWeight: FontWeight.normal),
                    ),
                    width: 50,
                    height: 50,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Theme.of(context).backgroundColor,
                    ),
                  ),
                  SizedBox(
                    width: spacing_large,
                  ),
                  Expanded(
                    child: Text(
                      reviewList[index].review,
                    ),
                  ),
                ],
              ),
              SizedBox(height: spacing_standard),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    reviewList[index].updatedAt,
                  )
                ],
              )
            ],
          ),
        );
      },
    );

    var priceDetail = Container(
      height: 80,
      margin: EdgeInsets.all(spacing_standard_new),
      child: ListView(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              if (widget.product.totalRange1Price != null)
                Container(
                  margin: EdgeInsets.all(spacing_control_half),
                  child: Column(
                    children: [
                      Text(
                        "${widget.product.range1} ${AppLocalizations.of(context).translate("lbl_pieces")}",
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      if (widget.product.range1PriceDiscountPercentage != '0')
                        Text(
                          "${widget.product.range1PriceDiscountPercentage}% ${AppLocalizations.of(context).translate("lbl_off")}",
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: Colors.red),
                        ),
                      Row(
                        children: [
                          Text(
                            widget.product.totalRange1Price!.toCurrencyString(),
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(
                                    color: widget.product.deal.hasDeal
                                        ? Colors.grey
                                        : Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .color,
                                    decoration: widget.product.deal.hasDeal
                                        ? TextDecoration.lineThrough
                                        : null),
                          ),
                          SizedBox(width: spacing_control_half),
                          if (widget.product.deal.hasDeal)
                            Text(
                              widget.product.deal.hasDeal
                                  ? widget.product.deal.totalRange1Price!
                                      .toCurrencyString()
                                  : '',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .copyWith(
                                      color: Theme.of(context).primaryColor),
                            ),
                        ],
                      ),
                    ],
                  ),
                ),
              if (widget.product.totalRange2Price != null)
                Container(
                  margin: EdgeInsets.all(spacing_control_half),
                  child: Column(
                    children: [
                      Text(
                        "${widget.product.range2} ${AppLocalizations.of(context).translate("lbl_pieces")}",
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      if (widget.product.range1PriceDiscountPercentage != '0')
                        Text(
                          "${widget.product.range2PriceDiscountPercentage}% ${AppLocalizations.of(context).translate("lbl_off")}",
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: Colors.red),
                        ),
                      Row(
                        children: [
                          Text(
                            widget.product.totalRange2Price!.toCurrencyString(),
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(
                                    color: widget.product.deal.hasDeal
                                        ? Colors.grey
                                        : Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .color,
                                    decoration: widget.product.deal.hasDeal
                                        ? TextDecoration.lineThrough
                                        : null),
                          ),
                          SizedBox(width: spacing_control_half),
                          if (widget.product.deal.hasDeal)
                            Text(
                              widget.product.deal.hasDeal
                                  ? widget.product.deal.totalRange2Price!
                                      .toCurrencyString()
                                  : '',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .copyWith(
                                      color: Theme.of(context).primaryColor),
                            ),
                        ],
                      ),
                    ],
                  ),
                ),
              if (widget.product.totalRange3Price != null)
                Container(
                  margin: EdgeInsets.all(spacing_control_half),
                  child: Column(
                    children: [
                      Text(
                        "${widget.product.range3} ${AppLocalizations.of(context).translate("lbl_pieces")}",
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      if (widget.product.range1PriceDiscountPercentage != '0')
                        Text(
                          "${widget.product.range3PriceDiscountPercentage}% ${AppLocalizations.of(context).translate("lbl_off")}",
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: Colors.red),
                        ),
                      Row(
                        children: [
                          Text(
                            widget.product.totalRange3Price!.toCurrencyString(),
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(
                                    color: widget.product.deal.hasDeal
                                        ? Colors.grey
                                        : Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .color,
                                    decoration: widget.product.deal.hasDeal
                                        ? TextDecoration.lineThrough
                                        : null),
                          ),
                          SizedBox(width: spacing_control_half),
                          if (widget.product.deal.hasDeal)
                            Text(
                              widget.product.deal.hasDeal
                                  ? widget.product.deal.totalRange3Price!
                                      .toCurrencyString()
                                  : '',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .copyWith(
                                      color: Theme.of(context).primaryColor),
                            ),
                        ],
                      ),
                    ],
                  ),
                ),
            ],
          ),
        ],
      ),
    );
    var descriptionSection = Container(
      margin: EdgeInsets.all(spacing_standard_new),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: spacing_standard),
          if (widget.product.deal.hasDeal)
            Container(
              decoration:
                  BoxDecoration(color: Theme.of(context).backgroundColor),
              width: width - 30,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(),
                    child: CountdownTimer(
                      endTime: endTime,
                      widgetBuilder: (_, time) {
                        if (time == null) {
                          return Container(
                            margin:
                                EdgeInsets.symmetric(vertical: spacing_large),
                            child: Text(
                              ' ${AppLocalizations.of(context).translate("lbl_deal_ended")} ',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .copyWith(
                                      color: Colors.red,
                                      fontWeight: FontWeight.normal),
                            ),
                          );
                        }
                        return Container(
                          margin: EdgeInsets.symmetric(vertical: spacing_large),
                          padding:
                              EdgeInsets.symmetric(horizontal: spacing_control),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: width / 2,
                                child: Text(
                                  AppLocalizations.of(context)
                                      .translate("lbl_dont_miss_out"),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6!
                                      .copyWith(fontWeight: FontWeight.normal),
                                ),
                              ),
                              SizedBox(
                                height: spacing_standard,
                              ),
                              Container(
                                width: width - 40,
                                child: Text(
                                  ' ${time.days ?? '00'} : ${time.hours ?? '00'} : ${time.min ?? '00'} : ${time.sec ?? '00'} ',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6!
                                      .copyWith(
                                          color:
                                              Theme.of(context).primaryColor),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          SizedBox(height: spacing_standard),
          Stack(
            alignment: Alignment.bottomRight,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).locale.languageCode == 'en'
                    ? widget.product.description
                    : widget.product.amharic.description,
                maxLines: 5,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ],
          ),
          SizedBox(height: spacing_standard_new),
          Row(
            children: [
              Icon(
                Icons.info_outlined,
                size: 18,
              ),
              Expanded(
                child: Text(
                    AppLocalizations.of(context)
                        .translate("lbl_contact_for_more_info"),
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2!
                        .copyWith(fontStyle: FontStyle.italic)),
              ),
            ],
          ),
          SizedBox(height: spacing_standard_new),
          Row(
            children: <Widget>[
              Text(
                "${AppLocalizations.of(context).translate("lbl_quantity")}: ",
                style: Theme.of(context)
                    .textTheme
                    .headline6!
                    .copyWith(fontWeight: FontWeight.normal),
              ),
              QuantityButton(
                  quantity,
                  (value) => setState(() {
                        quantity = value;
                      })),
              SizedBox(
                width: spacing_standard_new,
              ),
            ],
          ),
          ...widget.product.variations.entries.map((entry) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: spacing_standard_new,
                ),
                Text(
                  entry.key.toString(),
                  style: Theme.of(context).textTheme.headline6,
                ),
                SizedBox(height: spacing_standard_new),
                Container(
                  height: 50,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: entry.value.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          selections["${entry.key}"] = index;
                          setState(() {});
                        },
                        child: Container(
                            width: entry.key == 'size' ? 40 : null,
                            //height: 20,
                            padding: EdgeInsets.all(4),
                            margin: EdgeInsets.only(
                                right: spacing_standard_new,
                                top: spacing_control,
                                bottom: spacing_control),
                            decoration: selections["${entry.key}"] == index
                                ? BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    border: Border.all(
                                        color: Theme.of(context).primaryColor,
                                        width: 1),
                                    color: transparent)
                                : BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    border: Border.all(
                                        color: Colors.grey, width: 1),
                                    color: transparent),
                            child: Center(
                              child: Text(
                                entry.value[index],
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(
                                        color:
                                            selections["${entry.key}"] == index
                                                ? Theme.of(context).primaryColor
                                                : Theme.of(context)
                                                    .textTheme
                                                    .bodyText1!
                                                    .color),
                              ),
                            )),
                      );
                    },
                  ),
                )
              ],
            );
          }),
        ],
      ),
    );
    var reviewsSection = Container(
      padding: EdgeInsets.only(bottom: 60),
      margin: EdgeInsets.only(left: 16, top: 20, right: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(spacing_standard_new),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: width * 0.33,
                  width: width * 0.33,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Theme.of(context).backgroundColor),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      reviewText(
                        widget.product.rating.average,
                        size: 28.0,
                        fontSize: 30.0,
                      ),
                      Text(
                        '${reviewList.length.toString()} ${AppLocalizations.of(context).translate("lbl_reviews")}',
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: spacing_standard_new,
                ),
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          reviewText(5),
                          ratingProgress(fiveStar, Colors.green)
                        ],
                      ),
                      SizedBox(
                        height: spacing_control_half,
                      ),
                      Row(
                        children: <Widget>[
                          reviewText(4),
                          ratingProgress(fourStar, Colors.green)
                        ],
                      ),
                      SizedBox(
                        height: spacing_control_half,
                      ),
                      Row(
                        children: <Widget>[
                          reviewText(3),
                          ratingProgress(threeStar, Colors.amber)
                        ],
                      ),
                      SizedBox(
                        height: spacing_control_half,
                      ),
                      Row(
                        children: <Widget>[
                          reviewText(2),
                          ratingProgress(twoStar, Colors.amber)
                        ],
                      ),
                      SizedBox(
                        height: spacing_control_half,
                      ),
                      Row(
                        children: <Widget>[
                          reviewText(1),
                          ratingProgress(oneStar, Colors.red)
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: spacing_standard_new,
          ),
          Divider(
            height: 1,
          ),
          SizedBox(
            height: spacing_standard_new,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                reviewList.isEmpty
                    ? AppLocalizations.of(context).translate("lbl_no_reviews")
                    : AppLocalizations.of(context).translate("lbl_reviews"),
                style: Theme.of(context)
                    .textTheme
                    .headline6!
                    .copyWith(fontWeight: FontWeight.normal),
              ),
              MaterialButton(
                textColor: Theme.of(context).primaryColor,
                padding: EdgeInsets.only(
                    left: spacing_standard_new,
                    right: spacing_standard_new,
                    top: 0,
                    bottom: 0),
                child: Text(
                  AppLocalizations.of(context).translate("lbl_rate_now"),
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(spacing_large),
                  side: BorderSide(color: Theme.of(context).primaryColor),
                ),
                onPressed: () {
                  showRatingDialog(context, widget.product.id);
                },
              )
            ],
          ),
          reviews
        ],
      ),
    );
    var bottomButtons = Container(
      height: 50,
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
            color: Colors.grey.withOpacity(0.7),
            blurRadius: 16,
            spreadRadius: 2,
            offset: Offset(3, 1))
      ], color: Colors.white),
      child: Row(
        children: <Widget>[
          Expanded(
            child: InkWell(
              onTap: () {
                Map variation = widget.product.variations;
                var item = Item(
                    product: widget.product,
                    quantity: quantity,
                    variations: selections.isEmpty
                        ? []
                        : [
                            {
                              "$quantity": selections.map((key, value) {
                                return MapEntry(key, variation[key][value]);
                              })
                            }
                          ]);
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (ctx) => MakeOrderRequest(item: item),
                  ),
                );
              },
              child: Container(
                child: Text(
                  AppLocalizations.of(context).translate("lbl_buy_now"),
                  style: Theme.of(context)
                      .textTheme
                      .headline6!
                      .copyWith(fontWeight: FontWeight.normal),
                ),
                color: Theme.of(context).backgroundColor,
                alignment: Alignment.center,
                height: double.infinity,
              ),
            ),
          ),
          Expanded(
            child: InkWell(
              onTap: () => addToCart(),
              child: Container(
                child: Text(
                  AppLocalizations.of(context).translate("lbl_add_to_cart"),
                  style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: Colors.white, fontWeight: FontWeight.normal),
                ),
                color: Theme.of(context).primaryColor,
                alignment: Alignment.center,
                height: double.infinity,
              ),
            ),
          )
        ],
      ),
    );

    return Scaffold(
      body: Stack(
        alignment: Alignment.bottomLeft,
        children: <Widget>[
          DefaultTabController(
            length: 2,
            child: NestedScrollView(
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverAppBar(
                    expandedHeight: 450,
                    floating: false,
                    pinned: true,
                    titleSpacing: 0,
                    actions: <Widget>[
                      Consumer(builder: (context, watch, child) {
                        var cartItems = watch(cartProvider).cart.items.length;
                        var isFavourite =
                            watch(wishlistProvider).isFavourite(widget.product);

                        return Row(
                          children: [
                            InkWell(
                              onTap: () {
                                addToWishlist(isFavourite);
                              },
                              child: Container(
                                padding: EdgeInsets.all(spacing_standard),
                                margin: EdgeInsets.only(
                                    right: spacing_standard_new),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.grey.withOpacity(0.5)),
                                child: isFavourite
                                    ? Icon(Icons.favorite_rounded,
                                        color: Colors.red, size: 20)
                                    : Icon(Icons.favorite_border, size: 20),
                              ),
                            ),
                            InkWell(
                              child: cartIcon(context, cartItems),
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                  builder: (ctx) => CartScreen(
                                    isHome: false,
                                  ),
                                ));
                              },
                              radius: spacing_standard_new,
                            ),
                          ],
                        );
                      })
                    ],
                    title: Text(
                      innerBoxIsScrolled
                          ? AppLocalizations.of(context).locale.languageCode ==
                                  'en'
                              ? widget.product.name
                              : widget.product.amharic.name
                          : "",
                    ),
                    flexibleSpace: FlexibleSpaceBar(
                      background: Column(
                        children: <Widget>[
                          sliderImages,
                          productInfo,
                        ],
                      ),
                      collapseMode: CollapseMode.pin,
                    ),
                  ),
                ];
              },
              body: SingleChildScrollView(
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  if (widget.product.totalRange1Price != null &&
                      widget.product.totalRange2Price != null &&
                      widget.product.totalRange3Price != null)
                    priceDetail,
                  descriptionSection,
                  reviewsSection,
                ],
              )),
            ),
          ),
          bottomButtons
        ],
      ),
    );
  }

  Widget reviewText(rating,
      {size = 15.0, fontSize = textSizeLargeMedium, textColor = black}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          rating.toString(),
          style: Theme.of(context).textTheme.headline5,
        ),
        SizedBox(width: spacing_control),
        Icon(Icons.star, color: Colors.amber, size: size)
      ],
    );
  }

  Widget ratingProgress(double value, color) {
    return Expanded(
      child: LinearPercentIndicator(
        lineHeight: 10.0,
        percent: value / 100.0,
        linearStrokeCap: LinearStrokeCap.roundAll,
        backgroundColor: Colors.grey.withOpacity(0.2),
        progressColor: color,
      ),
    );
  }

  void showRatingDialog(BuildContext context, int id) {
    TextEditingController reviewController = TextEditingController();
    int rating = 5;
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    showDialog<ConfirmAction>(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return Scaffold(
          backgroundColor: Colors.transparent,
          body: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(16),
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).size.width - 40,
                    decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                    ),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            AppLocalizations.of(context)
                                .translate("hint_review"),
                            style: Theme.of(context).textTheme.headline6,
                          ),
                        ),
                        Divider(
                          thickness: 0.5,
                        ),
                        Padding(
                            padding: const EdgeInsets.all(spacing_large),
                            child: RatingBar.builder(
                              tapOnlyMode: true,
                              initialRating: rating.toDouble(),
                              minRating: 1,
                              direction: Axis.horizontal,
                              itemCount: 5,
                              itemPadding:
                                  EdgeInsets.symmetric(horizontal: 4.0),
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              onRatingUpdate: (double value) {
                                rating = value.toInt();
                              },
                            )),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: spacing_large, right: spacing_large),
                          child: Form(
                            key: _formKey,
                            autovalidateMode: AutovalidateMode.always,
                            child: TextFormField(
                              controller: reviewController,
                              keyboardType: TextInputType.multiline,
                              maxLines: 5,
                              validator: (value) {
                                return value!.isEmpty
                                    ? AppLocalizations.of(context)
                                        .translate("error_field_required")
                                    : null;
                              },
                              decoration: new InputDecoration(
                                hintText: AppLocalizations.of(context)
                                    .translate("hint_describe"),
                                border: InputBorder.none,
                                enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 1),
                                ),
                                filled: false,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(spacing_large),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Expanded(
                                child: MaterialButton(
                                  textColor: Theme.of(context).primaryColor,
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .translate("lbl_cancel"),
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  ),
                                  onPressed: () {
                                    Navigator.of(context)
                                        .pop(ConfirmAction.CANCEL);
                                  },
                                ),
                              ),
                              SizedBox(
                                width: spacing_standard_new,
                              ),
                              Expanded(
                                child: MaterialButton(
                                  color: Theme.of(context).primaryColor,
                                  textColor: Colors.white,
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .translate("lbl_submit"),
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  ),
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(5.0),
                                  ),
                                  onPressed: () async {
                                    final form = _formKey.currentState;
                                    if (form!.validate()) {
                                      form.save();
                                      var formdata = {
                                        'stars': rating,
                                        'review': reviewController.value.text
                                      };
                                      try {
                                        var review = await context
                                            .read(productProvider)
                                            .addReview(id, formdata);
                                        reviewList.add(review);
                                        Navigator.of(context).pop();
                                      } catch (e) {
                                        showErrorDialog(e, context);
                                      }
                                    } else {}
                                  },
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
