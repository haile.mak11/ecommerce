import 'package:ecommerce_client/models/cart.dart';
import 'package:ecommerce_client/models/order.dart';
import 'package:ecommerce_client/providers/cart_provider.dart';
import 'package:ecommerce_client/providers/order_provider.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';

import 'package:ecommerce_client/utils/widgets.dart';
import 'package:ecommerce_client/views/home/home_screen.dart';
import 'package:ecommerce_client/views/items_fragment.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ecommerce_client/utils/colors.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';

class MakeOrderRequest extends StatefulWidget {
  static final String tag = '/OrderSummaryScreen';

  final Item? item;

  MakeOrderRequest({this.item});

  @override
  MakeOrderRequestState createState() => MakeOrderRequestState();
}

class MakeOrderRequestState extends State<MakeOrderRequest> {
  Cart cart = Cart(items: []);

  var options = DeliveryType.values;
  var selectedOption = DeliveryType.PICKUP;

  var additionalPhoneNumberCont = new TextEditingController();

  var phoneNumberCont = new TextEditingController();
  var licensePlateCont = new TextEditingController();
  var addressCont = new TextEditingController();
  final _formKey = GlobalKey<FormState>();
  var _formdata = new Map();

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  fetchData() async {
    try {
      var loadedCart = await context.read(cartProvider).fetchCart();

      setState(() {
        this.cart = loadedCart;
      });
    } catch (e) {
      showErrorDialog(e, context);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var paymentDetail = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Divider(
            height: 1,
            color: view_color,
          ),
        ],
      ),
    );

    var deliveryOptions = Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: spacing_standard_new),
      margin: EdgeInsets.symmetric(horizontal: spacing_standard_new),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Text(
                    AppLocalizations.of(context)
                        .translate("lbl_delivery_option"),
                    style: Theme.of(context)
                        .textTheme
                        .headline6!
                        .copyWith(fontWeight: FontWeight.normal)),
              ),
              Divider(
                thickness: 0.5,
              ),
              RadioListTile(
                title: Text(options[0].toTranslatedString(context)),
                value: options[0],
                groupValue: selectedOption,
                onChanged: (DeliveryType? value) {
                  setState(() {
                    selectedOption = value!;
                  });
                },
              ),
              RadioListTile(
                title: Text(options[1].toTranslatedString(context)),
                value: options[1],
                groupValue: selectedOption,
                onChanged: (DeliveryType? value) {
                  setState(() {
                    selectedOption = value!;
                  });
                },
              ),
              RadioListTile(
                title: Text(options[2].toTranslatedString(context)),
                value: options[2],
                groupValue: selectedOption,
                onChanged: (DeliveryType? value) {
                  setState(() {
                    selectedOption = value!;
                  });
                },
              ),
              selectedOption == DeliveryType.PICKUP
                  ? Container()
                  : Padding(
                      padding: const EdgeInsets.all(spacing_standard),
                      child: Wrap(
                        runSpacing: spacing_standard_new,
                        children: <Widget>[
                          TextFormField(
                            controller: addressCont,
                            style: TextStyle(
                              color: Colors.black,
                            ),
                            textInputAction: TextInputAction.done,
                            autofocus: false,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: editText_background,
                              focusColor: editText_background_active,
                              errorBorder: OutlineInputBorder(),
                              hintText: AppLocalizations.of(context)
                                  .translate("hint_address"),
                            ),
                            validator: (String? value) {
                              if (value!.trim().isEmpty)
                                return 'This field is required';
                              return null;
                            },
                          ),
                          if (selectedOption == DeliveryType.HALFPICKUP)
                            TextFormField(
                              controller: licensePlateCont,
                              style: TextStyle(
                                color: Colors.black,
                              ),
                              autofocus: false,
                              validator: (String? value) {
                                if (value!.trim().isEmpty)
                                  return 'This field is required';
                                return null;
                              },
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: editText_background,
                                focusColor: editText_background_active,
                                errorBorder: OutlineInputBorder(),
                                hintText: AppLocalizations.of(context)
                                    .translate("hint_license_plate"),
                              ),
                            ),
                        ],
                      ),
                    ),
            ],
          ),
        ],
      ),
    );
    var items = Container(
      padding: EdgeInsets.symmetric(vertical: spacing_standard_new),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 26.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Text(
                AppLocalizations.of(context).translate("lbl_items"),
                style: Theme.of(context)
                    .textTheme
                    .headline6!
                    .copyWith(fontWeight: FontWeight.normal),
              ),
            ),
            widget.item != null
                ? ItemsFragment(
                    items: [widget.item!],
                    buyingOption: false,
                    isCart: false,
                  )
                : Consumer(builder: (context, watch, child) {
                    return ItemsFragment(
                      items: watch(cartProvider).cart.items,
                      buyingOption: false,
                      isCart: false,
                    );
                  }),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 26.0),
              child: paymentDetail,
            ),
          ],
        ),
      ),
    );

    var bottomButtons = Container(
      height: 65,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              color: Theme.of(context).backgroundColor,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  widget.item != null
                      ? Text(
                          "${(widget.item!.price * widget.item!.quantity).toString().toCurrencyString()}",
                          style: Theme.of(context).textTheme.bodyText1)
                      : Consumer(builder: (context, watch, child) {
                          var items = watch(cartProvider).cart.items;
                          var total = 0;
                          items.forEach((item) {
                            total += item.price * item.quantity;
                          });
                          return Text("${total.toString().toCurrencyString()}",
                              style: Theme.of(context).textTheme.bodyText1);
                        }),
                ],
              ),
            ),
          ),
          Expanded(
            child: InkWell(
              child: Container(
                child: Text(
                  AppLocalizations.of(context).translate("lbl_order"),
                  style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: Colors.white, fontWeight: FontWeight.normal),
                ),
                color: Theme.of(context).primaryColor,
                alignment: Alignment.center,
                height: double.infinity,
              ),
              onTap: () {
                widget.item != null
                    ? makeOrder(item: widget.item)
                    : makeOrder();
              },
            ),
          )
        ],
      ),
    );
    var contactInfo = Container(
      padding: EdgeInsets.symmetric(horizontal: spacing_standard_new),
      margin: EdgeInsets.symmetric(horizontal: spacing_standard_new),
      child: Wrap(
        runSpacing: spacing_standard_new,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: spacing_standard_new),
            child: Text(
                AppLocalizations.of(context)
                    .translate("lbl_contact_information"),
                style: Theme.of(context)
                    .textTheme
                    .headline6!
                    .copyWith(fontWeight: FontWeight.normal)),
          ),
          InternationalPhoneNumberInput(
            hintText: AppLocalizations.of(context).translate("hint_mobile_no"),
            onInputChanged: (PhoneNumber number) {},
            initialValue: PhoneNumber(isoCode: 'ET'),
            onInputValidated: (bool value) {},
            selectorConfig: SelectorConfig(
              selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
            ),
            ignoreBlank: false,
            autoValidateMode: AutovalidateMode.onUserInteraction,
            selectorTextStyle: Theme.of(context).textTheme.bodyText1,
            textFieldController: phoneNumberCont,
            formatInput: false,
            keyboardType:
                TextInputType.numberWithOptions(signed: true, decimal: true),
            inputBorder: OutlineInputBorder(),
            onSaved: (PhoneNumber number) {
              _formdata.addAll({'phone': number.toString()});
            },
          ),
          InternationalPhoneNumberInput(
            hintText: AppLocalizations.of(context)
                .translate("hint_additional_mobile_no"),
            onInputChanged: (PhoneNumber number) {},
            initialValue: PhoneNumber(isoCode: 'ET'),
            onInputValidated: (bool value) {},
            selectorConfig: SelectorConfig(
              selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
            ),
            ignoreBlank: true,
            autoValidateMode: AutovalidateMode.onUserInteraction,
            selectorTextStyle: Theme.of(context).textTheme.bodyText1,
            textFieldController: additionalPhoneNumberCont,
            formatInput: false,
            keyboardType:
                TextInputType.numberWithOptions(signed: true, decimal: true),
            inputBorder: OutlineInputBorder(),
            onSaved: (PhoneNumber number) {
              if (number.parseNumber() != '')
                _formdata.addAll({'backupPhone': number.toString()});
            },
          ),
        ],
      ),
    );
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate("order_summary"),
        ),
      ),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 70.0),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    contactInfo,
                    deliveryOptions,
                    items,
                  ],
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.all(0),
              child: bottomButtons,
            ),
          ),
        ],
      ),
    );
  }

  void makeOrder({Item? item}) async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      try {
        if (selectedOption != DeliveryType.PICKUP) {
          _formdata.addEntries([MapEntry('address', addressCont.value.text)]);
          if (selectedOption == DeliveryType.HALFPICKUP) {
            _formdata.addEntries(
                [MapEntry('licensePlate', licensePlateCont.value.text)]);
          }
        }

        if (item != null) {
          await context.read(orderProvider).orderProduct(
              item: item, deliveryType: selectedOption, notes: _formdata);
        } else {
          await context
              .read(orderProvider)
              .orderCart(deliveryType: selectedOption, notes: _formdata);
        }
        toast(
            AppLocalizations.of(context)
                .translate("lbl_order_placed_successfully"),
            context);
        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (ctx) => HomeScreen()),
          (route) => !route.navigator!.canPop(),
        );
      } catch (error) {
        showErrorDialog(error, context);
      }
    }
  }
}
