import 'package:ecommerce_client/models/order.dart';
import 'package:ecommerce_client/providers/order_provider.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';
import 'package:ecommerce_client/utils/widgets.dart';

import 'package:ecommerce_client/views/order/paypal_payment.dart';
import 'package:flutter/material.dart';
import 'package:ecommerce_client/utils/colors.dart';
import 'package:flutter_riverpod/src/provider.dart';
import 'package:loader_overlay/src/overlay_controller_widget_extension.dart';

class Payment extends StatefulWidget {
  static String tag = '/CompleteOrder';
  final OrderModel order;
  Payment({required this.order});
  @override
  PaymentState createState() => PaymentState();
}

enum Banks { COMMERCIAL_BANK, DASHEN_BANK, ABYSSINIA_BANK }

extension BanksToString on Banks {
  String toShortString() {
    return this.toString().split('.').last;
  }

  String toTranslatedString(context) {
    switch (this) {
      case Banks.COMMERCIAL_BANK:
        return AppLocalizations.of(context).translate("lbl_commercial");
      case Banks.DASHEN_BANK:
        return AppLocalizations.of(context).translate("lbl_dashen");
      case Banks.ABYSSINIA_BANK:
        return AppLocalizations.of(context).translate("lbl_abyssinia");

      default:
        return "";
    }
  }

  String accountNumber() {
    switch (this) {
      case Banks.COMMERCIAL_BANK:
        return '1000131294798';
      case Banks.DASHEN_BANK:
        return '5067212985011';
      case Banks.ABYSSINIA_BANK:
        return 'S-A 9713182';

      default:
        return "";
    }
  }
}

class PaymentState extends State<Payment> {
  var cvvCont = TextEditingController();
  var nameCont = TextEditingController();
  var ttNumberCont = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  var radioOptions = PaymentType.values;
  var selectedRadio = PaymentType.TT;

  Banks? selectedBank;
  bool isLoading = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var paymentDetail = Container(
      padding: EdgeInsets.symmetric(horizontal: spacing_standard_new),
      margin: EdgeInsets.symmetric(horizontal: spacing_standard_new),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(
              top: spacing_middle,
              bottom: spacing_middle,
            ),
            child: Text(
              AppLocalizations.of(context).translate("lbl_price_details"),
              style: Theme.of(context)
                  .textTheme
                  .headline6!
                  .copyWith(fontWeight: FontWeight.normal),
            ),
          ),
          Divider(
            height: 1,
            color: view_color,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(spacing_standard_new,
                spacing_middle, spacing_standard_new, spacing_middle),
            child: Column(
              children: <Widget>[
                if (widget.order.bonus != null)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        AppLocalizations.of(context)
                            .translate("lbl_bonus_amount"),
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      Text(
                        widget.order.bonus.toString().toCurrencyString(),
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ],
                  ),
                SizedBox(
                  height: spacing_standard,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      AppLocalizations.of(context)
                          .translate("lbl_total_amount"),
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    Text(
                      widget.order.totalPrice.toString().toCurrencyString(),
                      style: Theme.of(context)
                          .textTheme
                          .headline6!
                          .copyWith(fontWeight: FontWeight.normal),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
    var orderInfo = Container(
      padding: EdgeInsets.symmetric(horizontal: spacing_standard_new),
      margin: EdgeInsets.symmetric(horizontal: spacing_standard_new),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.symmetric(
              vertical: spacing_standard,
            ),
            child: Text(
              AppLocalizations.of(context).translate("lbl_order_info"),
              style: Theme.of(context)
                  .textTheme
                  .headline6!
                  .copyWith(fontWeight: FontWeight.normal),
            ),
          ),
          Divider(
            height: 1,
            color: view_color,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(spacing_standard_new,
                spacing_middle, spacing_standard_new, spacing_middle),
            child: Column(
              children: <Widget>[
                if (widget.order.userNotes.isNotEmpty)
                  ...widget.order.userNotes.entries.map<Widget>((e) {
                    return Container(
                      padding: EdgeInsets.symmetric(vertical: spacing_control),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            e.key,
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                          Text(
                            e.value,
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                        ],
                      ),
                    );
                  }),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        AppLocalizations.of(context).translate("lbl_name"),
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                    Expanded(
                      child: Text(
                        widget.order.user,
                        style: Theme.of(context).textTheme.bodyText1,
                        textAlign: TextAlign.end,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: spacing_standard,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        AppLocalizations.of(context)
                            .translate("lbl_delivery_option"),
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                    Expanded(
                      child: Text(
                        widget.order.deliveryType.toTranslatedString(context),
                        style: Theme.of(context).textTheme.bodyText1,
                        textAlign: TextAlign.end,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
    if (isLoading)
      context.loaderOverlay.show();
    else
      context.loaderOverlay.hide();
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate("title_payment"),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          orderInfo,
          paymentDetail,
          Form(
            key: _formKey,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: spacing_standard_new),
              margin: EdgeInsets.symmetric(horizontal: spacing_standard_new),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.symmetric(
                      vertical: spacing_standard,
                    ),
                    child: Text(
                      AppLocalizations.of(context)
                          .translate("lbl_payment_methods"),
                      style: Theme.of(context)
                          .textTheme
                          .headline6!
                          .copyWith(fontWeight: FontWeight.normal),
                    ),
                  ),
                  Divider(
                    height: 1,
                    color: view_color,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      IntrinsicHeight(
                        child: RadioListTile(
                          title:
                              Text(radioOptions[0].toTranslatedString(context)),
                          value: radioOptions[0],
                          groupValue: selectedRadio,
                          onChanged: (PaymentType? value) {
                            setState(() {
                              selectedRadio = value!;
                            });
                          },
                        ),
                      ),
                      IntrinsicHeight(
                        child: RadioListTile(
                          title:
                              Text(radioOptions[1].toTranslatedString(context)),
                          value: radioOptions[1],
                          groupValue: selectedRadio,
                          onChanged: (PaymentType? value) {
                            setState(() {
                              selectedRadio = value!;
                            });
                          },
                        ),
                      ),
                      IntrinsicHeight(
                        child: RadioListTile(
                          title:
                              Text(radioOptions[2].toTranslatedString(context)),
                          value: radioOptions[2],
                          groupValue: selectedRadio,
                          onChanged: (PaymentType? value) {
                            setState(() {
                              selectedRadio = value!;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                  selectedRadio == PaymentType.TT
                      ? Padding(
                          padding: const EdgeInsets.all(spacing_standard_new),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              DropdownButtonFormField<Banks>(
                                decoration: InputDecoration(
                                    border: OutlineInputBorder()),
                                items: Banks.values.map((Banks value) {
                                  return DropdownMenuItem<Banks>(
                                    value: value,
                                    child: Text(
                                      value.toTranslatedString(context),
                                    ),
                                  );
                                }).toList(),
                                hint: Text(
                                  AppLocalizations.of(context)
                                      .translate("lbl_select_bank"),
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                                value: selectedBank,
                                onChanged: (newVal) {
                                  setState(() {
                                    selectedBank = newVal!;
                                  });
                                },
                                validator: (value) {
                                  if (value == null)
                                    return 'This field is required';
                                  return null;
                                },
                              ),
                              SizedBox(
                                height: spacing_standard_new,
                              ),
                              if (selectedBank != null)
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      selectedBank!.accountNumber(),
                                      style:
                                          Theme.of(context).textTheme.bodyText1,
                                      maxLines: 2,
                                    ),
                                    SizedBox(
                                      height: spacing_standard,
                                    ),
                                    Text(
                                      "Naod Milkeyas / ናኦድ ሚልክያስ",
                                      style:
                                          Theme.of(context).textTheme.bodyText1,
                                    ),
                                  ],
                                ),
                              SizedBox(
                                height: spacing_standard_new,
                              ),
                              TextFormField(
                                style: TextStyle(
                                  color: Colors.black,
                                ),
                                textInputAction: TextInputAction.done,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: editText_background,
                                  focusColor: editText_background_active,
                                  errorBorder: OutlineInputBorder(),
                                  hintText: AppLocalizations.of(context)
                                      .translate("hint_payment_number"),
                                ),
                                autofocus: false,
                                controller: ttNumberCont,
                                textCapitalization: TextCapitalization.words,
                                validator: (value) {
                                  if (value!.trim().isEmpty)
                                    return 'This field is required';
                                  return null;
                                },
                              ),
                              SizedBox(
                                height: spacing_standard,
                              ),
                              Container(
                                width: double.infinity,
                                child: MaterialButton(
                                  padding: EdgeInsets.all(spacing_standard),
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .translate("lbl_verify"),
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  ),
                                  textColor: white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(8.0)),
                                  color: Theme.of(context).primaryColor,
                                  onPressed: () => {payWithTT()},
                                ),
                              ),
                              SizedBox(
                                height: spacing_standard,
                              ),
                            ],
                          ),
                        )
                      : selectedRadio == PaymentType.PAYPAL
                          ? Padding(
                              padding:
                                  const EdgeInsets.all(spacing_standard_new),
                              child: Container(
                                width: double.infinity,
                                child: MaterialButton(
                                  padding: EdgeInsets.all(spacing_standard),
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .translate("lbl_process_with_paypal"),
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  ),
                                  textColor: white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(8.0)),
                                  color: Theme.of(context).primaryColor,
                                  onPressed: () => {proceedWithPayPal()},
                                ),
                              ),
                            )
                          : Padding(
                              padding:
                                  const EdgeInsets.all(spacing_standard_new),
                              child: Container(
                                width: double.infinity,
                                child: MaterialButton(
                                  padding: EdgeInsets.all(spacing_standard),
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .translate("lbl_caon_delivery"),
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  ),
                                  textColor: white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(8.0)),
                                  color: Theme.of(context).primaryColor,
                                  onPressed: () => {cashOnDelivery()},
                                ),
                              ),
                            ),
                ],
              ),
            ),
          ),
        ]),
      ),
    );
  }

  proceedWithPayPal() async {
    try {
      setState(() {
        isLoading = true;
      });
      String redirectUrl = await context
          .read(orderProvider)
          .proceedWithPayPal(order: widget.order);

      setState(() {
        isLoading = false;
      });
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (_) => PaypalPayment(
            redirectUrl: redirectUrl,
            onFinish: () => () {},
          ),
        ),
      );
    } catch (error) {
      setState(() {
        isLoading = false;
      });
      showErrorDialog(error, context);
    }
  }

  cashOnDelivery() async {
    try {
      setState(() {
        isLoading = true;
      });
      await context.read(orderProvider).payWithCash(order: widget.order);

      setState(() {
        isLoading = false;
      });
      Navigator.of(context).popUntil(ModalRoute.withName("/home/orders"));
    } catch (error) {
      setState(() {
        isLoading = false;
      });
      showErrorDialog(error, context);
    }
  }

  payWithTT() async {
    if (_formKey.currentState!.validate() && selectedBank != null) {
      try {
        setState(() {
          isLoading = true;
        });
        await context.read(orderProvider).payWithTT(
            order: widget.order,
            bankName: selectedBank!.toShortString(),
            ttNumber: ttNumberCont.value.text);

        setState(() {
          isLoading = false;
        });
        Navigator.of(context).popUntil(ModalRoute.withName("/home/orders"));
      } catch (error) {
        setState(() {
          isLoading = false;
        });
        showErrorDialog(error, context);
      }
    }
  }
}
