import 'dart:core';
import 'package:ecommerce_client/providers/order_provider.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';
import 'package:ecommerce_client/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/src/provider.dart';
import 'package:loader_overlay/src/overlay_controller_widget_extension.dart';

class ConfirmPaypalPayment extends StatefulWidget {
  final id;

  final payerId;

  final paymentId;

  final amount;

  ConfirmPaypalPayment({this.id, this.payerId, this.paymentId, this.amount});
  @override
  State<ConfirmPaypalPayment> createState() => _ConfirmPaypalPaymentState();
}

class _ConfirmPaypalPaymentState extends State<ConfirmPaypalPayment> {
  final returnURL = 'https://wuqiyanos.com/account/payment/paypal';

  final cancelURL = 'https://wuqiyanos.com/account/payment-fail';

  bool isLoading = false;

  bool isError = false;

  @override
  Widget build(BuildContext context) {
    if (isLoading)
      context.loaderOverlay.show();
    else
      context.loaderOverlay.hide();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: GestureDetector(
          child: Icon(Icons.arrow_back_ios),
          onTap: () => Navigator.pop(context),
        ),
        title: Text(
            "${AppLocalizations.of(context).translate("lbl_confirm_order")} ${AppLocalizations.of(context).translate("title_payment")}"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              alignment: Alignment.center,
              child: MaterialButton(
                onPressed: () {
                  confirmPayment();
                },
                padding: EdgeInsets.all(spacing_standard),
                child: Text(
                  "${AppLocalizations.of(context).translate("lbl_confirm_order")} ${AppLocalizations.of(context).translate("title_payment")}",
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(8.0)),
                color: Theme.of(context).primaryColor,
              )),
        ],
      ),
    );
  }

  confirmPayment() async {
    try {
      setState(() {
        isLoading = true;
      });
      await context.read(orderProvider).confirmPaypalPayment(
          widget.id, widget.payerId, widget.paymentId, widget.amount);
      setState(() {
        isLoading = false;
      });
      Navigator.of(context).popUntil(ModalRoute.withName("/home/orders"));
      toast(
          "${AppLocalizations.of(context).translate("title_payment")} ${AppLocalizations.of(context).translate("lbl_successfully")}",
          context);
    } catch (error) {
      setState(() {
        isLoading = false;
        isError = true;
      });
      showErrorDialog(error, context);
    }
  }
}
