import 'dart:core';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/widgets.dart';
import 'package:ecommerce_client/views/order/confirm_paypal.dart';
import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

class PaypalPayment extends StatefulWidget {
  final Function onFinish;

  final String redirectUrl;

  PaypalPayment({required this.onFinish, required this.redirectUrl});

  @override
  State<StatefulWidget> createState() {
    return PaypalPaymentState();
  }
}

class PaypalPaymentState extends State<PaypalPayment> {
  String returnURL = 'https://wuqiyanos.com/account/payment/paypal';
  String cancelURL = 'https://wuqiyanos.com/account/payment-fail';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: GestureDetector(
          child: Icon(Icons.arrow_back_ios),
          onTap: () => Navigator.pop(context),
        ),
      ),
      body: WebView(
        initialUrl: widget.redirectUrl,
        javascriptMode: JavascriptMode.unrestricted,
        navigationDelegate: (NavigationRequest request) {
          if (request.url.contains(returnURL)) {
            final uri = Uri.parse(request.url);
            final id = uri.queryParameters['id'];
            final payerID = uri.queryParameters['PayerID'];
            final paymentID = uri.queryParameters['paymentId'];
            final total = uri.queryParameters['total'];
            if (payerID != null) {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (ctx) => ConfirmPaypalPayment(
                    id: id,
                    payerId: payerID,
                    paymentId: paymentID,
                    amount: total,
                  ),
                ),
              );
            } else {
              Navigator.of(context).pop();
            }
          }
          if (request.url.contains(cancelURL)) {
            Navigator.of(context).popUntil(ModalRoute.withName("/home/orders"));
            toast(
                "${AppLocalizations.of(context).translate("title_payment")} ${AppLocalizations.of(context).translate("lbl_failed")}",
                context);
          }
          return NavigationDecision.navigate;
        },
      ),
    );
  }
}
