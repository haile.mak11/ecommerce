import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerce_client/models/order.dart';

import 'package:ecommerce_client/providers/order_provider.dart';
import 'package:ecommerce_client/providers/product_provider.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';

import 'package:ecommerce_client/utils/widgets.dart';
import 'package:ecommerce_client/views/order/order_detail_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:ecommerce_client/utils/colors.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'payment_screen.dart';

class OrdersScreen extends StatefulWidget {
  static String tag = '/OrdersScreen';

  @override
  OrdersScreenState createState() => OrdersScreenState();
}

class OrdersScreenState extends State<OrdersScreen> {
  // List<OrderModel> list = [];
  var ordersRef;
  @override
  void initState() {
    super.initState();
    initLoad();
    _controller..addListener(loadMore);
    ordersRef = context.read(orderProvider).orders;
  }

  @override
  void dispose() {
    super.dispose();
    ordersRef.clear();
  }

  bool isLoading = true;
  bool isError = false;

  bool isLoadingMore = false;
  bool hasNextPage = true;
  int page = 1;
  ScrollController _controller = new ScrollController();
  void initLoad() async {
    try {
      await context.read(orderProvider).fetchOrders();
      setState(() {
        isLoading = false;
        isError = false;
      });
    } catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
        isError = true;
      });
    }
  }

  void loadMore() async {
    if (hasNextPage == true &&
        isLoading == false &&
        isLoadingMore == false &&
        _controller.position.extentAfter < 300)
      try {
        setState(() {
          isLoadingMore = true;
        });
        page += 1;
        List<OrderModel> orders =
            await context.read(orderProvider).fetchOrders(pageNumber: page);
        setState(() {
          isLoadingMore = false;
          if (orders.length > 0) {
          } else {
            hasNextPage = false;
          }
        });
      } catch (error) {
        setState(() {
          isLoadingMore = false;
        });
        showErrorDialog(error, context);
      }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate("lbl_my_orders"),
        ),
      ),
      body: isLoading
          ? LoadingListView()
          : Consumer(
              builder: (context, watch, child) {
                List<OrderModel> list = watch(orderProvider).orders;
                return list.isEmpty
                    ? errorScreen(context, initLoad, errorCode: 4)
                    : SingleChildScrollView(
                        controller: _controller,
                        child: Column(
                          children: [
                            Column(
                              children: [
                                ListView.builder(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: list.length,
                                  scrollDirection: Axis.vertical,
                                  itemBuilder: (context, index) {
                                    return Container(
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          SizedBox(height: 8),
                                          InkWell(
                                            onTap: () {
                                              Navigator.of(context)
                                                  .push(MaterialPageRoute(
                                                builder: (ctx) => OrderDetail(
                                                  order: list[index],
                                                ),
                                              ));
                                            },
                                            child: IntrinsicHeight(
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.stretch,
                                                children: <Widget>[
                                                  SizedBox(width: 16),
                                                  Container(
                                                    padding: EdgeInsets.all(1),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: view_color,
                                                            width: 1)),
                                                    child: CachedNetworkImage(
                                                        fit: BoxFit.cover,
                                                        height: width * 0.35,
                                                        width: width * 0.29,
                                                        imageUrl: list[index]
                                                            .items
                                                            .first
                                                            .product
                                                            .thumbnail),
                                                  ),
                                                  SizedBox(width: 16),
                                                  Expanded(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      mainAxisSize:
                                                          MainAxisSize.max,
                                                      children: <Widget>[
                                                        Text(
                                                          AppLocalizations.of(
                                                                          context)
                                                                      .locale
                                                                      .languageCode ==
                                                                  'en'
                                                              ? "${list[index].items.map((element) => element.product.name)}, "
                                                              : "${list[index].items.map((element) => element.product.amharic.name)}, ",
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .bodyText1,
                                                          maxLines: 2,
                                                        ),
                                                        SizedBox(
                                                            height:
                                                                spacing_standard),
                                                        Text(
                                                          "${AppLocalizations.of(context).translate("lbl_order_placed_on")} ${list[index].date}",
                                                        ),
                                                        SizedBox(
                                                          height:
                                                              spacing_standard,
                                                        ),
                                                        if (list[index]
                                                                .totalPrice !=
                                                            null)
                                                          Text(
                                                            list[index].status !=
                                                                    OrderStatus
                                                                        .REQUESTED
                                                                ? "${AppLocalizations.of(context).translate("lbl_total_amount")} ${list[index].totalPrice.toString().toCurrencyString()}"
                                                                : "${AppLocalizations.of(context).translate("lbl_subtotal_amount")} ${list[index].totalPrice.toString().toCurrencyString()}",
                                                          ),
                                                        SizedBox(
                                                          height:
                                                              spacing_standard,
                                                        ),
                                                        Text(
                                                          "${AppLocalizations.of(context).translate("lbl_order_status")} : ${list[index].status.toTranslatedString(context)}",
                                                        ),
                                                        SizedBox(
                                                          height:
                                                              spacing_standard,
                                                        ),
                                                        if (list[index]
                                                                .status ==
                                                            OrderStatus
                                                                .PAYMENTPENDING)
                                                          Text(
                                                            "${AppLocalizations.of(context).translate("lbl_payment_methods")} : ${list[index].paymentType!.toTranslatedString(context)}",
                                                          ),
                                                        SizedBox(
                                                          height:
                                                              spacing_standard,
                                                        ),
                                                        list[index].status ==
                                                                OrderStatus
                                                                    .CONFIRMED
                                                            ? MaterialButton(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColor,
                                                                child: Text(
                                                                  AppLocalizations.of(
                                                                          context)
                                                                      .translate(
                                                                          "lbl_checkout"),
                                                                  style: Theme.of(
                                                                          context)
                                                                      .textTheme
                                                                      .bodyText1
                                                                      ?.copyWith(
                                                                          color:
                                                                              Colors.white),
                                                                ),
                                                                onPressed: () {
                                                                  if (list[index]
                                                                          .status ==
                                                                      OrderStatus
                                                                          .CONFIRMED)
                                                                    Navigator
                                                                        .push(
                                                                      context,
                                                                      MaterialPageRoute(
                                                                          builder: (context) =>
                                                                              Payment(order: list[index])),
                                                                    );
                                                                },
                                                                shape: RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            4)),
                                                              )
                                                            : Container(),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                          SizedBox(height: 8),
                                          Divider()
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              ],
                            ),
                            SizedBox(
                              height: spacing_standard_new,
                            ),
                            if (isLoadingMore == true)
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 10, bottom: 40),
                                child: Center(
                                  child: CircularProgressIndicator(),
                                ),
                              ),

                            // When nothing else to load
                          ],
                        ),
                      );
              },
            ),
    );
  }
}
