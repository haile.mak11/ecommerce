import 'package:ecommerce_client/models/order.dart';
import 'package:ecommerce_client/providers/order_provider.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';
import 'package:ecommerce_client/utils/widgets.dart';

import 'package:ecommerce_client/views/items_fragment.dart';
import 'package:ecommerce_client/views/order/payment_screen.dart';
import 'package:flutter/material.dart';
import 'package:ecommerce_client/utils/colors.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:loader_overlay/loader_overlay.dart';

class OrderDetail extends StatefulWidget {
  static String tag = '/CompleteOrder';
  final order;

  final id;
  OrderDetail({this.order, this.id});
  @override
  OrderDetailState createState() => OrderDetailState();
}

class OrderDetailState extends State<OrderDetail> {
  bool isLoading = true;
  String? bonus;
  bool isBonusApplied = false;
  late OrderModel order;
  @override
  void initState() {
    fetch();
    super.initState();
  }

  fetch() async {
    try {
      order = widget.order != null
          ? widget.order
          : await context.read(orderProvider).getOrder(id: widget.id);

      setState(() {
        isLoading = false;
      });
      getBonus();
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      showErrorDialog(e, context);
    }
  }

  getBonus() async {
    if (order.status == OrderStatus.CONFIRMPENDING) {
      var bonusAmount = await context.read(orderProvider).getUserBonus();
      setState(() {
        bonus = bonusAmount;
      });
    }
  }

  confirmOrder() async {
    try {
      setState(() {
        isLoading = true;
      });

      await context.read(orderProvider).confirmOrderRequest(
            order: order,
            confirm: true,
            bonus: isBonusApplied ? bonus : null,
          );

      setState(() {
        isLoading = false;
      });
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      showErrorDialog(e, context);
    }
  }

  rejectOrder() async {
    try {
      setState(() {
        isLoading = true;
      });

      await context.read(orderProvider).confirmOrderRequest(
            order: order,
            confirm: false,
          );
      setState(() {
        isLoading = false;
      });
      Navigator.of(context).pop();
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      showErrorDialog(e, context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate("lbl_order_info"),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          isLoading
              ? Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Center(
                      child: CircularProgressIndicator(),
                    ),
                  ],
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Divider(
                      height: spacing_control,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: spacing_standard_new),
                      margin: EdgeInsets.symmetric(
                          horizontal: spacing_standard_new),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: spacing_middle),
                            child: Text(
                              AppLocalizations.of(context)
                                  .translate("lbl_order_info"),
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .copyWith(fontWeight: FontWeight.normal),
                            ),
                          ),
                          Divider(
                            height: 1,
                            color: view_color,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: spacing_standard_new,
                                vertical: spacing_middle),
                            child: Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      "${AppLocalizations.of(context).translate("lbl_order_status")} : ",
                                      style:
                                          Theme.of(context).textTheme.bodyText1,
                                    ),
                                    Expanded(
                                      child: Text(
                                        order.status
                                            .toTranslatedString(context),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                        textAlign: TextAlign.end,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: spacing_standard,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      "${AppLocalizations.of(context).translate("lbl_delivery_option")} : ",
                                      style:
                                          Theme.of(context).textTheme.bodyText1,
                                    ),
                                    Expanded(
                                      child: Text(
                                        order.deliveryType
                                            .toTranslatedString(context),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                        textAlign: TextAlign.end,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: spacing_standard,
                                ),
                                if (order.paymentType != null)
                                  Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            "${AppLocalizations.of(context).translate("lbl_payment_methods")} : ",
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1,
                                          ),
                                          Expanded(
                                            child: Text(
                                              order.paymentType!
                                                  .toTranslatedString(context),
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1,
                                              textAlign: TextAlign.end,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: spacing_standard,
                                      ),
                                      if (order.paymentType == PaymentType.TT)
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Expanded(
                                              child: Text(
                                                "${AppLocalizations.of(context).translate("lbl_bank_name")} : ",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1,
                                                textAlign: TextAlign.start,
                                              ),
                                            ),
                                            Expanded(
                                              child: Text(
                                                order.bankName!
                                                    .toTranslatedString(
                                                        context),
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1,
                                                textAlign: TextAlign.end,
                                              ),
                                            ),
                                          ],
                                        ),
                                      SizedBox(
                                        height: spacing_standard,
                                      ),
                                    ],
                                  ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      "${AppLocalizations.of(context).translate("lbl_admin_notes")} : ",
                                      style:
                                          Theme.of(context).textTheme.bodyText1,
                                    ),
                                    Text(
                                      order.adminNotes,
                                      style:
                                          Theme.of(context).textTheme.bodyText1,
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: spacing_standard,
                                ),
                                if (bonus != null)
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        AppLocalizations.of(context)
                                            .translate("lbl_bonus_amount"),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                      Text(
                                        bonus!.toCurrencyString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                    ],
                                  )
                                else if (order.bonus != null)
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        AppLocalizations.of(context)
                                            .translate("lbl_bonus_amount"),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                      Text(
                                        order.bonus.toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                    ],
                                  ),
                                SizedBox(
                                  height: spacing_standard,
                                ),
                                if (order.totalPrice != null)
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "${AppLocalizations.of(context).translate("lbl_total_amount")}",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                      Text(
                                        order.status != OrderStatus.REQUESTED
                                            ? order.totalPrice
                                                .toString()
                                                .toCurrencyString()
                                            : '?',
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                    ],
                                  ),
                                SizedBox(
                                  height: spacing_standard,
                                ),
                                if (order.status == OrderStatus.CONFIRMPENDING)
                                  Column(
                                    children: [
                                      SizedBox(
                                        height: spacing_standard,
                                      ),
                                      if (bonus != null)
                                        Row(
                                          children: [
                                            Text(AppLocalizations.of(context)
                                                .translate("lbl_apply_bonus")),
                                            Switch(
                                              value: isBonusApplied,
                                              onChanged: (value) {
                                                setState(() {
                                                  isBonusApplied = value;
                                                });
                                              },
                                            ),
                                          ],
                                        ),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: SizedBox(
                                              width: double.infinity,
                                              height: 50,
                                              // height: double.infinity,
                                              child: MaterialButton(
                                                padding: EdgeInsets.all(
                                                    spacing_standard),
                                                child: Text(
                                                  AppLocalizations.of(context)
                                                      .translate(
                                                          "lbl_confirm_order"),
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1,
                                                ),
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(8.0)),
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                onPressed: () {
                                                  confirmOrder();
                                                },
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            child: SizedBox(
                                              width: double.infinity,
                                              height: 50,
                                              // height: double.infinity,
                                              child: MaterialButton(
                                                padding: EdgeInsets.all(
                                                    spacing_standard),
                                                child: Text(
                                                  AppLocalizations.of(context)
                                                      .translate(
                                                          "lbl_reject_order"),
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1!
                                                      .copyWith(
                                                          color:
                                                              Colors.black87),
                                                ),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      new BorderRadius.circular(
                                                          8.0),
                                                ),
                                                onPressed: () {
                                                  rejectOrder();
                                                },
                                                color: white,
                                              ),
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                if (order.status == OrderStatus.CONFIRMED)
                                  MaterialButton(
                                    color: Theme.of(context).primaryColor,
                                    child: Text(
                                      AppLocalizations.of(context)
                                          .translate("lbl_checkout"),
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1
                                          ?.copyWith(color: Colors.white),
                                    ),
                                    onPressed: () {
                                      if (order.status == OrderStatus.CONFIRMED)
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  Payment(order: order)),
                                        );
                                    },
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(4)),
                                  )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(spacing_standard_new),
                      margin: const EdgeInsets.symmetric(
                          horizontal: spacing_standard_new),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context).translate("lbl_items"),
                            style: Theme.of(context)
                                .textTheme
                                .headline6!
                                .copyWith(fontWeight: FontWeight.normal),
                            textAlign: TextAlign.start,
                          ),
                          ItemsFragment(
                            items: order.items,
                            isCart: false,
                            buyingOption: false,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
        ]),
      ),
    );
  }
}
