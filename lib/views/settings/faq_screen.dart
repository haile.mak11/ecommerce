import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FAQScreen extends StatefulWidget {
  @override
  FAQScreenState createState() => FAQScreenState();
}

class FAQScreenState extends State<FAQScreen> {
  List<dynamic>? faq;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    faq = AppLocalizations.of(context).translate("faq");

    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate("lbl_faq"),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ListView.builder(
                padding: const EdgeInsets.all(16.0),
                itemCount: faq!.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  Map<String, dynamic> header = faq![index];
                  return ExpansionTile(
                    title: getTitle(header.keys.first),
                    children: <Widget>[
                      ...header.values.first.map((entry) {
                        return IntrinsicHeight(
                          child: getQuestion(
                            entry['title'],
                            entry['answer'],
                          ),
                        );
                      }),
                    ],
                  );
                }),
          ],
        ),
      ),
    );
  }

  Widget getTitle(String content) {
    return Text(
      content,
      style: Theme.of(context).textTheme.bodyText1,
    );
  }

  Widget getQuestion(String title, String answer) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(
              16.0, spacing_standard, 16.0, spacing_standard),
          child: Text(
            title,
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(spacing_standard),
          child: Text(answer),
        )
      ],
    );
  }
}
