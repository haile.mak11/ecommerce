import 'package:ecommerce_client/providers/app_theme_state.dart';
import 'package:ecommerce_client/providers/app_lang_state.dart';
import 'package:ecommerce_client/providers/authentication_state.dart';
import 'package:ecommerce_client/utils/constants.dart';

import 'package:ecommerce_client/views/auth/login_screen.dart';
import 'package:ecommerce_client/views/settings/contact_page.dart';
import 'package:ecommerce_client/views/settings/faq_screen.dart';
import 'package:ecommerce_client/views/order/orders_screen.dart';
import 'package:ecommerce_client/views/settings/profile_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ecommerce_client/utils/colors.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../utils/app_localizations.dart';

class Settings extends StatefulWidget {
  static String tag = '/SettingsScreen';

  @override
  SettingsState createState() => SettingsState();
}

class SettingsState extends State<Settings> {
  bool pushNotification = false;
  bool smsNotification = false;
  bool emailNotification = false;
  var selectedValue = "English(US)";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Consumer(builder: (context, watch, child) {
          return Column(
            children: <Widget>[
              if (watch(authenticationNotifierProvider).isAuth)
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        settings: RouteSettings(name: "/home/orders"),
                        builder: (ctx) {
                          return OrdersScreen();
                        }));
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                        left: spacing_standard_new,
                        right: spacing_standard_new,
                        top: spacing_standard_new),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(
                              AppLocalizations.of(context)
                                  .translate("lbl_my_orders"),
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .copyWith(fontWeight: FontWeight.normal),
                            ),
                          ],
                        ),
                        Text(
                          AppLocalizations.of(context)
                              .translate("lbl_my_orders_manage"),
                        ),
                        SizedBox(height: spacing_standard_new),
                        Divider(
                          height: 0.5,
                          color: view_color,
                        )
                      ],
                    ),
                  ),
                ),
              if (watch(authenticationNotifierProvider).isAuth)
                InkWell(
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (ctx) {
                      return ProfileScreen();
                    }));
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                        left: spacing_standard_new,
                        right: spacing_standard_new,
                        top: spacing_standard_new),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(
                              AppLocalizations.of(context)
                                  .translate("lbl_account"),
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .copyWith(fontWeight: FontWeight.normal),
                            ),
                          ],
                        ),
                        Text(
                          AppLocalizations.of(context)
                              .translate("lbl_edit_account_info"),
                        ),
                        SizedBox(height: spacing_standard_new),
                        Divider(
                          height: 0.5,
                          color: view_color,
                        )
                      ],
                    ),
                  ),
                ),
              if (watch(authenticationNotifierProvider).isAuth)
                Container(
                  margin: EdgeInsets.only(
                      left: spacing_standard_new,
                      right: spacing_standard_new,
                      top: spacing_standard_new),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            AppLocalizations.of(context)
                                .translate("lbl_push_notification"),
                            style: Theme.of(context)
                                .textTheme
                                .headline6!
                                .copyWith(fontWeight: FontWeight.normal),
                          ),
                          Consumer(
                            builder: (context, watch, child) {
                              final notificationState =
                                  watch(authenticationNotifierProvider)
                                          .fcmToken !=
                                      '';
                              final authNotifier =
                                  context.read(authenticationNotifierProvider);
                              return Switch(
                                value: notificationState,
                                onChanged: (enabled) {
                                  authNotifier.toggleNotification();
                                },
                                activeColor: Theme.of(context).primaryColor,
                              );
                            },
                          ),
                        ],
                      ),
                      Text(
                        AppLocalizations.of(context).translate(
                            "lbl_notification_arrive_on_order_status"),
                      ),
                      SizedBox(height: spacing_standard_new),
                      Divider(
                        height: 0.5,
                        color: view_color,
                      )
                    ],
                  ),
                ),
              Container(
                margin: EdgeInsets.only(
                    left: spacing_standard_new, right: spacing_standard_new),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          AppLocalizations.of(context).translate("lbl_theme"),
                          style: Theme.of(context)
                              .textTheme
                              .headline6!
                              .copyWith(fontWeight: FontWeight.normal),
                        ),
                        Consumer(
                          builder: (context, watch, child) {
                            final _appThemeState =
                                context.read(appThemeStateProvider);
                            final _appThemeNotifier =
                                context.read(appThemeStateProvider.notifier);
                            return Switch(
                              value: _appThemeState,
                              onChanged: (enabled) {
                                _appThemeNotifier.toggleAppTheme(context);
                              },
                              activeColor: Theme.of(context).primaryColor,
                            );
                          },
                        ),
                      ],
                    ),
                    Text(
                      AppLocalizations.of(context).translate("lbl_dark_mode"),
                    ),
                    SizedBox(height: spacing_standard_new),
                    Divider(
                      height: 0.5,
                      color: view_color,
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(spacing_standard_new),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          AppLocalizations.of(context)
                              .translate("lbl_language"),
                          style: Theme.of(context)
                              .textTheme
                              .headline6!
                              .copyWith(fontWeight: FontWeight.normal),
                        ),
                        Consumer(builder: (context, watch, child) {
                          final languageNotifier =
                              context.read(languageStateProvider.notifier);
                          final _languageState =
                              context.read(languageStateProvider);

                          return DropdownButton<String>(
                            underline: SizedBox(),
                            items: <String>["English", "አማርኛ"]
                                .map((String buttonValue) {
                              return DropdownMenuItem<String>(
                                value: buttonValue,
                                child: Text(
                                  buttonValue,
                                ),
                              );
                            }).toList(),
                            value: _languageState,
                            onChanged: (newVal) {
                              languageNotifier.changeLocale(
                                  context, newVal ?? "English");
                              setState(() {
                                selectedValue = newVal!;
                              });
                            },
                          );
                        }),
                      ],
                    ),
                    SizedBox(height: spacing_standard_new),
                    Divider(
                      height: 0.5,
                      color: view_color,
                    )
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (ctx) {
                    return FAQScreen();
                  }));
                },
                child: Container(
                  margin: EdgeInsets.only(
                      left: spacing_standard_new,
                      right: spacing_standard_new,
                      top: spacing_standard_new),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            AppLocalizations.of(context).translate("lbl_faq"),
                            style: Theme.of(context)
                                .textTheme
                                .headline6!
                                .copyWith(fontWeight: FontWeight.normal),
                          ),
                        ],
                      ),
                      Text(
                        AppLocalizations.of(context)
                            .translate("lbl_faq_subtitle"),
                      ),
                      SizedBox(height: spacing_standard_new),
                      Divider(
                        height: 0.5,
                        color: view_color,
                      )
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (ctx) {
                    return ContactScreen();
                  }));
                },
                child: Container(
                  margin: EdgeInsets.only(
                      left: spacing_standard_new,
                      right: spacing_standard_new,
                      top: spacing_standard_new),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            AppLocalizations.of(context)
                                .translate("lbl_contact_us"),
                            style: Theme.of(context)
                                .textTheme
                                .headline6!
                                .copyWith(fontWeight: FontWeight.normal),
                          ),
                        ],
                      ),
                      SizedBox(height: spacing_standard_new),
                      Divider(
                        height: 0.5,
                        color: view_color,
                      )
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  if (watch(authenticationNotifierProvider).isAuth) {
                    context.read(authenticationNotifierProvider).logout();
                  } else {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (ctx) => LoginScreen()));
                  }
                },
                child: Container(
                  margin: EdgeInsets.only(
                      left: spacing_standard_new,
                      right: spacing_standard_new,
                      top: spacing_standard_new),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            watch(authenticationNotifierProvider).isAuth
                                ? AppLocalizations.of(context)
                                    .translate("lbl_log_out")
                                : AppLocalizations.of(context)
                                    .translate("lbl_sign_in"),
                            style: Theme.of(context)
                                .textTheme
                                .headline6!
                                .copyWith(fontWeight: FontWeight.normal),
                          ),
                        ],
                      ),
                      SizedBox(height: spacing_standard_new),
                      Divider(
                        height: 0.5,
                        color: view_color,
                      )
                    ],
                  ),
                ),
              ),
            ],
          );
        }),
      ),
    );
  }
}
