import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactScreen extends StatefulWidget {
  @override
  ContactScreenState createState() => ContactScreenState();
}

class ContactScreenState extends State<ContactScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate("lbl_contact_information"),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.all(spacing_standard),
                    height: height * 0.3,
                    child: Image.asset(
                      ic_logo,
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(
                    height: spacing_xxLarge,
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: spacing_xlarge),
                    child: Text(
                        AppLocalizations.of(context)
                            .translate("lbl_contact_us"),
                        style: Theme.of(context)
                            .textTheme
                            .headline5!
                            .copyWith(fontWeight: FontWeight.normal)),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "${AppLocalizations.of(context).translate("hint_mobile_no")} :",
                        style: Theme.of(context)
                            .textTheme
                            .headline6!
                            .copyWith(fontWeight: FontWeight.normal),
                      ),
                      TextButton(
                        onPressed: () => launch('tel:+251913461620'),
                        child: Text(
                          " +251 913461620",
                          style: Theme.of(context)
                              .textTheme
                              .headline6!
                              .copyWith(fontWeight: FontWeight.normal),
                        ),
                      ),
                    ],
                  ),
                  Divider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "${AppLocalizations.of(context).translate("hint_Email")} :",
                        style: Theme.of(context)
                            .textTheme
                            .headline6!
                            .copyWith(fontWeight: FontWeight.normal),
                      ),
                      TextButton(
                        onPressed: () => launch("mailto:wuqiyanos@gmail.com"),
                        child: Text(
                          "wuqiyanos@gmail.com",
                          style: Theme.of(context)
                              .textTheme
                              .headline6!
                              .copyWith(fontWeight: FontWeight.normal),
                        ),
                      ),
                    ],
                  ),
                  Divider(),
                  SizedBox(
                    height: spacing_standard,
                  ),
                  Text(
                    AppLocalizations.of(context)
                        .translate("lbl_find_us_on_social"),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                        onPressed: () =>
                            {launch("https://facebook.com/wuqiyanos")},
                        icon: Icon(
                          FontAwesomeIcons.facebookSquare,
                          size: 40,
                        ),
                      ),
                      IconButton(
                        onPressed: () =>
                            {launch("https://www.instagram.com/wuqiyanos")},
                        icon: Icon(
                          FontAwesomeIcons.instagram,
                          size: 40,
                        ),
                      ),
                      IconButton(
                        onPressed: () =>
                            {launch("https://twitter.com/wuqiyanos")},
                        icon: Icon(
                          FontAwesomeIcons.twitter,
                          size: 40,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
