import 'dart:convert';

import 'package:ecommerce_client/providers/authentication_state.dart';
import 'package:ecommerce_client/providers/shared_utility.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';

import 'package:ecommerce_client/utils/widgets.dart';
import 'package:ecommerce_client/views/auth/new_password.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:loader_overlay/loader_overlay.dart';

class ProfileScreen extends StatefulWidget {
  static String tag = '/ProfileFragment';

  @override
  ProfileScreenState createState() => ProfileScreenState();
}

class ProfileScreenState extends State<ProfileScreen> {
  var _formKey = GlobalKey<FormState>();
  bool isLoading = false;
  String username = '';
  var firstNameCont = TextEditingController();
  var lastNameCont = TextEditingController();
  @override
  void initState() {
    super.initState();
    fetch();
  }

  void fetch() async {
    var userdata =
        json.decode(context.read(sharedUtilityProvider).getUserData());
    firstNameCont.text = userdata['user']['name'].toString();
  }

  @override
  Widget build(BuildContext context) {
    isLoading ? context.loaderOverlay.show() : context.loaderOverlay.hide();
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate("title_account"),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(spacing_standard_new),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      AppLocalizations.of(context).translate("lbl_name"),
                      style: Theme.of(context).textTheme.headline6!.copyWith(
                            fontWeight: FontWeight.normal,
                          ),
                    ),
                    SizedBox(
                      height: spacing_standard_new,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.text,
                      autofocus: false,
                      controller: firstNameCont,
                      textCapitalization: TextCapitalization.words,
                      decoration: InputDecoration(
                        hintText:
                            AppLocalizations.of(context).translate("lbl_name"),
                      ),
                    ),
                    SizedBox(
                      height: spacing_standard_new,
                    ),
                    SizedBox(
                      width: double.infinity,
                      height: 50,
                      child: MaterialButton(
                        padding: EdgeInsets.all(spacing_standard),
                        child: Text(
                          AppLocalizations.of(context)
                              .translate("lbl_save_profile"),
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: Colors.white),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(8.0)),
                        color: Theme.of(context).primaryColor,
                        onPressed: () => changeName(),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: spacing_standard_new,
              ),
              SizedBox(
                width: double.infinity,
                height: 50,
                child: TextButton(
                  child: Text(
                    AppLocalizations.of(context).translate("lbl_change_pswd"),
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                      builder: (ctx) => NewPassword(isForgot: false))),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> changeName() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      setState(() {
        isLoading = true;
      });
      Map<String, String> _formdata = {};
      _formdata.addEntries([MapEntry('name', "${firstNameCont.value.text} ")]);
      try {
        var response = await context
            .read(authenticationNotifierProvider)
            .changeName(_formdata);
        context.read(authenticationNotifierProvider).refresh();
        setState(() {
          isLoading = false;
        });
        toast(response, context);
      } catch (error) {
        setState(() {
          isLoading = false;
        });
        showErrorDialog(error, context);
      }
    }
  }
}
