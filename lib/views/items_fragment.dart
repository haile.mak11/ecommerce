import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerce_client/models/cart.dart';
import 'package:ecommerce_client/models/product.dart';
import 'package:ecommerce_client/providers/cart_provider.dart';
import 'package:ecommerce_client/providers/product_provider.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';
import 'package:ecommerce_client/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'product_detail_screen.dart';

class ItemsFragment extends StatefulWidget {
  static String tag = 'Cart';
  final bool buyingOption;
  final bool isCart;

  final List<Item> items;
  ItemsFragment({
    this.buyingOption = true,
    this.isCart = true,
    required this.items,
  });
  @override
  ItemsFragmentState createState() => ItemsFragmentState();
}

class ItemsFragmentState extends State<ItemsFragment> {
  var total = 0;
  @override
  void initState() {
    widget.items.forEach((item) {
      total += item.price * item.quantity;
    });
    super.initState();
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  removeItem(ProductModel product) {
    try {
      context.read(cartProvider).removeItem(
            product: product,
          );
    } catch (e) {
      showErrorDialog(e, context);
    }
  }

  removeVariation({required ProductModel product, variation}) {
    try {
      context
          .read(cartProvider)
          .removeVariation(product: product, variation: variation);
    } catch (e) {
      showErrorDialog(e, context);
    }
  }

  Widget variations(entry) {
    var name = entry.key;
    var value = entry.value;
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, top: spacing_control),
      child: Row(
        children: [
          Container(
              width: MediaQuery.of(context).size.width / 3,
              child: Text(
                name.toString(),
              )),
          Expanded(
            child: Container(
                decoration: BoxDecoration(
                  border: Border.all(),
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                ),
                padding: EdgeInsets.symmetric(
                    horizontal: spacing_standard, vertical: spacing_control),
                child: Text(
                  value,
                )),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    List<Item> items = widget.items;
    return SingleChildScrollView(
      child: Container(
        padding: widget.isCart
            ? const EdgeInsets.only(bottom: 70.0)
            : const EdgeInsets.only(bottom: spacing_standard),
        child: Column(
          children: <Widget>[
            ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: items.length,
                shrinkWrap: true,
                padding: EdgeInsets.only(bottom: spacing_standard_new),
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.only(
                        left: spacing_standard_new,
                        right: spacing_standard_new,
                        top: spacing_standard_new),
                    child: items[index].variations.length > 1
                        ? ExpansionTile(
                            trailing: Icon(Icons.add),
                            tilePadding: const EdgeInsets.all(0),
                            title: IntrinsicHeight(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: <Widget>[
                                  Container(
                                    child: CachedNetworkImage(
                                      imageUrl: items[index].product.thumbnail,
                                      width: width * 0.25,
                                      height: width * 0.3,
                                      fit: BoxFit.fitWidth,
                                    ),
                                  ),
                                  Expanded(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            SizedBox(
                                              height: spacing_standard,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 16.0),
                                              child: Text(
                                                AppLocalizations.of(context)
                                                            .locale
                                                            .languageCode ==
                                                        'en'
                                                    ? items[index].product.name
                                                    : items[index]
                                                        .product
                                                        .amharic
                                                        .name,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1,
                                                maxLines: 2,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  16, 1, spacing_standard, 1),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                      "${AppLocalizations.of(context).translate("lbl_quantity")} : ${items[index].quantity}",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      16.0, 8, 0, 0),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: <Widget>[
                                                  Text(
                                                    items[index]
                                                        .price
                                                        .toString()
                                                        .toCurrencyString(),
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1,
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      16,
                                                      spacing_standard,
                                                      spacing_standard,
                                                      spacing_standard),
                                              child: Text(
                                                "${items[index].variations.length} ${AppLocalizations.of(context).translate("lbl_variations")}",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            children: [
                              ...items[index].variations.map<Widget>((entry) {
                                var quantity = entry.keys.first;
                                var variation = entry.values.first;
                                return IntrinsicHeight(
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: <Widget>[
                                      Expanded(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            ...variation.entries
                                                .map<Widget>((entry) {
                                              var name = entry.key;
                                              var value = entry.value;
                                              return Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 16.0,
                                                    top: spacing_control),
                                                child: Row(
                                                  children: [
                                                    Container(
                                                        width: width / 3,
                                                        child: Text(
                                                          name,
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .bodyText1,
                                                        )),
                                                    Container(
                                                        decoration:
                                                            BoxDecoration(
                                                          border: Border.all(),
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          8.0)),
                                                        ),
                                                        padding: EdgeInsets.symmetric(
                                                            horizontal:
                                                                spacing_standard,
                                                            vertical:
                                                                spacing_control),
                                                        child: Text(
                                                          value,
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .bodyText1,
                                                        )),
                                                  ],
                                                ),
                                              );
                                            }),
                                            SizedBox(
                                              width: spacing_standard,
                                            ),
                                            Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  16, 1, spacing_standard, 1),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  Container(
                                                    width: width / 3,
                                                    child: Text(
                                                      "${AppLocalizations.of(context).translate("lbl_quantity")} : ",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1,
                                                    ),
                                                  ),
                                                  Text(
                                                    quantity.toString(),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            if (widget.buyingOption)
                                              Expanded(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: <Widget>[
                                                      Expanded(
                                                        child: InkWell(
                                                          onTap: () {
                                                            Navigator.of(
                                                                    context)
                                                                .push(
                                                              MaterialPageRoute(
                                                                builder: (ctx) =>
                                                                    ProductDetail(
                                                                  product: items[
                                                                          index]
                                                                      .product,
                                                                ),
                                                              ),
                                                            );
                                                          },
                                                          child: Container(
                                                            padding: EdgeInsets.all(
                                                                spacing_standard),
                                                            decoration:
                                                                BoxDecoration(
                                                              border: Border.all(
                                                                  color: Theme.of(
                                                                          context)
                                                                      .backgroundColor),
                                                            ),
                                                            child: Row(
                                                              children: <
                                                                  Widget>[
                                                                Icon(
                                                                  Icons.details,
                                                                  size: 18,
                                                                ),
                                                                Text(
                                                                  AppLocalizations.of(
                                                                          context)
                                                                      .translate(
                                                                          "lbl_product_details"),
                                                                )
                                                              ],
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: InkWell(
                                                          onTap: () =>
                                                              removeVariation(
                                                                  product: items[
                                                                          index]
                                                                      .product,
                                                                  variation:
                                                                      variation),
                                                          child: Container(
                                                            padding: EdgeInsets.all(
                                                                spacing_standard),
                                                            decoration: BoxDecoration(
                                                                border: Border.all(
                                                                    color: Theme.of(
                                                                            context)
                                                                        .backgroundColor)),
                                                            child: Row(
                                                              children: <
                                                                  Widget>[
                                                                Icon(
                                                                  Icons
                                                                      .delete_outline,
                                                                  size: 18,
                                                                ),
                                                                Text(
                                                                  AppLocalizations.of(
                                                                          context)
                                                                      .translate(
                                                                          "lbl_remove"),
                                                                )
                                                              ],
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                            ),
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            SizedBox(
                                              height: spacing_standard,
                                            ),
                                            Divider(),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              }),
                            ],
                          )
                        : Column(
                            children: [
                              IntrinsicHeight(
                                child: Row(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: <Widget>[
                                    Container(
                                      child: CachedNetworkImage(
                                        imageUrl:
                                            items[index].product.thumbnail,
                                        width: width * 0.25,
                                        height: width * 0.3,
                                        fit: BoxFit.fitWidth,
                                      ),
                                    ),
                                    Expanded(
                                      child: Column(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              SizedBox(
                                                height: spacing_standard,
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 16.0),
                                                child: Text(
                                                  AppLocalizations.of(context)
                                                              .locale
                                                              .languageCode ==
                                                          'en'
                                                      ? items[index]
                                                          .product
                                                          .name
                                                      : items[index]
                                                          .product
                                                          .amharic
                                                          .name,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1,
                                                ),
                                              ),
                                              if (items[index]
                                                  .variations
                                                  .isNotEmpty)
                                                ...items[index]
                                                    .variations
                                                    .first
                                                    .values
                                                    .first
                                                    .entries
                                                    .map<Widget>((entry) {
                                                  return variations(entry);
                                                }),
                                              Container(
                                                padding: EdgeInsets.fromLTRB(
                                                    16,
                                                    spacing_standard,
                                                    spacing_standard,
                                                    0),
                                                child: Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: <Widget>[
                                                    Row(
                                                      children: [
                                                        Text(
                                                            "${AppLocalizations.of(context).translate("lbl_quantity")} : "),
                                                        Text(
                                                          items[index]
                                                              .quantity
                                                              .toString(),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        16.0, 8, 0, 0),
                                                child: Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: <Widget>[
                                                    Text(
                                                      items[index]
                                                          .price
                                                          .toString()
                                                          .toCurrencyString(),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          if (widget.buyingOption)
                                            Expanded(
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: InkWell(
                                                        onTap: () {
                                                          if (widget.isCart) {
                                                            Navigator.of(
                                                                    context)
                                                                .push(
                                                              MaterialPageRoute(
                                                                builder: (ctx) =>
                                                                    ProductDetail(
                                                                  product: items[
                                                                          index]
                                                                      .product,
                                                                ),
                                                              ),
                                                            );
                                                          }
                                                        },
                                                        child: Container(
                                                          padding: EdgeInsets.all(
                                                              spacing_standard),
                                                          decoration:
                                                              BoxDecoration(
                                                            border: Border.all(
                                                                color: Theme.of(
                                                                        context)
                                                                    .backgroundColor),
                                                          ),
                                                          child: Row(
                                                            children: <Widget>[
                                                              Icon(
                                                                Icons.details,
                                                                size: 18,
                                                              ),
                                                              Text(
                                                                AppLocalizations.of(
                                                                        context)
                                                                    .translate(
                                                                        "lbl_product_details"),
                                                              )
                                                            ],
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: InkWell(
                                                        onTap: () => removeItem(
                                                          items[index].product,
                                                        ),
                                                        child: Container(
                                                          padding: EdgeInsets.all(
                                                              spacing_standard),
                                                          decoration: BoxDecoration(
                                                              border: Border.all(
                                                                  color: Theme.of(
                                                                          context)
                                                                      .backgroundColor)),
                                                          child: Row(
                                                            children: <Widget>[
                                                              Icon(
                                                                Icons
                                                                    .delete_outline,
                                                                size: 18,
                                                              ),
                                                              Text(
                                                                AppLocalizations.of(
                                                                        context)
                                                                    .translate(
                                                                        "lbl_remove"),
                                                              )
                                                            ],
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                  );
                }),
            if (!widget.isCart)
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    AppLocalizations.of(context)
                        .translate("lbl_subtotal_amount"),
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  Text(
                    total.toString().toCurrencyString(),
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            SizedBox(
              height: spacing_standard,
            ),
          ],
        ),
      ),
    );
  }
}
