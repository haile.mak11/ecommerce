import 'package:ecommerce_client/providers/authentication_state.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';
import 'package:ecommerce_client/utils/widgets.dart';
import 'package:ecommerce_client/views/auth/login_screen.dart';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class NewPassword extends StatefulWidget {
  final isForgot;

  NewPassword({this.isForgot = true});
  @override
  _NewPasswordState createState() => _NewPasswordState();
}

class _NewPasswordState extends State<NewPassword> {
  bool isLoading = false;
  bool newPassWordObscureText = true;
  bool oldPassWordObscureText = true;
  bool newConfirmPassWordObscureText = true;

  var newPassWordCont = TextEditingController();
  var oldPassWordCont = TextEditingController();

  var newPassWordFormKey = GlobalKey<FormState>();

  void set() async {
    if (newPassWordFormKey.currentState!.validate()) {
      newPassWordFormKey.currentState!.save();
      setState(() {
        isLoading = true;
      });

      var formdata = new Map<String, String>();
      formdata.addEntries([MapEntry('password', newPassWordCont.value.text)]);
      try {
        await context
            .read(authenticationNotifierProvider)
            .setPassword(formdata);
        setState(() {
          isLoading = false;
        });

        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => LoginScreen()));
      } catch (error) {
        setState(() {
          isLoading = false;
        });
        showErrorDialog(error.toString(), context);
      }
    }
  }

  void reset() async {
    if (newPassWordFormKey.currentState!.validate()) {
      newPassWordFormKey.currentState!.save();
      setState(() {
        isLoading = true;
      });

      var formdata = new Map<String, String>();
      formdata.addEntries([
        MapEntry('password', newPassWordCont.value.text),
        MapEntry('oldPassword', oldPassWordCont.value.text)
      ]);
      try {
        await context
            .read(authenticationNotifierProvider)
            .changePassword(formdata);
        setState(() {
          isLoading = false;
        });
        toast("${AppLocalizations.of(context).translate("msg_successpwd")} ",
            context);
        Navigator.of(context).pop();
      } catch (error) {
        setState(() {
          isLoading = false;
        });
        showErrorDialog(error.toString(), context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          iconTheme: Theme.of(context).iconTheme),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                height: (MediaQuery.of(context).size.height) / 3.5,
                child: Container(
                  margin: EdgeInsets.only(left: 16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        AppLocalizations.of(context)
                            .translate("lbl_reset_password"),
                        style: Theme.of(context).textTheme.headline4!.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                          AppLocalizations.of(context)
                              .translate("lbl_enter_new_password"),
                          style: Theme.of(context).textTheme.headline6),
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(spacing_large),
                child: Form(
                    key: newPassWordFormKey,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    child: Column(
                      children: <Widget>[
                        if (!widget.isForgot)
                          Padding(
                            padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                            child: TextFormField(
                              obscureText: oldPassWordObscureText,
                              controller: oldPassWordCont,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  hintText: AppLocalizations.of(context)
                                      .translate("hint_enter_old_password"),
                                  filled: true,
                                  fillColor: Color(0xFFF5F4F4),
                                  suffix: Padding(
                                    padding: EdgeInsets.only(right: 8),
                                    child: InkWell(
                                      onTap: () {
                                        oldPassWordObscureText =
                                            !oldPassWordObscureText;
                                        setState(() {});
                                      },
                                      child: Icon(
                                          !oldPassWordObscureText
                                              ? Icons.visibility
                                              : Icons.visibility_off,
                                          color: Colors.black),
                                    ),
                                  )),
                              validator: (s) {
                                if (s!.trim().isEmpty)
                                  return AppLocalizations.of(context)
                                      .translate("error_field_required");
                                if (s.length < 6)
                                  return AppLocalizations.of(context)
                                      .translate("error_pwd_digit_required");

                                return null;
                              },
                            ),
                          ),
                        SizedBox(height: 16),
                        Padding(
                          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: TextFormField(
                            obscureText: newPassWordObscureText,
                            controller: newPassWordCont,
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                                hintText: AppLocalizations.of(context)
                                    .translate("hint_password"),
                                filled: true,
                                fillColor: Color(0xFFF5F4F4),
                                suffix: Padding(
                                  padding: EdgeInsets.only(right: 8),
                                  child: InkWell(
                                    onTap: () {
                                      newPassWordObscureText =
                                          !newPassWordObscureText;
                                      setState(() {});
                                    },
                                    child: Icon(
                                        !newPassWordObscureText
                                            ? Icons.visibility
                                            : Icons.visibility_off,
                                        color: Colors.black),
                                  ),
                                )),
                            validator: (s) {
                              if (s!.trim().isEmpty)
                                return AppLocalizations.of(context)
                                    .translate("error_field_required");
                              if (s.length < 6)
                                return AppLocalizations.of(context)
                                    .translate("error_pwd_digit_required");

                              return null;
                            },
                          ),
                        ),
                        SizedBox(height: 16),
                        Padding(
                          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: TextFormField(
                            style: TextStyle(color: Colors.black),
                            obscureText: newConfirmPassWordObscureText,
                            decoration: InputDecoration(
                                hintText: AppLocalizations.of(context)
                                    .translate("hint_confirm_password"),
                                filled: true,
                                fillColor: Color(0xFFF5F4F4),
                                suffix: Padding(
                                  padding: EdgeInsets.only(right: 8),
                                  child: InkWell(
                                    onTap: () {
                                      newConfirmPassWordObscureText =
                                          !newConfirmPassWordObscureText;
                                      setState(() {});
                                    },
                                    child: Icon(
                                      !newConfirmPassWordObscureText
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      color: Colors.black,
                                    ),
                                  ),
                                )),
                            validator: (s) {
                              if (s!.trim().isEmpty)
                                return AppLocalizations.of(context)
                                    .translate("error_field_required");
                              if (s.trim() != newPassWordCont.text) {
                                return AppLocalizations.of(context)
                                    .translate("error_password_not_matches");
                              }
                              return null;
                            },
                          ),
                        ),
                        SizedBox(height: 16),
                        Padding(
                          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: SizedBox(
                            width: double.infinity,
                            height: 50,
                            child: MaterialButton(
                              padding: EdgeInsets.all(spacing_standard),
                              child: Text(
                                " ${AppLocalizations.of(context).translate("lbl_submit")}   ",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(color: Colors.white),
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(8.0),
                              ),
                              color: Theme.of(context).primaryColor,
                              onPressed: () {
                                if (newPassWordFormKey.currentState!
                                    .validate()) {
                                  newPassWordFormKey.currentState!.save();
                                  if (widget.isForgot)
                                    set();
                                  else
                                    reset();
                                }
                              },
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                      ],
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
