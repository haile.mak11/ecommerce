import 'package:ecommerce_client/providers/authentication_state.dart';
import 'package:ecommerce_client/utils/constants.dart';
import 'package:ecommerce_client/utils/widgets.dart';
import 'package:ecommerce_client/views/auth/login_screen.dart';
import 'package:ecommerce_client/views/auth/verification_screen.dart';
import 'package:flutter/material.dart';
import 'package:ecommerce_client/utils/colors.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:loader_overlay/loader_overlay.dart';

import '../../utils/app_localizations.dart';

class SignupScreen extends StatefulWidget {
  const SignupScreen({Key? key}) : super(key: key);

  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  final _formKey = GlobalKey<FormState>();
  var emailCont = TextEditingController();
  var phoneCont = TextEditingController();
  var passwordCont = TextEditingController();
  var confirmPasswordCont = TextEditingController();
  var firstNameCont = TextEditingController();
  var lastNameCont = TextEditingController();

  var _formdata = Map<String, String>();
  bool isLoading = false;

  Future<void> signup() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      setState(() {
        isLoading = true;
      });

      var formdata = _formdata.map<String, String>((key, value) {
        if (key == 'first_name') {
          return MapEntry('name', "$value ${_formdata['last_name']}");
        }
        if (key == 'last_name') {
          return MapEntry("type", "user");
        }
        return MapEntry(key, value);
      });

      try {
        var response =
            await context.read(authenticationNotifierProvider).signup(formdata);

        setState(() {
          isLoading = false;
        });
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => VerificationScreen(
              response['email'] ?? '',
              resetData: formdata,
            ),
          ),
        );
      } catch (error) {
        setState(() {
          isLoading = false;
        });
        showErrorDialog(error, context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    if (isLoading)
      context.loaderOverlay.show();
    else
      context.loaderOverlay.hide();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
      ),
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(24.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: height * 0.2,
                      child: Image.asset(
                        ic_logo_1,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          AppLocalizations.of(context).translate("app_name"),
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: spacing_xlarge,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Expanded(
                            child: TextFormField(
                                keyboardType: TextInputType.emailAddress,
                                autofocus: false,
                                controller: firstNameCont,
                                textCapitalization: TextCapitalization.words,
                                style: TextStyle(
                                  color: Colors.black,
                                ),
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: editText_background,
                                  focusColor: editText_background_active,
                                  hintText: AppLocalizations.of(context)
                                      .translate("hint_first_name"),
                                ),
                                textInputAction: TextInputAction.next,
                                onSaved: (value) {
                                  _formdata.addAll({'first_name': value ?? ''});
                                },
                                validator: (value) {
                                  if (value!.trim().isEmpty)
                                    return AppLocalizations.of(context)
                                        .translate("error_field_required");
                                  if (value.length > 20)
                                    return AppLocalizations.of(context)
                                        .translate("error_pwd_digit_required");
                                  return null;
                                }),
                          ),
                          SizedBox(
                            width: spacing_standard_new,
                          ),
                          Expanded(
                            child: TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              autofocus: false,
                              controller: lastNameCont,
                              textCapitalization: TextCapitalization.words,
                              textInputAction: TextInputAction.next,
                              style: TextStyle(
                                color: Colors.black,
                              ),
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: editText_background,
                                focusColor: editText_background_active,
                                hintText: AppLocalizations.of(context)
                                    .translate("hint_last_name"),
                              ),
                              onSaved: (value) {
                                _formdata.addAll({'last_name': value ?? ''});
                              },
                              validator: (value) {
                                if (value!.trim().isEmpty)
                                  return AppLocalizations.of(context)
                                      .translate("error_field_required");
                                if (value.length > 20)
                                  return AppLocalizations.of(context)
                                      .translate("error_pwd_digit_required");
                                return null;
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: spacing_standard_new,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      autofocus: false,
                      controller: emailCont,
                      textCapitalization: TextCapitalization.words,
                      textInputAction: TextInputAction.next,
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: editText_background,
                        focusColor: editText_background_active,
                        hintText: AppLocalizations.of(context)
                            .translate("hint_Email"),
                      ),
                      onSaved: (value) {
                        _formdata.addAll({'email': value ?? ''});
                      },
                      validator: (value) {
                        if (value!.trim().isEmpty)
                          return AppLocalizations.of(context)
                              .translate("error_field_required");
                        if (!value.contains("@"))
                          return AppLocalizations.of(context)
                              .translate("error_enter_valid_email");
                        return null;
                      },
                    ),
                    SizedBox(
                      height: spacing_standard_new,
                    ),
                    InternationalPhoneNumberInput(
                      onInputChanged: (PhoneNumber number) {},
                      initialValue: PhoneNumber(isoCode: 'ET'),
                      onInputValidated: (bool value) {},
                      selectorConfig: SelectorConfig(
                        selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
                      ),
                      ignoreBlank: false,
                      autoValidateMode: AutovalidateMode.onUserInteraction,
                      selectorTextStyle: Theme.of(context).textTheme.bodyText1,
                      textFieldController: phoneCont,
                      formatInput: false,
                      keyboardType: TextInputType.numberWithOptions(
                          signed: true, decimal: true),
                      inputBorder: OutlineInputBorder(),
                      onSaved: (PhoneNumber number) {
                        _formdata.addAll({'phone': number.toString()});
                      },
                    ),
                    SizedBox(
                      height: spacing_standard_new,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.text,
                      autofocus: false,
                      obscureText: true,
                      controller: passwordCont,
                      textCapitalization: TextCapitalization.words,
                      textInputAction: TextInputAction.next,
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: editText_background,
                        focusColor: editText_background_active,
                        hintText: AppLocalizations.of(context)
                            .translate("hint_password"),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onSaved: (value) {
                        _formdata.addAll({'password': value ?? ''});
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return AppLocalizations.of(context)
                              .translate("error_field_required");
                        } else if (value.length < 5) {
                          return AppLocalizations.of(context)
                              .translate("error_pwd_digit_required");
                        } else
                          return null;
                      },
                    ),
                    SizedBox(
                      height: spacing_standard_new,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.text,
                      autofocus: false,
                      obscureText: true,
                      controller: confirmPasswordCont,
                      textCapitalization: TextCapitalization.words,
                      textInputAction: TextInputAction.send,
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: editText_background,
                        focusColor: editText_background_active,
                        hintText: AppLocalizations.of(context)
                            .translate("hint_confirm_password"),
                      ),
                      onFieldSubmitted: (value) => signup(),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return AppLocalizations.of(context)
                              .translate("error_field_required");
                        } else if (value != passwordCont.value.text) {
                          return AppLocalizations.of(context)
                              .translate("error_password_not_matches");
                        } else
                          return null;
                      },
                    ),
                    SizedBox(
                      height: spacing_xlarge,
                    ),
                    SizedBox(
                      width: double.infinity,
                      height: 50,
                      // height: double.infinity,
                      child: MaterialButton(
                        padding: EdgeInsets.all(spacing_standard),
                        child: Text(
                          AppLocalizations.of(context).translate("lbl_sign_up"),
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(8.0)),
                        color: Theme.of(context).primaryColor,
                        onPressed: () => {signup()},
                      ),
                    ),
                    SizedBox(
                      height: spacing_standard_new,
                    ),
                    SizedBox(
                      width: double.infinity,
                      height: 50,
                      child: TextButton(
                        child: Text(
                          " ${AppLocalizations.of(context).translate("lbl_already_ha_an_account")}  ${AppLocalizations.of(context).translate("lbl_sign_in")} ",
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        onPressed: () => {
                          _goBackToLogin(),
                        },
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _goBackToLogin() {
    Navigator.of(context).pushReplacement((MaterialPageRoute(
      builder: (ctx) => LoginScreen(),
    )));
  }
}
