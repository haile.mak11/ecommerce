import 'package:ecommerce_client/providers/authentication_state.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';
import 'package:ecommerce_client/utils/widgets.dart';
import 'package:ecommerce_client/views/home/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:loader_overlay/loader_overlay.dart';

import 'new_password.dart';

class VerificationScreen extends StatefulWidget {
  final String email;
  final resetData;
  final bool isReset;
  VerificationScreen(this.email, {this.resetData, this.isReset = false});
  @override
  _VerificationScreenState createState() => _VerificationScreenState();
}

class _VerificationScreenState extends State<VerificationScreen> {
  final _formKey = GlobalKey<FormState>();
  var _formdata = Map<String, String>();
  bool isLoading = false;

  FocusNode myFocusNode1 = FocusNode();
  FocusNode myFocusNode2 = FocusNode();
  FocusNode myFocusNode3 = FocusNode();
  FocusNode myFocusNode4 = FocusNode();
  FocusNode myFocusNode5 = FocusNode();
  FocusNode myFocusNode6 = FocusNode();

  bool isActive1 = false;
  bool isActive2 = false;
  bool isActive3 = false;
  bool isActive4 = false;
  bool isActive5 = false;
  bool isActive6 = false;

  var endTime = DateTime(DateTime.now().year, DateTime.now().month,
          DateTime.now().day, DateTime.now().hour, DateTime.now().minute + 30)
      .millisecondsSinceEpoch;

  Future<void> verify() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      setState(() {
        isLoading = true;
      });

      var formdata = new Map<String, String>();
      var code = '';
      _formdata.forEach((key, value) {
        code += value;
      });

      formdata.addEntries(
          [MapEntry('email', widget.email), MapEntry('code', code)]);
      try {
        if (widget.isReset) {
          await context
              .read(authenticationNotifierProvider)
              .forgotConfirm(formdata);
          setState(() {
            isLoading = false;
          });
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => NewPassword()));
        } else {
          var response = await context
              .read(authenticationNotifierProvider)
              .verify(formdata);
          setState(() {
            isLoading = false;
          });
          toast(
              "${AppLocalizations.of(context).translate("lbl_welcome")}, ${response['name']} ",
              context);
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomeScreen()));
        }
      } catch (error) {
        setState(() {
          isLoading = false;
        });
        showErrorDialog(error.toString(), context);
      }
    }
  }

  Future<void> resend() async {
    try {
      if (widget.isReset) {
        await context
            .read(authenticationNotifierProvider)
            .forgotPassword({'email': widget.email});
        setState(() {
          isLoading = false;
        });
      } else {
        var formdata = {
          'email': widget.resetData['email'].toString(),
          'password': widget.resetData['password'].toString(),
          'type': 'USER'
        };
        var response =
            await context.read(authenticationNotifierProvider).login(formdata);
        setState(() {
          isLoading = false;
        });
        toast(
            "${AppLocalizations.of(context).translate("lbl_welcome")}, ${response['name']} ",
            context);
      }
    } catch (error) {
      setState(() {
        isLoading = false;
      });
      showErrorDialog(error.toString(), context);
    }
  }

  void change() {
    myFocusNode1.addListener(() {
      bool status;
      status = myFocusNode1.hasFocus;
      if (status == true) {
        setState(() {
          isActive1 = true;
        });
      } else {
        setState(() {
          isActive1 = false;
        });
      }
    });
    myFocusNode2.addListener(() {
      bool status;
      status = myFocusNode2.hasFocus;
      if (status == true) {
        setState(() {
          isActive2 = true;
        });
      } else {
        setState(() {
          isActive2 = false;
        });
      }
    });
    myFocusNode3.addListener(() {
      bool status;
      status = myFocusNode3.hasFocus;
      if (status == true) {
        setState(() {
          isActive3 = true;
        });
      } else {
        setState(() {
          isActive3 = false;
        });
      }
    });

    myFocusNode4.addListener(() {
      bool status;
      status = myFocusNode4.hasFocus;
      if (status == true) {
        setState(() {
          isActive4 = true;
        });
      } else {
        setState(() {
          isActive4 = false;
        });
      }
    });
    myFocusNode5.addListener(() {
      bool status;
      status = myFocusNode4.hasFocus;
      if (status == true) {
        setState(() {
          isActive5 = true;
        });
      } else {
        setState(() {
          isActive5 = false;
        });
      }
    });
    myFocusNode6.addListener(() {
      bool status;
      status = myFocusNode4.hasFocus;
      if (status == true) {
        setState(() {
          isActive6 = true;
        });
      } else {
        setState(() {
          isActive6 = false;
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    change();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    if (isLoading)
      context.loaderOverlay.show();
    else
      context.loaderOverlay.hide();
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          iconTheme: Theme.of(context).iconTheme,
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(top: 100, left: 25, right: 25),
            height: size.height,
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                      AppLocalizations.of(context).translate(
                          "lbl_we_have_sent_you_mail_with_verification_code_to"),
                      style: Theme.of(context).textTheme.headline4),
                  Text(
                    widget.email,
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  SizedBox(height: 25),
                  Text(
                      AppLocalizations.of(context)
                          .translate("lbl_enter_the_code_nto_verify_email"),
                      style: Theme.of(context).textTheme.headline6),
                  SizedBox(height: 30),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Flexible(
                        flex: 1,
                        child: Container(
                          height: 80,
                          margin: EdgeInsets.only(right: 10),
                          decoration:
                              boxDecorations(radius: 5, showShadow: true),
                          child: Align(
                            alignment: Alignment.center,
                            child: TextFormField(
                              focusNode: myFocusNode1,
                              onChanged: (term) {
                                if (term.isEmpty) {
                                  FocusScope.of(context).unfocus();
                                } else {
                                  FocusScope.of(context)
                                      .requestFocus(myFocusNode2);
                                }
                              },
                              keyboardType: TextInputType.number,
                              textAlign: TextAlign.center,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  contentPadding: EdgeInsets.all(0)),
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(1)
                              ],
                              style: TextStyle(
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black),
                              onSaved: (term) {
                                _formdata
                                    .addEntries([MapEntry('1', term ?? '')]);
                              },
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          height: 80,
                          margin: EdgeInsets.only(right: 10),
                          decoration:
                              boxDecorations(radius: 5, showShadow: true),
                          child: Align(
                            alignment: Alignment.center,
                            child: TextFormField(
                              focusNode: myFocusNode2,
                              keyboardType: TextInputType.number,
                              textAlign: TextAlign.center,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.all(0),
                              ),
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(1)
                              ],
                              style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                              onChanged: (term) {
                                if (term.isEmpty) {
                                  FocusScope.of(context)
                                      .requestFocus(myFocusNode1);
                                } else {
                                  FocusScope.of(context)
                                      .requestFocus(myFocusNode3);
                                }
                              },
                              onSaved: (term) {
                                _formdata
                                    .addEntries([MapEntry('2', term ?? '')]);
                              },
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          height: 80,
                          margin: EdgeInsets.only(right: 10),
                          decoration:
                              boxDecorations(radius: 5, showShadow: true),
                          child: Align(
                            alignment: Alignment.center,
                            child: TextFormField(
                              focusNode: myFocusNode3,
                              keyboardType: TextInputType.number,
                              textAlign: TextAlign.center,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.all(0),
                              ),
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(1)
                              ],
                              style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                              onChanged: (term) {
                                if (term.isEmpty) {
                                  FocusScope.of(context)
                                      .requestFocus(myFocusNode2);
                                } else {
                                  FocusScope.of(context)
                                      .requestFocus(myFocusNode4);
                                }
                              },
                              onSaved: (term) {
                                _formdata
                                    .addEntries([MapEntry('3', term ?? '')]);
                              },
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          height: 80,
                          margin: EdgeInsets.only(right: 10),
                          decoration:
                              boxDecorations(radius: 5, showShadow: true),
                          child: Align(
                            alignment: Alignment.center,
                            child: TextFormField(
                              focusNode: myFocusNode4,
                              keyboardType: TextInputType.number,
                              textAlign: TextAlign.center,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.all(0),
                              ),
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(1)
                              ],
                              style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                              onChanged: (term) {
                                if (term.isEmpty) {
                                  FocusScope.of(context)
                                      .requestFocus(myFocusNode3);
                                } else {
                                  FocusScope.of(context)
                                      .requestFocus(myFocusNode5);
                                }
                              },
                              onSaved: (term) {
                                _formdata
                                    .addEntries([MapEntry('4', term ?? '')]);
                              },
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          height: 80,
                          margin: EdgeInsets.only(right: 10),
                          decoration:
                              boxDecorations(radius: 5, showShadow: true),
                          child: Align(
                            alignment: Alignment.center,
                            child: TextFormField(
                              focusNode: myFocusNode5,
                              keyboardType: TextInputType.number,
                              textAlign: TextAlign.center,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.all(0),
                              ),
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(1)
                              ],
                              style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                              onEditingComplete: () =>
                                  FocusScope.of(context).previousFocus(),
                              onChanged: (term) {
                                if (term.isEmpty) {
                                  FocusScope.of(context)
                                      .requestFocus(myFocusNode4);
                                } else {
                                  FocusScope.of(context)
                                      .requestFocus(myFocusNode6);
                                }
                              },
                              onSaved: (term) {
                                _formdata
                                    .addEntries([MapEntry('5', term ?? '')]);
                              },
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          height: 80,
                          margin: EdgeInsets.only(right: 10),
                          decoration:
                              boxDecorations(radius: 5, showShadow: true),
                          child: Align(
                            alignment: Alignment.center,
                            child: TextFormField(
                              focusNode: myFocusNode6,
                              keyboardType: TextInputType.number,
                              textAlign: TextAlign.center,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.all(0),
                              ),
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(1)
                              ],
                              style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                              onChanged: (term) {
                                if (term.isEmpty) {
                                  FocusScope.of(context)
                                      .requestFocus(myFocusNode5);
                                } else {
                                  FocusScope.of(context).unfocus();
                                }
                              },
                              onSaved: (term) {
                                _formdata
                                    .addEntries([MapEntry('6', term ?? '')]);
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: spacing_standard_new),
                    alignment: Alignment.bottomLeft,
                    child: CountdownTimer(
                      endTime: endTime,
                      widgetBuilder: (_, time) {
                        if (time == null) {
                          return Text(
                            ' ${AppLocalizations.of(context).translate("lbl_code_expired")} ',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(color: Colors.white),
                          );
                        }
                        return Text(
                          ' ${time.hours ?? '0'}:${time.min ?? '0'}:${time.sec ?? '0'}',
                          style:
                              Theme.of(context).textTheme.bodyText2!.copyWith(
                                    color: Colors.white,
                                  ),
                          textAlign: TextAlign.center,
                        );
                      },
                    ),
                  ),
                  SizedBox(height: 45),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: SizedBox(
                      width: double.infinity,
                      height: 50,
                      child: MaterialButton(
                        padding: EdgeInsets.all(spacing_standard),
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8.0),
                        ),
                        color: Theme.of(context).primaryColor,
                        child: Text(
                          AppLocalizations.of(context)
                              .translate("lbl_continue"),
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: Colors.white),
                        ),
                        onPressed: () {
                          verify();
                        },
                      ),
                    ),
                  ),
                  SizedBox(height: 50),
                  Align(
                    alignment: Alignment.center,
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 20),
                        Text(
                            AppLocalizations.of(context)
                                .translate("lbl_did_not_receive"),
                            style: Theme.of(context).textTheme.bodyText1),
                        SizedBox(height: 20),
                        TextButton(
                          onPressed: () => resend(),
                          child: Text(
                            AppLocalizations.of(context)
                                .translate("lbl_resend_code"),
                            style: Theme.of(context).textTheme.headline6,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
