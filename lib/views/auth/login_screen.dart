import 'package:ecommerce_client/providers/authentication_state.dart';
import 'package:ecommerce_client/utils/constants.dart';
import 'package:ecommerce_client/utils/widgets.dart';

import 'package:ecommerce_client/views/auth/forgot_password_screen.dart';

import 'package:ecommerce_client/views/auth/signup_screen.dart';
import 'package:ecommerce_client/views/auth/verification_screen.dart';
import 'package:ecommerce_client/views/home/home_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:ecommerce_client/utils/colors.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:loader_overlay/loader_overlay.dart';
import '../../utils/app_localizations.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var emailCont = TextEditingController();
  var passwordCont = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  var _formdata = Map<String, String>();
  bool isLoading = false;
  late FirebaseMessaging messaging;
  late Firebase firebase;
  String fcmToken = '';
  @override
  void initState() {
    super.initState();

    messaging = FirebaseMessaging.instance;
    messaging.getToken().then((value) {
      fcmToken = value!;
    });
  }

  Future<void> login() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      setState(() {
        isLoading = true;
      });

      _formdata.addEntries([MapEntry('fcm_token', fcmToken)]);
      _formdata.addEntries([MapEntry('type', 'USER')]);
      try {
        var response =
            await context.read(authenticationNotifierProvider).login(_formdata);

        setState(() {
          isLoading = false;
        });
        toast(
            "${AppLocalizations.of(context).translate("lbl_welcome")}, ${response['name']} ",
            context);

        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
            builder: (context) => response['isActive']
                ? HomeScreen()
                : VerificationScreen(
                    response['email'] ?? '',
                    resetData: _formdata,
                  ),
          ),
          (route) => !route.navigator!.canPop(),
        );
      } catch (error) {
        setState(() {
          isLoading = false;
        });
        showErrorDialog(error, context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    if (isLoading)
      context.loaderOverlay.show();
    else
      context.loaderOverlay.hide();

    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          iconTheme: Theme.of(context).iconTheme),
      body: Container(
        height: height,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(24.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: height * 0.2,
                        child: Image.asset(
                          ic_logo_1,
                          fit: BoxFit.cover,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            AppLocalizations.of(context).translate("app_name"),
                            style: Theme.of(context).textTheme.headline4,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: spacing_xlarge,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        autofocus: false,
                        controller: emailCont,
                        textCapitalization: TextCapitalization.words,
                        textInputAction: TextInputAction.next,
                        style: TextStyle(
                          color: Colors.black,
                        ),
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: editText_background,
                          focusColor: editText_background_active,
                          hintText: AppLocalizations.of(context)
                              .translate("hint_Email"),
                        ),
                        onSaved: (value) {
                          _formdata.addAll({'email': value ?? ''});
                        },
                        validator: (value) {
                          if (value!.trim().isEmpty)
                            return 'This field is required';
                          if (!value.contains("@")) return 'Invalid email';
                          return null;
                        },
                      ),
                      SizedBox(
                        height: spacing_standard_new,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.visiblePassword,
                        autofocus: false,
                        obscureText: true,
                        style: TextStyle(
                          color: Colors.black,
                        ),
                        controller: passwordCont,
                        textCapitalization: TextCapitalization.words,
                        textInputAction: TextInputAction.send,
                        onFieldSubmitted: (value) => login(),
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: editText_background,
                          focusColor: editText_background_active,
                          hintText: AppLocalizations.of(context)
                              .translate("hint_password"),
                        ),
                        onSaved: (value) {
                          _formdata.addAll({'password': value ?? ''});
                        },
                        validator: (value) {
                          if (value!.isEmpty) {
                            return AppLocalizations.of(context)
                                .translate("error_field_required");
                          } else if (value.length < 6) {
                            return AppLocalizations.of(context)
                                .translate("error_pwd_digit_required");
                          } else
                            return null;
                        },
                      ),
                      SizedBox(
                        height: spacing_xlarge,
                      ),
                      SizedBox(
                        width: double.infinity,
                        height: 50,
                        // height: double.infinity,
                        child: MaterialButton(
                          padding: EdgeInsets.all(spacing_standard),
                          child: Text(
                            AppLocalizations.of(context)
                                .translate("lbl_sign_in"),
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          color: Theme.of(context).primaryColor,
                          onPressed: () => {login()},
                        ),
                      ),
                      SizedBox(height: spacing_standard_new),
                      SizedBox(
                        width: double.infinity,
                        height: 50,
                        // height: double.infinity,
                        child: TextButton(
                          child: Text(
                            "${AppLocalizations.of(context).translate("lbl_dont_have_account")} ${AppLocalizations.of(context).translate("lbl_sign_up")}",
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                          onPressed: () => {_goToSignup()},
                        ),
                      ),
                      SizedBox(height: spacing_standard_new),
                      SizedBox(
                        width: double.infinity,
                        height: 50,
                        // height: double.infinity,
                        child: TextButton(
                          child: Text(
                              "${AppLocalizations.of(context).translate("lbl_forgot_password")}",
                              style: Theme.of(context).textTheme.bodyText1),
                          onPressed: () => {_goToForgotPassword()},
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _goToSignup() {
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => SignupScreen()));
  }

  void _goToForgotPassword() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => ForgotPassword()));
  }
}
