import 'package:ecommerce_client/providers/authentication_state.dart';
import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/utils/constants.dart';
import 'package:ecommerce_client/utils/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter/material.dart';

import 'verification_screen.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  bool recoverWithPhone = true;
  var _formKey = GlobalKey<FormState>();
  bool isLoading = false;
  var _emailController = TextEditingController();
  Future<void> forgotPassword() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      setState(() {
        isLoading = true;
      });
      Map<String, String> _formdata = {};
      _formdata.addEntries([MapEntry('email', _emailController.value.text)]);
      _formdata.addEntries([MapEntry('type', 'USER')]);
      try {
        var response = await context
            .read(authenticationNotifierProvider)
            .forgotPassword(_formdata);

        setState(() {
          isLoading = false;
        });
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => VerificationScreen(
              response['email'],
              isReset: true,
            ),
          ),
        );
      } catch (error) {
        setState(() {
          isLoading = false;
        });
        showErrorDialog(error, context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        iconTheme: Theme.of(context).iconTheme,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(top: 100, left: 25, right: 25),
          child: Column(
            children: <Widget>[
              Container(
                height: (MediaQuery.of(context).size.height) / 3.5,
                child: Container(
                  margin: EdgeInsets.only(left: 16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        AppLocalizations.of(context)
                            .translate("lbl_forgot_password"),
                        style: Theme.of(context).textTheme.headline4!.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        AppLocalizations.of(context)
                            .translate("lbl_dont_worry"),
                        style: Theme.of(context).textTheme.headline6,
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(spacing_large),
                child: Container(
                  alignment: Alignment.topLeft,
                  child: Text(
                    AppLocalizations.of(context)
                        .translate("lbl_email_revcover"),
                    style: Theme.of(context).textTheme.headline6!.copyWith(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                  ),
                ),
              ),
              Form(
                key: _formKey,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: TextFormField(
                          controller: _emailController,
                          style: TextStyle(
                            color: Colors.black,
                          ),
                          obscureText: false,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            hintText: AppLocalizations.of(context)
                                .translate("hint_Email"),
                            filled: true,
                            fillColor: Color(0xFFF5F4F4),
                          ),
                          validator: (s) {
                            if (s!.trim().isEmpty)
                              return AppLocalizations.of(context)
                                  .translate("error_field_required");
                            return null;
                          }),
                    ),
                    SizedBox(height: 24),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: SizedBox(
                        width: double.infinity,
                        height: 50,
                        child: MaterialButton(
                          padding: EdgeInsets.all(spacing_standard),
                          child: Text(
                            " ${AppLocalizations.of(context).translate("lbl_submit")}   ",
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(color: Colors.white),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(8.0),
                          ),
                          color: Theme.of(context).primaryColor,
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              _formKey.currentState!.save();
                              forgotPassword();
                            }
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
