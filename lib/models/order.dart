import 'package:ecommerce_client/utils/app_localizations.dart';
import 'package:ecommerce_client/views/order/payment_screen.dart';

import 'cart.dart';

class OrderModel {
  int id;

  // user or admin only
  int? totalPrice;
  int? bonus;
  dynamic userNotes;
  String adminNotes;
  DeliveryType deliveryType;
  OrderStatus status;
  String user;
  List<Item> items;
  String date;
  PaymentType? paymentType;
  String? ttNumber;
  Banks? bankName;

  OrderModel({
    required this.id,
    required this.totalPrice,
    required this.bonus,
    required this.userNotes,
    required this.adminNotes,
    required this.deliveryType,
    required this.status,
    required this.user,
    required this.items,
    required this.date,
    required this.paymentType,
    required this.ttNumber,
    required this.bankName,
  });

  factory OrderModel.fromJson(Map<String, dynamic> json) {
    var date = DateTime.parse(json['updatedAt']);
    return OrderModel(
      id: json['id'],
      totalPrice: json['totalPrice'] ?? null,
      bonus: json['bonus'] ?? null,
      userNotes: json['userNotes'] ?? '',
      adminNotes: json['adminNotes'] ?? '',
      deliveryType: DeliveryType.values.firstWhere(
          (element) => element.toShortString() == json['deliveryType']),
      status: OrderStatus.values
          .firstWhere((element) => element.toShortString() == json['status']),
      user: json['user']['name'],
      items: List<Item>.from(json['orderItems'].map(
        (i) => Item.fromJson(i),
      )),
      date: "${date.day}-${date.month}-${date.year}",
      paymentType: json['paymentType'] != null
          ? PaymentType.values.firstWhere(
              (element) => element.toShortString() == json['paymentType'])
          : null,
      ttNumber: json['tt'] != null ? json['tt'].toString() : null,
      bankName: json['bankName'] != null
          ? Banks.values.firstWhere(
              (element) => element.toShortString() == json['bankName'])
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    //data['item'] = this.item.toJson();
    return data;
  }
}

enum DeliveryType {
  PICKUP,
  HALFPICKUP,
  DELIVERY,
}

extension DeliveryToString on DeliveryType {
  String toShortString() {
    return this.toString().split('.').last;
  }

  String toTranslatedString(context) {
    switch (this) {
      case DeliveryType.PICKUP:
        return AppLocalizations.of(context).translate("lbl_pickup");
      case DeliveryType.HALFPICKUP:
        return AppLocalizations.of(context).translate("lbl_half_pickup");
      case DeliveryType.DELIVERY:
        return AppLocalizations.of(context).translate("lbl_delivery");

      default:
        return "";
    }
  }
}

extension OrderToString on OrderStatus {
  String toShortString() {
    return this.toString().split('.').last;
  }

  String toTranslatedString(context) {
    switch (this) {
      case OrderStatus.CONFIRMED:
        return AppLocalizations.of(context).translate("lbl_confirmed");
      case OrderStatus.CONFIRMPENDING:
        return AppLocalizations.of(context).translate("lbl_confirm_pending");
      case OrderStatus.DELIVERED:
        return AppLocalizations.of(context).translate("lbl_delivered");
      case OrderStatus.PAYMENTPENDING:
        return AppLocalizations.of(context).translate("lbl_payment_pending");
      case OrderStatus.PAID:
        return AppLocalizations.of(context).translate("lbl_paid");
      case OrderStatus.REQUESTED:
        return AppLocalizations.of(context).translate("lbl_requested");

      default:
        return "";
    }
  }
}

enum OrderStatus {
  REQUESTED,
  CONFIRMPENDING,
  CONFIRMED,
  PAYMENTPENDING,
  PAID,
  DELIVERED,
}
enum PaymentType { TT, PAYPAL, CASHONDELIVERY }

extension PaymentToString on PaymentType {
  String toShortString() {
    return this.toString().split('.').last;
  }

  String toTranslatedString(context) {
    switch (this) {
      case PaymentType.TT:
        return AppLocalizations.of(context).translate("lbl_local_bank");
      case PaymentType.PAYPAL:
        return "Paypal";
      case PaymentType.CASHONDELIVERY:
        return AppLocalizations.of(context).translate("lbl_caon_delivery");

      default:
        return "";
    }
  }
}
