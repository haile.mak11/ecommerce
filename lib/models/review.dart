class ReviewModel {
  String updatedAt;
  int id;
  String name;
  int stars;
  String review;

  ReviewModel({
    required this.updatedAt,
    required this.id,
    required this.name,
    required this.stars,
    required this.review,
  });

  factory ReviewModel.fromJson(Map<String, dynamic> json) {
    var date = DateTime.parse(json['updatedAt']);
    return ReviewModel(
      updatedAt: "${date.day}-${date.month}-${date.year}",
      id: json['id'] ?? 0,
      name: json['user']['name'].toString().trim(),
      stars: json['stars'],
      review: json['review'],
    );
  }
}
