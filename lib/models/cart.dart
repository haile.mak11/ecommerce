import 'dart:convert';

import 'package:ecommerce_client/models/product.dart';

class Cart {
  List<Item> items;
  Cart({
    required this.items,
  });
  factory Cart.fromJson(List<dynamic> json) {
    return Cart(
      items: json
          .map(
            (i) => Item.fromJson(i),
          )
          .toList(),
    );
  }
}

extension Range on num {
  bool isBetween(String? range) {
    if (range != null) {
      var regex = RegExp(r"^(.*?)(?:-|>=)(.*?)$");
      var regexGroups = regex.firstMatch(range);

      var rangeValues = [regexGroups?.group(1), regexGroups?.group(2)];

      num min, max;

      if (rangeValues[0]! == '') {
        min = num.parse(rangeValues[1]!);
        max = double.infinity;
      } else {
        min = num.parse(rangeValues[0]!);
        max = num.parse(rangeValues[1]!);
      }

      return min <= this && this <= max;
    }
    return false;
  }
}

class Item {
  ProductModel product;
  int quantity;
  List<dynamic> variations;
  late int price;
  Item({
    required this.product,
    required this.quantity,
    required this.variations,
  }) {
    if (quantity.isBetween(product.range1)) {
      this.price = product.deal.hasDeal
          ? int.parse(product.deal.totalRange1Price!)
          : int.parse(product.totalRange1Price!);
    } else if (quantity.isBetween(product.range2)) {
      this.price = product.deal.hasDeal
          ? int.parse(product.deal.totalRange2Price!)
          : int.parse(product.totalRange2Price!);
    } else if (quantity.isBetween(product.range3)) {
      this.price = product.deal.hasDeal
          ? int.parse(product.deal.totalRange3Price!)
          : int.parse(product.totalRange3Price!);
    } else
      this.price = product.deal.hasDeal
          ? int.parse(product.deal.totalUnitPrice!)
          : int.parse(product.totalUnitPrice!);
  }

  factory Item.fromJson(Map<String, dynamic> json) {
    return Item(
      product: ProductModel.fromJson(json['product']),
      quantity: json['pieces'].runtimeType == String
          ? int.parse(json['pieces'])
          : json['pieces'],
      variations: json['variations'] != null ? json['variations'] : [],
    );
  }

  String toJson() {
    Map map = variations.isEmpty
        ? {'pieces': '$quantity'}
        : {'variations': '${json.encode(variations)}'};
    return json.encode(map);
  }
}
