import 'package:ecommerce_client/utils/constants.dart';

class CategoryModel {
  int id;
  bool isSelected;
  String name;
  String nameAm;
  int mainCategoryId;
  int subCategoryId;
  String slug;
  String? image;

  CategoryModel(
      {required this.id,
      required this.isSelected,
      required this.name,
      required this.nameAm,
      required this.mainCategoryId,
      required this.subCategoryId,
      required this.slug,
      required this.image});

  factory CategoryModel.fromJson(Map<String, dynamic> json) {
    String? image;
    switch (json['slug']) {
      case 'agriculture-food':
        image = agriculture;
        break;
      case 'appareltextiles-accessories':
        image = apparel;
        break;
      case 'books':
        image = books;
        break;
      case 'electronics':
        image = electronics;
        break;
      case 'health-beauty':
        image = health;
        break;
      case 'home-lights-construction':
        image = home_lights;
        break;
      case 'machinery-industrial-parts-tools':
        image = machinery;
        break;
      case 'packaging-advertising-office':
        image = packaging;
        break;
      case 'auto-transportation':
        image = auto;
        break;
      case 'bags-shoes-accessories':
        image = bags_shoes;
        break;
      case 'metallurgy-chemicals-tire-rubber-plastics':
        image = metallurgy;
        break;
      case 'electrical-equipment-components-telecoms':
        image = electrical_equipment;
        break;
      case 'gifts-sports-toys':
        image = gifts_sports;
        break;
      default:
    }
    return CategoryModel(
        id: json['id'] ?? 0,
        isSelected: json['isSelected'] ?? false,
        name: json['name'] ?? "",
        nameAm: json['nameAm'] ?? "",
        mainCategoryId:
            json['mainCategory'] != null ? json['mainCategory']['id'] : 0,
        subCategoryId:
            json['subCategory'] != null ? json['subCategory']['id'] : 0,
        slug: json['slug'],
        image: image);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['isSelected'] = this.isSelected;

    data['name'] = this.name;
    data['mainCategory']['id'] = this.mainCategoryId;
    data['subCategory']['id'] = this.subCategoryId;

    return data;
  }
}
