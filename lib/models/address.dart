class AddressModel {
  int id;
  String firstName;
  String lastName;
  String pinCode;
  String city;
  String state;
  String addressType;
  String address;
  String phoneNumber;
  String country;

  AddressModel({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.pinCode,
    required this.city,
    required this.state,
    required this.addressType,
    required this.address,
    required this.phoneNumber,
    required this.country,
  });

  factory AddressModel.fromJson(Map<String, dynamic> json) {
    return AddressModel(
      id: json['id'],
      firstName: json['first_name'],
      lastName: json['last_name'],
      pinCode: json['pinCode'],
      city: json['city'],
      state: json['state'],
      addressType: json['address_type'],
      address: json['address'],
      phoneNumber: json['phone_number'],
      country: json['country'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['pinCode'] = this.pinCode;
    data['city'] = this.city;
    data['state'] = this.state;
    data['address_type'] = this.addressType;
    data['address'] = this.address;
    data['phone_number'] = this.phoneNumber;
    data['country'] = this.country;
    return data;
  }
}
