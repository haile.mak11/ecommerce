class DailyDeal {
  String? totalPriceRange;
  String? totalUnitPrice;
  String? totalRange1Price;
  String? totalRange2Price;
  String? totalRange3Price;
  DateTime? updatedAt;
  bool hasDeal;
  DailyDeal({
    required this.totalPriceRange,
    required this.totalUnitPrice,
    required this.totalRange1Price,
    required this.totalRange2Price,
    required this.totalRange3Price,
    required this.hasDeal,
    required this.updatedAt,
  });

  factory DailyDeal.fromJson(json) {
    return DailyDeal(
      totalPriceRange:
          "${json['totalRange3Price'] != null ? json['totalRange3Price'].toString() + '-' : json['totalRange2Price'] != null ? json['totalRange2Price'].toString() + '-' : json['totalRange1Price'] != null ? json['totalRange1Price'].toString() + '-' : ''}${json['totalUnitPrice'].toString()}",
      hasDeal: json['totalUnitPrice'] != null ? true : false,
      totalUnitPrice: json['totalUnitPrice'] != null
          ? json['totalUnitPrice'].toString()
          : null,
      totalRange1Price: json['totalRange1Price'] != null
          ? json['totalRange1Price'].toString()
          : null,
      totalRange2Price: json['totalRange2Price'] != null
          ? json['totalRange2Price'].toString()
          : null,
      totalRange3Price: json['totalRange3Price'] != null
          ? json['totalRange3Price'].toString()
          : null,
      updatedAt:
          json['updatedAt'] != null ? DateTime.parse(json['updatedAt']) : null,
    );
  }
}

class ProductAmharic {
  String name;
  String description;
  String unit;
  dynamic variations;
  ProductAmharic({
    required this.name,
    required this.description,
    required this.unit,
    required this.variations,
  });

  factory ProductAmharic.fromJson(json) {
    if (json == null) {
      return ProductAmharic(
        name: '',
        description: '',
        unit: '',
        variations: {},
      );
    }
    return ProductAmharic(
      name: json['name'] ?? '',
      description: json['description'] ?? '',
      unit: json['unit'] ?? '',
      variations: json['variations'] ?? {},
    );
  }
}

class ProductModel {
  int id;
  String name;
  String description;
  String unit;
  dynamic variations;

  String? range1;
  String? range2;
  String? range3;

  String unitPriceDiscountPercentage;
  String range1PriceDiscountPercentage;
  String range2PriceDiscountPercentage;
  String range3PriceDiscountPercentage;

  String totalPriceRange;
  String? totalUnitPrice;
  String? totalRange1Price;
  String? totalRange2Price;
  String? totalRange3Price;
  Rating rating;
  bool outOfStock;

  ProductAmharic amharic;
  DailyDeal deal;
  int photosCount;
  String thumbnail;

  ProductModel({
    required this.id,
    required this.name,
    required this.description,
    required this.unit,
    required this.variations,
    required this.range1,
    required this.range2,
    required this.range3,
    required this.unitPriceDiscountPercentage,
    required this.range1PriceDiscountPercentage,
    required this.range2PriceDiscountPercentage,
    required this.range3PriceDiscountPercentage,
    required this.totalPriceRange,
    required this.totalUnitPrice,
    required this.totalRange1Price,
    required this.totalRange2Price,
    required this.totalRange3Price,
    required this.outOfStock,
    required this.amharic,
    required this.deal,
    required this.rating,
    required this.photosCount,
    required this.thumbnail,
  });
  factory ProductModel.fromJson(Map<String, dynamic> json) {
    return ProductModel(
      id: json['id'],
      name: json['name'],
      description: json['description'],
      unit: json['unit'],
      variations: (json['variations']) ?? {},
      range1: json['range1'],
      range2: json['range2'],
      range3: json['range3'],
      unitPriceDiscountPercentage: json['unitPriceDiscountPercentage'] != null
          ? json['unitPriceDiscountPercentage'].toString()
          : '0',
      range1PriceDiscountPercentage:
          json['range1PriceDiscountPercentage'] != null
              ? json['range1PriceDiscountPercentage'].toString()
              : '0',
      range2PriceDiscountPercentage:
          json['range2PriceDiscountPercentage'] != null
              ? json['range2PriceDiscountPercentage'].toString()
              : '0',
      range3PriceDiscountPercentage:
          json['range3PriceDiscountPercentage'] != null
              ? json['range3PriceDiscountPercentage'].toString()
              : '0',
      totalPriceRange:
          "${json['totalRange3Price'] != null ? json['totalRange3Price'].toString() + '-' : json['totalRange2Price'] != null ? json['totalRange2Price'].toString() + '-' : json['totalRange1Price'] != null ? json['totalRange1Price'].toString() + '-' : ''}${json['totalUnitPrice'].toString()}",
      totalUnitPrice: json['totalUnitPrice'] != null
          ? json['totalUnitPrice'].toString()
          : null,
      totalRange1Price: json['totalRange1Price'] != null
          ? json['totalRange1Price'].toString()
          : null,
      totalRange2Price: json['totalRange2Price'] != null
          ? json['totalRange2Price'].toString()
          : null,
      totalRange3Price: json['totalRange3Price'] != null
          ? json['totalRange3Price'].toString()
          : null,
      outOfStock: json['outOfStock'],
      rating: Rating.fromJson(json['rating'] ??
          {
            'average': '0',
            'oneStars': '0',
            'twoStars': '0',
            'threeStars': '0',
            'fourStars': '0',
            'fiveStars': '0',
            'total': '0',
          }),
      amharic: ProductAmharic.fromJson(json['amharic']),
      deal: DailyDeal.fromJson(json['deal']),
      photosCount: json['photosCount'],
      thumbnail: json['thumbnail'],
    );
  }
}

class Rating {
  String average;
  String oneStars;
  String twoStars;
  String threeStars;
  String fourStars;
  String fiveStars;
  String total;
  Rating({
    required this.average,
    required this.oneStars,
    required this.twoStars,
    required this.threeStars,
    required this.fourStars,
    required this.fiveStars,
    required this.total,
  });
  factory Rating.fromJson(Map<String, dynamic> json) {
    return Rating(
        average: json['average'].toString(),
        oneStars: json['oneStars'].toString(),
        twoStars: json['twoStars'].toString(),
        threeStars: json['threeStars'].toString(),
        fourStars: json['fourStars'].toString(),
        fiveStars: json['fiveStars'].toString(),
        total: json['total'].toString());
  }
}
