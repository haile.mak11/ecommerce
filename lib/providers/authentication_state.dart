import 'dart:convert';
import 'dart:io';
import 'package:ecommerce_client/providers/cart_provider.dart';
import 'package:ecommerce_client/providers/shared_utility.dart';
import 'package:ecommerce_client/providers/wishlist_provider.dart';
import 'package:ecommerce_client/utils/http_exception.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;

final authenticationNotifierProvider =
    ChangeNotifierProvider<AuthenticationNotifier>((ref) {
  final userJsonString = ref.read(sharedUtilityProvider).getUserData();
  var accessToken;
  var requestToken;
  var userId;
  var fcmToken;

  try {
    if (userJsonString == '') throw Exception('no user data found');
    final userData = json.decode(userJsonString);

    accessToken = userData['accessToken'] ?? '';
    requestToken = userData['requestToken'] ?? '';
    fcmToken = userData['fcm_token'] ?? '';
    userId = userData['user']['id'].toString();
  } catch (e) {
    accessToken = '';
    requestToken = '';
    userId = '';
  }
  return AuthenticationNotifier(
      ref, accessToken, requestToken, fcmToken, userId);
});

class AuthenticationNotifier extends ChangeNotifier {
  AuthenticationNotifier(this.ref, this._accessToken, this._requestToken,
      this.fcmToken, this._userId);
  ProviderReference ref;
  String _requestToken;
  String _userId;
  String _accessToken;
  var fcmToken;
  var verificationToken;
  bool get isAuth {
    return _requestToken != '';
  }

  String get requestToken {
    return _requestToken;
  }

  set requestToken(value) {
    _requestToken = value;
  }

  String get accessToken {
    return _accessToken;
  }

  set accessToken(value) {
    _accessToken = value;
  }

  String get userId {
    return _userId;
  }

  set userId(value) {
    _userId = value;
  }

  String get apiUrl {
    return ref.read(sharedUtilityProvider).getApiString();
  }

  bool autoLogin() {
    if (_requestToken != '') {
      return true;
    } else
      return false;
  }

  Future<dynamic> login(Map<String, String> formdata) async {
    try {
      final response = await http.post(
        Uri.parse(apiUrl + "login"),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: json.encode(formdata),
      );
      var responseData = json.decode(response.body);
      print(responseData);
      if (responseData["success"] == true) {
        if (responseData['data']['user']['isActive']) {
          _requestToken = responseData['data']['requestToken'];
          _accessToken = responseData['data']['accessToken'];
          fcmToken = responseData['data']['fcm_Token'];
          _userId = responseData['data']['user']['id'].toString();
          notifyListeners();

          final userData = jsonEncode(responseData['data']);
          ref.read(sharedUtilityProvider).setUserData(userData);
        }
        return responseData['data']['user'];
      }
      throw HttpException(responseData['err']['message']);
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    } catch (error) {
      throw new HttpException(jsonEncode({'Error': error.toString()}));
    }
  }

  Future<dynamic> signup(Map<String, String> formdata) async {
    try {
      final response = await http.post(
        Uri.parse(apiUrl + "signup"),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: json.encode(formdata),
      );
      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        return responseData['data'];
      }
      throw HttpException(responseData['err']['message']);
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    } catch (error) {
      throw new HttpException(jsonEncode({'Error': error.toString()}));
    }
  }

  Future<dynamic> verify(Map<String, String> formdata) async {
    try {
      final response = await http.post(
        Uri.parse(apiUrl + "signup/activate"),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: json.encode(formdata),
      );
      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        _requestToken = responseData['data']['requestToken'];
        _accessToken = responseData['data']['accessToken'];
        _userId = responseData['data']['user']['id'].toString();
        notifyListeners();

        final userData = jsonEncode(responseData['data']);
        ref.read(sharedUtilityProvider).setUserData(userData);
        return responseData['data']['user'];
      }
      throw HttpException(responseData['err']['message']);
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    } catch (error) {
      throw new HttpException(jsonEncode({'Error': error.toString()}));
    }
  }

  void logout() {
    _accessToken = '';
    _requestToken = '';
    userId = '';
    notifyListeners();
    ref.read(sharedUtilityProvider).removeData();
    ref.read(cartProvider).cart.items.clear();
    ref.read(wishlistProvider).wishlist.clear();
  }

  Future<dynamic> forgotPassword(Map<String, String> formdata) async {
    try {
      final response = await http.post(
        Uri.parse(apiUrl + "forgot"),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: json.encode(formdata),
      );
      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        return formdata;
      }
      throw HttpException(responseData['err']['message']);
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    } catch (error) {
      throw new HttpException(jsonEncode({'Error': error.toString()}));
    }
  }

  Future<void> forgotConfirm(Map<dynamic, dynamic> formdata) async {
    try {
      final response = await http.post(
        Uri.parse(apiUrl + "forgot/confirm"),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: json.encode(formdata),
      );
      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        verificationToken = responseData['data']['token'];
        return;
      }
      throw HttpException(responseData['err']['message']);
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    } catch (error) {
      throw new HttpException(jsonEncode({'Error': error.toString()}));
    }
  }

  Future<void> setPassword(Map<dynamic, dynamic> formdata) async {
    try {
      final response = await http.post(
        Uri.parse(apiUrl + "forgot/set"),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $verificationToken',
        },
        body: json.encode(formdata),
      );
      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        // verificationToken = responseData['data']['token'];
        return;
      }
      throw HttpException(responseData['err']['message']);
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    } catch (error) {
      throw new HttpException(jsonEncode({'Error': error.toString()}));
    }
  }

  Future<String> changeName(Map<dynamic, dynamic> formdata) async {
    try {
      final response = await http.patch(
        Uri.parse(apiUrl + "users/name"),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $accessToken',
        },
        body: json.encode(formdata),
      );
      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        return "You have changed your name.";
      }
      throw HttpException(responseData['err']['message']);
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    } catch (error) {
      throw new HttpException(jsonEncode({'Error': error.toString()}));
    }
  }

  Future<void> changePassword(Map<dynamic, dynamic> formdata) async {
    try {
      final response = await http.patch(
        Uri.parse(apiUrl + "users/password"),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $accessToken',
        },
        body: json.encode(formdata),
      );
      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        return;
      }
      throw HttpException(responseData['err']['message']);
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    } catch (error) {
      throw new HttpException(jsonEncode({'Error': error.toString()}));
    }
  }

  Future<void> refresh() async {
    try {
      print(requestToken);
      final response = await http.get(
        Uri.parse(apiUrl + "refresh"),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $requestToken',
        },
      );
      var responseData = json.decode(response.body);
      print(responseData);
      if (responseData["success"] == true) {
        if (responseData['data']['user']['isActive']) {
          _requestToken = responseData['data']['requestToken'];
          _accessToken = responseData['data']['accessToken'];
          fcmToken = responseData['data']['fcm_token'];
          _userId = responseData['data']['user']['id'].toString();
          notifyListeners();

          final userData = jsonEncode(responseData['data']);
          ref.read(sharedUtilityProvider).setUserData(userData);
        }
        return responseData['data']['user'];
      } else if (response.statusCode == 401) {
        ref.read(authenticationNotifierProvider).logout();
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      throw HttpException(responseData['err']['message']);
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    } catch (error) {
      throw new HttpException(jsonEncode({'Error': error.toString()}));
    }
  }

  bool isNotificationEnabled() {
    return fcmToken != '';
  }

  Future<void> toggleNotification() async {
    try {
      final response;
      if (isNotificationEnabled())
        response = await http.delete(
          Uri.parse(apiUrl + "fcm"),
          headers: {
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': 'Bearer $accessToken',
          },
        );
      else
        response = await http.post(
          Uri.parse(apiUrl + "fcm"),
          headers: {
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': 'Bearer $accessToken',
          },
          body: json.encode(
            {'fcm_token': await FirebaseMessaging.instance.getToken()},
          ),
        );
      var responseData = json.decode(response.body);

      if (response.statusCode == 200) {
        fcmToken = isNotificationEnabled()
            ? ''
            : await FirebaseMessaging.instance.getToken();
      } else
        throw HttpException(responseData['err']['message']);
      final userJsonString = ref.read(sharedUtilityProvider).getUserData();
      final userData = json.decode(userJsonString);
      userData['fcm_token'] = fcmToken;
      ref.read(sharedUtilityProvider).setUserData(json.encode(userData));
      notifyListeners();
      return;
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    } catch (error) {
      throw new HttpException(jsonEncode({'Error': error.toString()}));
    }
  }
}
