import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

final sharedPreferencesProvider = Provider<SharedPreferences>((ref) {
  //The return type Future<SharedPreferences> isn't a 'SharedPreferences', as required by the closure's context
  //Code Reference: https://codewithandrea.com/videos/flutter-state-management-riverpod
  throw UnimplementedError();
});

final sharedUtilityProvider = Provider<SharedUtility>((ref) {
  final _sharedPrefs = ref.watch(sharedPreferencesProvider);
  return SharedUtility(sharedPreferences: _sharedPrefs);
});

class SharedUtility {
  SharedUtility({
    required this.sharedPreferences,
  });

  final SharedPreferences sharedPreferences;
  String getApiString() {
    return "https://api.wuqiyanos.com/api/v1/";
  }

  bool isDarkModeEnabled() {
    return sharedPreferences.getBool('isDarkModeEnabled') ?? false;
  }

  Future<bool> setDarkModeEnabled(bool value) async {
    return await sharedPreferences.setBool('isDarkModeEnabled', value);
  }

  String getLanguage() {
    return sharedPreferences.getString('language') ?? "English";
  }

  Future<bool> setLanguage(String lang) {
    return sharedPreferences.setString('language', lang);
  }

  String getUserData() {
    return sharedPreferences.getString('user_data') ?? '';
  }

  Future<bool> setUserData(String userData) {
    return sharedPreferences.setString('user_data', userData);
  }

  Future<bool> removeData() {
    return sharedPreferences.remove('user_data');
  }

  Future<String> lookupUserCountry() async {
    final response = await http
        .get(Uri.parse('https://api.ipregistry.co?key=4rrhmdhw0tz7kuft'));
    if (response.statusCode == 200) {
      return json.decode(response.body)['location']['country']['name'];
    } else {
      return '';
    }
  }
}
