import 'dart:convert';
import 'dart:io';
import 'package:ecommerce_client/models/product.dart';
import 'package:ecommerce_client/models/review.dart';
import 'package:ecommerce_client/providers/authentication_state.dart';
import 'package:ecommerce_client/providers/shared_utility.dart';
import 'package:ecommerce_client/utils/http_exception.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_riverpod/flutter_riverpod.dart';

final productProvider = ChangeNotifierProvider<ProductChangeNotifier>((ref) {
  var apiUrl = ref.read(authenticationNotifierProvider).apiUrl;
  return ProductChangeNotifier(apiUrl, ref);
});

class ProductChangeNotifier extends ChangeNotifier {
  // List<ProductModel> newestProducts = [];
  List<ProductModel> dailyDealProducts = [];
  List<ProductModel> category0Products = [];
  List<ProductModel> category1Products = [];
  List<ProductModel> category2Products = [];

  final _apiUrl;

  ProviderReference ref;

  ProductChangeNotifier(this._apiUrl, this.ref);

  Future<List<ProductModel>> fetchProducts({
    bool dailyDeal = false,
    bool recent = false,
    bool featured = false,
    String? category,
    int pageNumber = 1,
    int pageSize = 12,
    int? min,
    int? max,
  }) async {
    try {
      final response = await http.get(
        Uri.parse(_apiUrl +
            "products${dailyDeal ? '/deals' : ''}?${recent ? 'sort=recent&' : ''}page=$pageNumber&size=$pageSize${category != null ? '&category=${Uri.encodeComponent(category)}' : ''}${min != null ? '&min=$min' : ''}${max != null ? '&max=$max' : ''}"),
      );
      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        var responseJson = json.decode(response.body)['data']['data'];
        List<ProductModel> productsList = [];
        responseJson.forEach((value) {
          productsList.add(ProductModel.fromJson(value));
        });
        notifyListeners();
        return productsList;
      } else if (response.statusCode == 401) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<List<ProductModel>> fetchCategoryProducts({
    required productsList,
    bool dailyDeal = false,
    bool recent = false,
    bool featured = false,
    String? category,
    int pageNumber = 1,
    int pageSize = 12,
  }) async {
    try {
      if (productsList.isNotEmpty) {
        return productsList;
      }
      final response = await http.get(
        Uri.parse(_apiUrl +
            "products${dailyDeal ? '/deals' : ''}?${recent ? 'sort=recent&' : ''}page=$pageNumber&size=$pageSize${category != null ? '&category=${Uri.encodeComponent(category)}' : ''}"),
      );
      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        var responseJson = json.decode(response.body)['data']['data'];

        responseJson.forEach((value) {
          productsList.add(ProductModel.fromJson(value));
        });
        notifyListeners();
        return productsList;
      } else if (response.statusCode == 401) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  fetch(category1, category2, category3) async {
    fetchCategoryProducts(
        productsList: this.dailyDealProducts, dailyDeal: true);

    fetchCategoryProducts(
        productsList: this.category0Products, category: category1);
    fetchCategoryProducts(
        productsList: this.category1Products, category: category2);
    fetchCategoryProducts(
        productsList: this.category2Products, category: category3);
    notifyListeners();
  }

  String getProductImage({required id}) {
    return "${ref.read(sharedUtilityProvider).getApiString()}products/$id/images/";
  }

  Future<List<ProductModel>> searchProduct(String searchText,
      {pageNumber = 1, pageSize = 5}) async {
    try {
      final response = await http.get(
        Uri.parse(_apiUrl + "products/search?key=$searchText"),
      );

      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        List<dynamic> productJson = json.decode(response.body)['data']['data'];
        List<ProductModel> searchResult = [];
        productJson.forEach((value) {
          searchResult.add(ProductModel.fromJson(value));
        });
        notifyListeners();
        return searchResult;
      } else if (response.statusCode == 401) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<ReviewModel> addReview(int id, formdata) async {
    try {
      final response = await http
          .post(Uri.parse(_apiUrl + "products/$id/ratings"), headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':
            'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
      }, body: {
        'stars': json.encode(formdata['stars']),
        'review': json.encode(formdata['review'])
      });

      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        var reviewJson = json.decode(response.body)['data'];
        return ReviewModel.fromJson(reviewJson);
      } else if (response.statusCode == 401) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<List<ReviewModel>> getProductReviews(
    int id,
  ) async {
    try {
      final response = await http.get(
        Uri.parse(_apiUrl + "products/$id/ratings"),
      );

      var responseData = json.decode(response.body);
      if (responseData["success"] == true) {
        List<dynamic> reviewJson = json.decode(response.body)['data']['data'];
        List<ReviewModel> reviews = [];
        reviewJson.forEach((value) {
          reviews.add(ReviewModel.fromJson(value));
        });
        notifyListeners();
        return reviews;
      } else if (response.statusCode == 401) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }
}
