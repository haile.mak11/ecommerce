import 'package:ecommerce_client/providers/shared_utility.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';

/* AppTheme */

final appThemeProvider = Provider<AppTheme>((ref) {
  return AppTheme();
});

class AppTheme {
  static ThemeData _lightThemeData = ThemeData.light().copyWith(
    primaryColor: Color(0xFF0096d9),
    primaryColorDark: Color(0xFF283891),
    backgroundColor: Colors.grey[200],
    scaffoldBackgroundColor: Colors.white,
    iconTheme: IconThemeData(color: Colors.black),
    appBarTheme: AppBarTheme(
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
      titleTextStyle: TextStyle(color: Colors.white, fontSize: 18),
      backgroundColor: Color(0xFF0096d9),
      actionsIconTheme: IconThemeData(color: Colors.white),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        textStyle: TextStyle(
          color: Colors.white,
        ),
        primary: Color(0xFF0096d9),
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      border: OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadius.all(Radius.circular(0)),
      ),
      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
      floatingLabelBehavior: FloatingLabelBehavior.never,
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8.0),
          borderSide:
              BorderSide(color: Colors.grey.withOpacity(0.5), width: 0.5)),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8.0),
        borderSide: BorderSide(color: Colors.grey.withOpacity(0.5), width: 0),
      ),
    ),
    textTheme: TextTheme(
        bodyText1: TextStyle(
            fontWeight: FontWeight.normal,
            fontSize: 18,
            fontFamily: GoogleFonts.workSans().fontFamily,
            color: Colors.black),
        bodyText2: TextStyle(
            fontWeight: FontWeight.normal,
            fontSize: 16,
            fontFamily: GoogleFonts.workSans().fontFamily,
            color: Colors.black),
        headline6: TextStyle(
            fontFamily: GoogleFonts.workSans().fontFamily,
            fontWeight: FontWeight.w900,
            color: Colors.black),
        headline5: TextStyle(
            fontFamily: GoogleFonts.workSans().fontFamily,
            fontWeight: FontWeight.w900,
            color: Colors.black),
        headline4: TextStyle(
            fontFamily: GoogleFonts.workSans().fontFamily,
            fontWeight: FontWeight.w900,
            color: Colors.black),
        headline3: TextStyle(
            fontFamily: GoogleFonts.workSans().fontFamily,
            fontWeight: FontWeight.w900,
            color: Colors.black),
        headline2: TextStyle(
            fontFamily: GoogleFonts.workSans().fontFamily,
            fontWeight: FontWeight.w900,
            color: Colors.black),
        headline1: TextStyle(
            fontFamily: GoogleFonts.workSans().fontFamily,
            fontWeight: FontWeight.w900,
            color: Colors.black),
        button: TextStyle(
            fontFamily: GoogleFonts.workSans().fontFamily, color: Colors.black),
        subtitle1: TextStyle(
            fontFamily: GoogleFonts.workSans().fontFamily,
            fontWeight: FontWeight.bold,
            color: Colors.black),
        subtitle2: TextStyle(
            fontFamily: GoogleFonts.workSans().fontFamily,
            fontWeight: FontWeight.bold,
            color: Colors.black)),
  );

  static ThemeData _darkThemeData = ThemeData(
    primaryColor: Color(0xFF0096d9),
    primaryColorDark: Color(0xFF283891),
    backgroundColor: Colors.blueGrey[700],

    appBarTheme: AppBarTheme(
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
      titleTextStyle: TextStyle(color: Colors.white, fontSize: 18),
      backgroundColor: Color(0xFF0096d9),
      actionsIconTheme: IconThemeData(
        color: Colors.white,
      ),
    ),
    //cardColor: Colors.grey[900],
    fontFamily: GoogleFonts.workSans().fontFamily,
    iconTheme: IconThemeData(color: Colors.grey[200]),
    scaffoldBackgroundColor: Colors.black,
    canvasColor: Colors.blueGrey[700],
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        textStyle: TextStyle(
          color: Colors.white,
        ),
        primary: Color(0xFF0096d9),
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      counterStyle: TextStyle(
        backgroundColor: Colors.blueGrey[700],
      ),
      hintStyle: TextStyle(color: Colors.blueGrey[700]),
      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
      floatingLabelBehavior: FloatingLabelBehavior.never,
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8.0),
          borderSide: BorderSide(color: Colors.grey, width: 0.5)),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8.0),
        borderSide: BorderSide(color: Colors.grey, width: 0),
      ),
    ),
    dialogBackgroundColor: Colors.blueGrey[700],
    dialogTheme: DialogTheme(
      backgroundColor: Colors.blueGrey[700],
    ),
    unselectedWidgetColor: Colors.white,

    textTheme: TextTheme(
      bodyText1: TextStyle(
        fontWeight: FontWeight.normal,
        fontSize: 18,
        fontFamily: GoogleFonts.workSans().fontFamily,
        color: Colors.white,
      ),
      bodyText2: TextStyle(
        fontWeight: FontWeight.normal,
        fontSize: 16,
        fontFamily: GoogleFonts.workSans().fontFamily,
        color: Colors.white,
      ),
      headline6: TextStyle(
        fontFamily: GoogleFonts.workSans().fontFamily,
        fontWeight: FontWeight.w900,
        color: Colors.white,
      ),
      headline5: TextStyle(
        fontFamily: GoogleFonts.workSans().fontFamily,
        fontWeight: FontWeight.w900,
        color: Colors.white,
      ),
      headline4: TextStyle(
        fontFamily: GoogleFonts.workSans().fontFamily,
        fontWeight: FontWeight.w900,
        color: Colors.white,
      ),
      headline3: TextStyle(
        fontFamily: GoogleFonts.workSans().fontFamily,
        fontWeight: FontWeight.w900,
        color: Colors.white,
      ),
      headline2: TextStyle(
        fontFamily: GoogleFonts.workSans().fontFamily,
        fontWeight: FontWeight.w900,
        color: Colors.white,
      ),
      headline1: TextStyle(
        fontFamily: GoogleFonts.workSans().fontFamily,
        fontWeight: FontWeight.w900,
        color: Colors.white,
      ),
      button: TextStyle(
        fontFamily: GoogleFonts.workSans().fontFamily,
        color: Colors.white,
      ),
      subtitle1: TextStyle(
        fontFamily: GoogleFonts.workSans().fontFamily,
        fontWeight: FontWeight.bold,
        color: Colors.white,
      ),
    ),
  );

  ThemeData getAppThemedata(BuildContext context, bool isDarkModeEnabled) {
    return isDarkModeEnabled ? _darkThemeData : _lightThemeData;
  }
}

final appThemeStateProvider =
    StateNotifierProvider<AppThemeNotifier, bool>((ref) {
  final _isDarkModeEnabled =
      ref.read(sharedUtilityProvider).isDarkModeEnabled();
  return AppThemeNotifier(_isDarkModeEnabled);
});

class AppThemeNotifier extends StateNotifier<bool> {
  AppThemeNotifier(this.defaultDarkModeValue) : super(defaultDarkModeValue);

  final bool defaultDarkModeValue;

  toggleAppTheme(BuildContext context) {
    final _isDarkModeEnabled =
        context.read(sharedUtilityProvider).isDarkModeEnabled();
    final _toggleValue = !_isDarkModeEnabled;
    context
        .read(
          sharedUtilityProvider,
        )
        .setDarkModeEnabled(_toggleValue)
        .whenComplete(
          () => {
            state = _toggleValue,
          },
        );
  }
}
