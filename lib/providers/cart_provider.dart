import 'dart:convert';
import 'dart:io';
import 'package:ecommerce_client/models/cart.dart';
import 'package:ecommerce_client/models/product.dart';
import 'package:ecommerce_client/providers/authentication_state.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../utils/http_exception.dart';

final cartProvider = ChangeNotifierProvider<CartChangeNotifier>((ref) {
  var apiUrl = ref.read(authenticationNotifierProvider).apiUrl;
  return CartChangeNotifier(apiUrl, ref);
});

class CartChangeNotifier extends ChangeNotifier {
  Cart cart = new Cart(items: []);

  final _apiUrl;

  ProviderReference ref;

  CartChangeNotifier(this._apiUrl, this.ref);

  Future<Cart> fetchCart() async {
    try {
      if (!ref.read(authenticationNotifierProvider).isAuth) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      final response = await http.get(
        Uri.parse(_apiUrl + "cart"),
        headers: {
          'Authorization':
              'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
        },
      );
      var responseData = json.decode(response.body);
      print(response.body);
      if (responseData["success"] == true) {
        var cartJson = json.decode(response.body)['data']['data'];
        cart = Cart.fromJson(cartJson);
        notifyListeners();
        return cart;
      } else if (response.statusCode == 401) {
        ref.read(authenticationNotifierProvider).refresh();
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<void> addToCart({
    required ProductModel product,
    required int quantity,
    required variations,
  }) async {
    try {
      Item item = cart.items.firstWhere(
          (element) => element.product.id == product.id,
          orElse: () =>
              Item(product: product, quantity: quantity, variations: []));

      if (variations.isNotEmpty) {
        item.variations = [
          {"$quantity": variations}
        ];
        item.quantity = quantity;
      } else {
        item.quantity = quantity;
      }
      final response = await http.post(
        Uri.parse(_apiUrl + "cart/${product.id}"),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization':
              'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
        },
        body: item.toJson(),
      );
      var responseData = json.decode(response.body);
      if (responseData["success"] == true) {
        cart.items.removeWhere((element) => element.product.id == product.id);
        try {
          var newItem = Item.fromJson(responseData['data']);
          cart.items.add(newItem);
        } catch (e) {
          cart.items.add(item);
        }
        notifyListeners();
        return;
      } else if (response.statusCode == 401) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      throw HttpException(
        jsonEncode({"Information": "You are not logged in"}),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<void> removeItem({
    required ProductModel product,
  }) async {
    try {
      final response = await http.delete(
        Uri.parse(_apiUrl + "cart/${product.id}"),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization':
              'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
        },
      );
      print(response.body);
      if (response.statusCode == 204) {
        cart.items.removeWhere((element) => element.product.id == product.id);

        notifyListeners();
        return;
      } else if (response.statusCode == 401) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      throw HttpException(
        jsonEncode({"Information": "You are not logged in"}),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<void> removeVariation(
      {required ProductModel product, dynamic variation}) async {
    try {
      final response = await http.put(Uri.parse(_apiUrl + "cart/${product.id}"),
          headers: {
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization':
                'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
          },
          body: json.encode({
            "variations": [variation]
          }));

      var responseData = json.decode(response.body);
      if (responseData["success"] == true) {
        var responseJson = json.decode(response.body)['data'];
        cart.items
            .firstWhere((element) => element.product.id == product.id)
            .variations = responseJson['variations'];

        notifyListeners();
        return;
      } else if (response.statusCode == 401) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }
}
