import 'dart:convert';
import 'dart:io';

import 'package:ecommerce_client/models/category.dart';
import 'package:ecommerce_client/providers/authentication_state.dart';
import 'package:ecommerce_client/utils/http_exception.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;

final categoriesProvider =
    ChangeNotifierProvider<CategoriesChangeNotifier>((ref) {
  var apiUrl = ref.read(authenticationNotifierProvider).apiUrl;
  return CategoriesChangeNotifier(apiUrl);
});

class CategoriesChangeNotifier extends ChangeNotifier {
  List<CategoryModel> mainCategories = [];

  final apiUrl;

  CategoriesChangeNotifier(this.apiUrl);

  Future<List<CategoryModel>> fetchMainCategories() async {
    try {
      if (mainCategories.isNotEmpty) {
        return mainCategories;
      }
      final response = await http.get(
        Uri.parse(apiUrl + "categories?subLevel=1"),
      );
      var responseData = json.decode(response.body);
      mainCategories.clear();
      if (responseData["success"] == true) {
        var responseJson = json.decode(response.body)['data'];
        responseJson.forEach((value) {
          mainCategories.add(CategoryModel.fromJson(value));
        });

        notifyListeners();
        return mainCategories;
      } else if (response.statusCode == 502) {
        throw HttpException(jsonEncode({"Error": "Server error"}));
      }
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<List<CategoryModel>> fetchSubCategories(
      String category, int subLevel) async {
    try {
      List<CategoryModel> subCategories = [];
      var encodedCategory = Uri.encodeComponent(category);
      final response = await http.get(
        Uri.parse(
            apiUrl + "categories?category=$encodedCategory&subLevel=$subLevel"),
      );
      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        List<dynamic> categoriesJson = json.decode(response.body)['data'];
        categoriesJson.forEach((value) {
          subCategories.add(CategoryModel.fromJson(value));
        });
        notifyListeners();
        return subCategories;
      } else if (response.statusCode == 502) {
        throw HttpException(jsonEncode({"Error": "Server error"}));
      }
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }
}
