import 'dart:convert';

import 'package:ecommerce_client/providers/shared_utility.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class LanguageHelper {
  static convertLangNameToLocale(String langNameToConvert) {
    Locale convertedLocale;
    switch (langNameToConvert) {
      case 'English':
        convertedLocale = Locale('en', 'EN');
        break;
      case 'አማርኛ':
        convertedLocale = Locale('am', 'ET');
        break;
      default:
        convertedLocale = Locale('en', 'EN');
    }
    return convertedLocale;
  }

  static convertLocaleToLangName(String localeToConvert) {
    String langName;
    switch (localeToConvert) {
      case 'en':
        langName = "English";
        break;
      case 'am':
        langName = "አማርኛ";
        break;

      default:
        langName = "English";
    }
    return langName;
  }
}

// final languageNotifierProvider =
//     ChangeNotifierProvider<LanguageNotifier>((ref) {
//   final lang = ref.read(sharedUtilityProvider).getLanguage();
//   return LanguageNotifier(lang, LanguageHelper().convertLangNameToLocale(lang));
// });

// class LanguageNotifier with ChangeNotifier {
//   String currentLanguage;
//   Locale locale;
//   LanguageNotifier(this.currentLanguage, this.locale);
//   LanguageHelper languageHelper = LanguageHelper();

//   Locale get getlocale => locale;

//   void changeLocale(BuildContext context, String newLocale) {
//     Locale convertedLocale;

//     currentLanguage = newLocale;
//     convertedLocale = languageHelper.convertLangNameToLocale(newLocale);
//     context.read(sharedUtilityProvider).setLanguage(newLocale).whenComplete(
//           () => {
//             locale = convertedLocale,
//           },
//         );

//     notifyListeners();
//   }

//   Future<String> lookupUserCountry() async {
//     final response = await http
//         .get(Uri.parse('https://api.ipregistry.co?key=4rrhmdhw0tz7kuft'));
//     if (response.statusCode == 200) {
//       return json.decode(response.body)['location']['country']['name'];
//     } else {
//       return '';
//     }
//   }

//   defineCurrentLanguage(context) {
//     String definedCurrentLanguage;
//     definedCurrentLanguage = currentLanguage;
//     return definedCurrentLanguage;
//   }
// }

final languageStateProvider =
    StateNotifierProvider<LanguageStateNotifier, String>((ref) {
  final lang = ref.read(sharedUtilityProvider).getLanguage();
  return LanguageStateNotifier(lang);
});

class LanguageStateNotifier extends StateNotifier<String> {
  LanguageStateNotifier(this.currentLanguage)
      : super(
          currentLanguage,
        ) {
    locale = LanguageHelper.convertLangNameToLocale(currentLanguage);
  }

  String currentLanguage;
  late Locale locale;

  LanguageHelper languageHelper = LanguageHelper();

  Locale get getlocale => locale;

  void changeLocale(BuildContext context, String newLanguage) {
    Locale convertedLocale;
    currentLanguage = newLanguage;
    convertedLocale = LanguageHelper.convertLangNameToLocale(newLanguage);
    context
        .read(sharedUtilityProvider)
        .setLanguage(
          newLanguage,
        )
        .whenComplete(
          () => {
            state = newLanguage,
            locale = convertedLocale,
          },
        );
  }
}
