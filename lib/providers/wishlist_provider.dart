import 'dart:convert';
import 'dart:io';
import 'package:ecommerce_client/models/product.dart';
import 'package:ecommerce_client/providers/authentication_state.dart';
import 'package:ecommerce_client/utils/http_exception.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_riverpod/flutter_riverpod.dart';

final wishlistProvider = ChangeNotifierProvider<WishlistChangeNotifier>((ref) {
  var apiUrl = ref.read(authenticationNotifierProvider).apiUrl;
  return WishlistChangeNotifier(apiUrl, ref);
});

class WishlistChangeNotifier extends ChangeNotifier {
  List<ProductModel> wishlist = [];

  final _apiUrl;

  ProviderReference ref;

  WishlistChangeNotifier(this._apiUrl, this.ref) {
    // fetchWishlist();
  }

  Future<List<ProductModel>> fetchWishlist() async {
    try {
      if (!ref.read(authenticationNotifierProvider).isAuth) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      final response = await http.get(
        Uri.parse(_apiUrl + "wishlist"),
        headers: {
          'Authorization':
              'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
        },
      );
      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        var wishlistJson = json.decode(response.body)['data']['data'];
        if (wishlistJson.isNotEmpty) {
          wishlist = wishlistJson
              .map<ProductModel>(
                (i) => ProductModel.fromJson(i['product']),
              )
              .toList();
        }

        notifyListeners();
        return wishlist;
      } else if (response.statusCode == 401) {
        ref.read(authenticationNotifierProvider).refresh();
        throw HttpException(
            jsonEncode({"Information": "You need to be logged in"}));
      }
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<void> addToWishlist({
    required ProductModel product,
  }) async {
    try {
      final response = await http.post(
        Uri.parse(_apiUrl + "wishlist/${product.id}"),
        headers: {
          'Authorization':
              'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
        },
      );

      var responseData = json.decode(response.body);
      if (responseData["success"] == true) {
        wishlist.add(product);
        notifyListeners();
        return;
      } else if (response.statusCode == 401) {
        throw HttpException(
            jsonEncode({"Information": "You need to be logged in"}));
      }
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<void> removeItem({
    required ProductModel product,
  }) async {
    try {
      final response = await http.delete(
        Uri.parse(_apiUrl + "wishlist/${product.id}"),
        headers: {
          'Authorization':
              'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
        },
      );

      if (response.statusCode == 204) {
        wishlist.removeWhere((element) => element.id == product.id);
        notifyListeners();
        return;
      } else if (response.statusCode == 401) {
        ref.read(authenticationNotifierProvider).refresh();
        throw HttpException(
            jsonEncode({"Information": "You need to be logged in"}));
      }
      var responseData = json.decode(response.body);
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  bool isFavourite(ProductModel product) {
    int count = wishlist.where((c) => c.id == product.id).toList().length;
    return count != 0;
  }
}
