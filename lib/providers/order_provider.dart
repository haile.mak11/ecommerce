import 'dart:convert';
import 'dart:io';
import 'package:ecommerce_client/models/cart.dart';
import 'package:ecommerce_client/models/order.dart';
import 'package:ecommerce_client/providers/authentication_state.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../utils/http_exception.dart';

final orderProvider = ChangeNotifierProvider<OrderChangeNotifier>((ref) {
  var apiUrl = ref.read(authenticationNotifierProvider).apiUrl;
  return OrderChangeNotifier(apiUrl, ref);
});

class OrderChangeNotifier extends ChangeNotifier {
  List<OrderModel> orders = [];

  final _apiUrl;

  ProviderReference ref;

  OrderChangeNotifier(this._apiUrl, this.ref);

  Future<List<OrderModel>> fetchOrders({
    int pageNumber = 1,
    int pageSize = 12,
  }) async {
    try {
      if (!ref.read(authenticationNotifierProvider).isAuth) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      final response = await http.get(
        Uri.parse(_apiUrl + "orders?page=$pageNumber&size=$pageSize"),
        headers: {
          'Authorization':
              'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
        },
      );
      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        var orderJson = json.decode(response.body)['data']['data'];
        var fetchedOrders = List<OrderModel>.from(
          orderJson.map((order) {
            return OrderModel.fromJson(order);
          }),
        );
        orders.addAll(fetchedOrders);

        notifyListeners();
        return fetchedOrders;
      } else if (response.statusCode == 401) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      print(responseData['err']);
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<OrderModel> getOrder({
    required String id,
  }) async {
    try {
      if (!ref.read(authenticationNotifierProvider).isAuth) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      final response = await http.get(
        Uri.parse(_apiUrl + "orders/$id"),
        headers: {
          'Authorization':
              'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
        },
      );
      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        var orderJson = json.decode(response.body)['data'];
        var fetchedOrder = OrderModel.fromJson(orderJson);

        notifyListeners();
        return fetchedOrder;
      } else if (response.statusCode == 401) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<String> getUserBonus() async {
    try {
      final response = await http.get(
        Uri.parse(_apiUrl + "users/bonus"),
        headers: {
          'Authorization':
              'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
        },
      );
      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        String bonus = json.decode(response.body)['data']['bonus'].toString();

        notifyListeners();
        return bonus;
      } else if (response.statusCode == 401) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<void> orderCart(
      {required DeliveryType deliveryType, dynamic notes}) async {
    try {
      final response = await http.post(Uri.parse(_apiUrl + "orders"), headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':
            'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
      }, body: {
        "deliveryType": deliveryType.toShortString(),
        "notes": json.encode(notes),
      });

      var responseData = json.decode(response.body);
      if (responseData["success"] == true) {
        return;
      } else if (response.statusCode == 401) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }

      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<void> orderProduct(
      {required Item item,
      required DeliveryType deliveryType,
      dynamic notes}) async {
    try {
      final response = await http.post(
          Uri.parse(_apiUrl + "orders?productId=${item.product.id}"),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization':
                'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
          },
          body: {
            "pieces": "${item.quantity}",
            "deliveryType": deliveryType.toShortString(),
            if (item.variations.isNotEmpty)
              "variations": json.encode(item.variations),
            "notes": json.encode(notes)
          });

      var responseData = json.decode(response.body);
      if (responseData["success"] == true) {
        return;
      } else if (response.statusCode == 401) {
        throw HttpException(
            jsonEncode({"Information": "You are not logged in"}));
      }

      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<void> confirmOrderRequest(
      {required OrderModel order, required bool confirm, String? bonus}) async {
    try {
      final response =
          await http.patch(Uri.parse(_apiUrl + "orders/${order.id}"), headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':
            'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
      }, body: {
        "confirm": "$confirm",
        if (bonus != null) "bonus": bonus
      });

      if (response.statusCode == 200 || response.statusCode == 204) {
        if (confirm) {
          var confirmedOrder = orders.firstWhere(
            (element) => element.id == order.id,
          );
          confirmedOrder.status = OrderStatus.CONFIRMED;
        } else
          orders.removeWhere(
            (element) => element.id == order.id,
          );
        notifyListeners();
        return;
      }
      var responseData = json.decode(response.body);
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<String> proceedWithPayPal({
    required OrderModel order,
  }) async {
    try {
      final response =
          await http.post(Uri.parse(_apiUrl + "payment/pay"), headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':
            'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
      }, body: {
        "orderId": order.id.toString(),
      });

      var responseData = json.decode(response.body);
      if (responseData["success"] == true) {
        var redirectUrl = json.decode(response.body)['data']['url'];

        notifyListeners();
        return redirectUrl;
      }
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<void> payWithCash({required OrderModel order}) async {
    try {
      final response =
          await http.post(Uri.parse(_apiUrl + "payment/cash"), headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':
            'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
      }, body: {
        "orderId": order.id.toString(),
      });

      var responseData = json.decode(response.body);
      if (responseData["success"] == true) {
        var confirmedOrder = orders.firstWhere(
          (element) => element.id == order.id,
        );
        confirmedOrder = OrderModel.fromJson(responseData['data']['order']);
        notifyListeners();
        return;
      }
      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<void> payWithTT({
    required OrderModel order,
    required String ttNumber,
    required String bankName,
  }) async {
    try {
      final response =
          await http.post(Uri.parse(_apiUrl + "payment/local"), headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':
            'Bearer ${ref.read(authenticationNotifierProvider).accessToken}',
      }, body: {
        "orderId": order.id.toString(),
        "ttNumber": ttNumber,
        "bankName": bankName
      });

      var responseData = json.decode(response.body);

      if (responseData["success"] == true) {
        var confirmedOrder = orders.firstWhere(
          (element) => element.id == order.id,
        );
        confirmedOrder = OrderModel.fromJson(responseData['data']['order']);
        notifyListeners();
        return;
      }

      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }

  Future<void> confirmPaypalPayment(id, payerId, paymentId, amount) async {
    try {
      final response = await http.get(
        Uri.parse(_apiUrl +
            "payment/process?id=$id&PayerID=$payerId&paymentId=$paymentId&total=$amount"),
      );

      var responseData = json.decode(response.body);

      if (responseData["succes"] == true) {
        var confirmedOrder =
            orders.firstWhere((element) => element.id == int.parse(id));
        confirmedOrder.status = OrderStatus.PAID;
        notifyListeners();
        return;
      }

      throw HttpException(
        jsonEncode(responseData['err']),
      );
    } on SocketException {
      throw new HttpException(
          jsonEncode({"Error": "Could not connect to server"}));
    }
  }
}
